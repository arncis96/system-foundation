#pragma once

#include <list>

#include "Word.h"
#include "SFINE.h"
#include "Global.h"
#include "TypeTraits.h"
#include "Visitor.h"

namespace System
{
    namespace Reflection
    {
        template<typename StreamType, typename T>
        $enable_ift(IsEnum<T>::value, void) ParseValue(StreamType& rStream, T& rValue, const Visitor::Slice& krsSlice) {
            const Enum* kpcEnum = Enum::Get<T>();

            if (kpcEnum == NULL)
                return;

            const Constant* kpcConstant = kpcEnum->FindConstant(krsSlice.m_nWordId);

            if (kpcConstant)
                rValue = kpcConstant->GetValue<T>();
        }

        template<typename StreamType, typename T>
        $enable_if<Has<StreamType, T, Operator::GlobalRightShift>::value, void>::type ParseValue(StreamType& rStream, T& rValue, const Visitor::Slice& krsSlice) {
            rStream.clear();
            rStream.seekg(krsSlice.m_nOffset);
            rStream >> rValue;
        }

        template<typename StreamType, typename T>
        $enable_if<!IsEnum<T>::value && !Has<StreamType, T, Operator::GlobalRightShift>::value, void>::type ParseValue(StreamType& rStream, T& rValue, const Visitor::Slice& krsSlice) {

        }


        template<typename StreamType>
        inline void ParseValue(StreamType& rStream, std::string& rstrValue, const Visitor::Slice& krsSlice) {
            rstrValue.resize(krsSlice.m_nLength, 0);

            rStream.clear();
            rStream.seekg(krsSlice.m_nOffset);
            rStream.read(&rstrValue[0], krsSlice.m_nLength);
        }

        template<typename LHS, typename RHS>
        struct Operators
        {
            typedef LHS(LHS::* IntCopySig)(int);
            typedef LHS(LHS::* UnaryImmutSig)() const;
            typedef LHS& (LHS::* UnaryMutSig)();

            typedef bool(LHS::* UnaryLogicSig)() const;

            typedef bool(LHS::* CmpSig)(const RHS&) const;
            typedef LHS(LHS::* CopySig)(const RHS&) const;
            typedef LHS& (LHS::* SelfSig)(const RHS&);
            typedef RHS& (LHS::* MutSig)(RHS&);


            $get_member_pointer(MutSig, GetMutableLeftShift, LHS, C, operator<<);
            $get_member_pointer(MutSig, GetMutableRightShift, LHS, C, operator>>);

            //Assignment operators
            $get_member_pointer(SelfSig, GetSimpleAssignment, LHS, C, operator=);
            $get_member_pointer(SelfSig, GetAdditionAssignment, LHS, C, operator+=);
            $get_member_pointer(SelfSig, GetSubtractionAssignment, LHS, C, operator-=);
            $get_member_pointer(SelfSig, GetMultiplicationAssignment, LHS, C, operator*=);
            $get_member_pointer(SelfSig, GetDivisionAssignment, LHS, C, operator /=);
            $get_member_pointer(SelfSig, GetRemainderAssignment, LHS, C, operator %=);
            $get_member_pointer(SelfSig, GetBitwiseAndAssignment, LHS, C, operator &=);
            $get_member_pointer(SelfSig, GetBitwiseOrAssignment, LHS, C, operator |=);
            $get_member_pointer(SelfSig, GetBitwiseXorAssignment, LHS, C, operator ^=);
            $get_member_pointer(SelfSig, GetBitwiseLeftShiftAssignment, LHS, C, operator <<=);
            $get_member_pointer(SelfSig, GetBitwiseRightShiftAssignment, LHS, C, operator >>=);

            //Increment/decrement operators
            $get_member_pointer(UnaryMutSig, GetPreIncrement, LHS, C, operator++);
            $get_member_pointer(UnaryMutSig, GetPreDecrement, LHS, C, operator--);

            $get_member_pointer(IntCopySig, GetPostIncrement, LHS, C, operator++);
            $get_member_pointer(IntCopySig, GetPostDecrement, LHS, C, operator--);


            //Arithmetic operators
            $get_member_pointer(UnaryImmutSig, GetUnaryPlus, LHS, C, operator+);
            $get_member_pointer(UnaryImmutSig, GetUnaryMinus, LHS, C, operator-);
            $get_member_pointer(CopySig, GetAddition, LHS, C, operator+);
            $get_member_pointer(CopySig, GetSubtraction, LHS, C, operator-);
            $get_member_pointer(CopySig, GetMultiplication, LHS, C, operator*);
            $get_member_pointer(CopySig, GetDivision, LHS, C, operator/);
            $get_member_pointer(CopySig, GetRemainder, LHS, C, operator%);
            $get_member_pointer(UnaryImmutSig, GetBitwiseNot, LHS, C, operator~);
            $get_member_pointer(CopySig, GetBitwiseAnd, LHS, C, operator&);
            $get_member_pointer(CopySig, GetBitwiseOr, LHS, C, operator|);
            $get_member_pointer(CopySig, GetBitwiseXor, LHS, C, operator^);
            $get_member_pointer(CopySig, GetBitwiseLeftShift, LHS, C, operator<<);
            $get_member_pointer(CopySig, GetBitwiseRightShift, LHS, C, operator>>);
            //Logical operators
            $get_member_pointer(UnaryLogicSig, GetNegation, LHS, C, operator!);
            $get_member_pointer(CmpSig, GetAnd, LHS, C, operator&&);
            $get_member_pointer(CmpSig, GetInclusiveOr, LHS, C, operator||);

            //Comparison operators
            $get_member_pointer(CmpSig, GetEqualTo, LHS, C, operator==);
            $get_member_pointer(CmpSig, GetNotEqualTo, LHS, C, operator!=);
            $get_member_pointer(CmpSig, GetLessThan, LHS, C, operator <);
            //$get_member_pointer(CmpSig, GetGreaterThan, LHS, C, operator > );
            $get_member_pointer(CmpSig, GetLessThanOrEqualTo, LHS, C, operator <=);
            $get_member_pointer(CmpSig, GetGreaterThanOrEqualTo, LHS, C, operator >=);

        };

        class Name
        {
        public:
            Name();
            Name(const Name& rhs);
            explicit Name(const char* szName);

            const Word::Handle& GetWordHandle() const;
            const Word::StringType& GetWordString() const;
            const Word::IdType& GetWordId() const;

            static void Trim(char* name);
        private:
            Word::Handle m_cWordHandle;
        };

        class SourceLocation
        {
        public:
            SourceLocation();
            SourceLocation(const SourceLocation& rhs);
            SourceLocation(const char* szFileName, const size_t knLineNunmber);

            operator bool() const;

            const char* GetFileName() const;
            const size_t GetLineNumber() const;
        private:
            const char* m_szFileName;
            const size_t m_knLineNumber;
        };


        #pragma region type
        class Type
        {
        public:
            template<typename T>
            class Name;

            template<typename T>
            struct Decay;

            typedef size_t IdType;
        private:

            static IdType GenId()
            {
                static IdType s_nId = 0;

                return s_nId++;
            }

            template<typename T>
            inline Type& Create() 
            {
                m_nTypeId = GetId<T>();
                m_nDecayTypeId = GetId<Decay<T>::Type>();
                m_szTypeName = Name<T>::GetName();
                m_bIsMethod = System::IsMethod<T>::value;

                if (!m_bIsMethod)
                    Namespace::TryParse(*this);

                return *this;
            }

        public:
            Type()
            : m_nWordId(~0)
            , m_nNamespaceId(~0)
            , m_nTypeId(~0)
            , m_nDecayTypeId(~0)
            , m_szTypeName(NULL)
            , m_bIsMethod(false)
            { }

            Type(const Type& rhs) 
            : m_nWordId(rhs.m_nWordId)
            , m_nNamespaceId(rhs.m_nNamespaceId)
            , m_nTypeId(rhs.m_nTypeId)
            , m_nDecayTypeId(rhs.m_nDecayTypeId)
            , m_szTypeName(rhs.m_szTypeName)
            , m_bIsMethod(rhs.m_bIsMethod)
            { }

            template<typename T>
            static IdType GetId() {
                static IdType s_nId = GenId();

                return s_nId;
            }
            
            template<typename T>
            static const char* GetName() {
                return Name<T>::GetName();
            }

            static std::vector<Type>& GetTypes() {
                static std::vector<Type> s_vecTypes;

                return s_vecTypes;
            }

            static const Type& Get(const IdType knTypeId) {
                return GetTypes()[knTypeId];
            }


            template<typename T>
            static Type& GetOrCreateType() {
                std::vector<Type>& vecTypes = GetTypes();
                static IdType nTypeId = GetId<T>();

                if (vecTypes.size() > nTypeId)
                    return vecTypes[nTypeId];

                vecTypes.resize(nTypeId+1);

                return vecTypes[nTypeId].Create<T>();
            }
            
            template<typename T>
            static Type& GetTypeOf(const T&) {
                return GetOrCreateType<T>();
            }
            template<typename T>
            static Type& GetTypeOf(const T*) {
                return GetOrCreateType<T>();
            }

            inline bool IsMethod() const {
                return m_bIsMethod;
            }

            inline IdType GetId() const {
                return m_nTypeId;
            }

            inline IdType GetDecayId() const {
                return m_nDecayTypeId;
            }

            inline const char* GetName() const {
                return m_szTypeName;
            }

            inline IdType GetWordId() const {
                return m_nWordId;
            }
            inline IdType GetNamespaceId() const {
                return m_nNamespaceId;
            }

            inline void Set(const IdType knWordId, const IdType knNamespaceId) {
                m_nWordId = knWordId;
                m_nNamespaceId = knNamespaceId;
            }
        private:
            IdType m_nWordId;
            IdType m_nNamespaceId;

            IdType m_nTypeId;
            IdType m_nDecayTypeId;
            const char* m_szTypeName;

            bool m_bIsMethod;
        };

        template<typename T>
        class Type::Name
        {
        public:
            static const char* GetName() {
                static const unsigned int FRONT_SIZE = sizeof("System::Reflection::Type::Name<") - 1u;
                static const unsigned int BACK_SIZE = sizeof(">::GetName") - 1u;
                static const size_t size = sizeof(__FUNCTION__) - FRONT_SIZE - BACK_SIZE;
                static char typeName[size + 6] = {};
                
                if (!typeName[0]) {
                    memcpy(typeName, __FUNCTION__ + FRONT_SIZE, size - 1u);
                    Reflection::Name::Trim(typeName);
                }

                return typeName;
            }
        };
        
        template<typename T>
        struct Type::Decay
        {
            typedef T Type;
        };
        template<typename T>
        struct Type::Decay<const T> : Type::Decay<T> { };
        template<typename T>
        struct Type::Decay<T*> : Type::Decay<T> { };
        template<typename T>
        struct Type::Decay<T&> : Type::Decay<T> { };

        #pragma endregion type

        #pragma region attribute

        class Attribute
        {
        public:
            class Link;

            class BaseBuilder;
            template<typename ClassType> class ClassBuilder;
            template<typename InputType, typename OutputType> class MemberBuilder;

            typedef size_t IdType;
            typedef std::list<Attribute> ListType;
            typedef std::vector<Attribute> VectorType;
            
            Attribute()
            : m_nTypeId(~0)
            , m_nAttributeId(~0)
            { }

            Attribute(const Attribute& rhs)
            : m_nTypeId(rhs.m_nTypeId)
            , m_nAttributeId(rhs.m_nAttributeId)
            { }

            template<class AttributeClass>
            static std::vector<AttributeClass>& GetAttributes() {
                static std::vector<AttributeClass> s_vecAttributes;

                return s_vecAttributes;
            }

            template<class AttributeClass>
            static AttributeClass& Get(const IdType& knId) {
                return GetAttributes<AttributeClass>()[knId];
            }

            template<class AttributeClass>
            Attribute& operator [] (const AttributeClass& attribute) {
                std::vector<AttributeClass>& vecAttributes = GetAttributes<AttributeClass>();
                
                m_nTypeId = Type::GetOrCreateType<AttributeClass>().GetId();
                m_nAttributeId = vecAttributes.size();

                vecAttributes.push_back(attribute);

                return *this; 
            }

            inline const Type& GetType() const {
                return Type::Get(m_nTypeId);
            }

            inline IdType GetId() const {
                return m_nAttributeId;
            }

            template<class AttributeClass>
            inline bool MatchType() const {
                return m_nTypeId == Type::GetId<AttributeClass>();
            }

            template<class AttributeClass>
            AttributeClass* GetPointer() const {
                if (MatchType<AttributeClass>())
                    return &GetAttributes<AttributeClass>()[m_nAttributeId];
                return NULL;
            }
            
            template<class AttributeClass>
            static AttributeClass* FindPointer(const VectorType& krvecAttributes, size_t& rnInOutIndex) {
                AttributeClass* ptrAttribute = NULL;

                for (size_t i = rnInOutIndex; !ptrAttribute && i < krvecAttributes.size(); ++i)
                    ptrAttribute = krvecAttributes[i].GetPointer<AttributeClass>();

                return ptrAttribute;
            }
            template<class AttributeClass>
            static AttributeClass* FindPointer(const VectorType& krvecAttributes) {
                size_t i = 0;

                return FindPointer<AttributeClass>(krvecAttributes, i);
            }
            
            template<typename OutputType, typename InputType> 
            static inline MemberBuilder<InputType, OutputType>& GetBuilder() {
                return MemberBuilder<InputType, OutputType>::GetInstance();
            }

            template<typename OutputType, typename InputType> 
            static inline MemberBuilder<InputType, OutputType>& SetBuilder(const InputType& rcInput) {
                return MemberBuilder<InputType, OutputType>::GetInstance().Begin(rcInput);
            }

            template<typename ClassType>
            static inline ClassBuilder<ClassType>& GetBuilder() {
                return ClassBuilder<ClassType>::GetInstance();
            }

            template<typename ClassType>
            static inline ClassBuilder<ClassType>& SetBuilder() {
                return ClassBuilder<ClassType>::GetInstance().Begin();
            }
        private:
            Type::IdType m_nTypeId;
            Attribute::IdType m_nAttributeId;
        };

        class Attribute::Link
        {
        public:
            typedef std::vector<Link> VectorType; 
            typedef std::map<Type::IdType, VectorType> MapType;

            Link()
            : m_nAttributeId(~0)
            , m_nOwnerId(~0)
            { }

            Link(const Link& rhs)
            : m_nAttributeId(rhs.m_nAttributeId)
            , m_nOwnerId(rhs.m_nOwnerId)
            { }

            explicit Link(IdType nAttributeId, IdType nOwnerId)
            : m_nAttributeId(nAttributeId)
            , m_nOwnerId(nOwnerId)
            { }

            template<typename AttributeClass>
            AttributeClass& GetAttribute() const {
                return GetAttributes<AttributeClass>()[m_nAttributeId];
            }

            template<class OwerClass> 
            const OwerClass& GetOwner() const {
                return OwerClass::Get(m_nOwnerId);
            }

            const IdType& GetId() const {
                return m_nAttributeId;
            }
        private:
            IdType m_nAttributeId;
            IdType m_nOwnerId;
        };

        class Attribute::BaseBuilder
        {
        public:
            BaseBuilder() : m_lstAttributes() { }
            BaseBuilder(const BaseBuilder& rhs) : m_lstAttributes(rhs.m_lstAttributes) { }


            template<class AttributeClass>
            Attribute* FindNext(size_t& rnOffset) const {
                for (; rnOffset < m_lstAttributes.size(); ++rnOffset) {
                    Attribute* pcAttribute = m_lstAttributes[rnOffset];

                    if (pcAttribute->MatchType<AttributeClass>())
                        return pcAttribute;
                }

                return NULL;
            }

            template<class AttributeClass>
            AttributeClass* FindNextPointer(size_t& rnOffset) const {
                Attribute* pcAttribute = FindNext<AttributeClass>(nOffset);

                if (pcAttribute)
                    return &GetAttributes<AttributeClass>()[pcAttribute->GetId()];

                return NULL;
            }

            template<class AttributeClass>
            IdType TakeId() {
                IdType nAttributeId = ~0;

                for (Attribute::ListType::iterator it = m_lstAttributes.begin(); it != m_lstAttributes.end();) {
                    if (it->MatchType<AttributeClass>()) {
                        nAttributeId = it->GetId();

                        it = m_lstAttributes.erase(it);
                    } else {
                        ++it;
                    }
                }

                return nAttributeId;
            }

            void TakeTo(Attribute::VectorType& rvecTarget) {
                rvecTarget.clear();

                for (Attribute::ListType::iterator it = m_lstAttributes.begin(); it != m_lstAttributes.end(); ++it) {
                    rvecTarget.push_back(*it);
                }
            }
        protected:
            void Reset() {
                m_lstAttributes.clear();
            }

            template<class AttributeClass>
            Attribute& Add(const AttributeClass& attribute) {
                m_lstAttributes.push_back(Attribute()[attribute]);

                return m_lstAttributes.back();
            }
        private:
            Attribute::ListType m_lstAttributes;
        };

        template<typename ClassType>
        class Attribute::ClassBuilder : public Attribute::BaseBuilder
        {
        public:
            template<class AttributeClass>
            struct Has
            {

                //template <typename C> $has(__test_bind_type, void (C::*)(const Type&), &C::BindType)
                template <typename C> $has_template(__test_bind_type, C, BindType)
                template <typename> $no(__test_bind_type)


                enum {
                    bind_type = $yes(__test_bind_type<AttributeClass>)
                };
            };

            ClassBuilder()
                : BaseBuilder()
            { }

            ClassBuilder(const ClassBuilder& rhs)
                : BaseBuilder(rhs)
            { }

            static ClassBuilder& GetInstance()
            {
                static ClassBuilder s_cBuilder;

                return s_cBuilder;
            }

            inline Type& GetType(){
                return Type::GetOrCreateType<ClassType>();
            }

            inline const Type& GetType() const {
                return Type::GetOrCreateType<ClassType>();
            }

            ClassBuilder& Begin() {
                Reset();

                return *this;
            }

            template<class AttributeClass>
            ClassBuilder& operator [] (const AttributeClass& attribute)
            {
                Attribute& rcAttribute = Add(attribute);

                BindType<AttributeClass>(rcAttribute);

                return *this;
            }


            ClassBuilder& End() {
                Class::Build(*this);

                return *this;
            }


            inline operator bool() {
                return Build(*this);
            }
        private:
            template<class AttributeClass>
            $enable_ift(Has<AttributeClass>::bind_type, void) BindType(const Attribute& krcAttribute) {
                krcAttribute.GetPointer<AttributeClass>()->BindType<ClassType>(GetType());
            }

            template<class AttributeClass>
            $enable_ift(!Has<AttributeClass>::bind_type, void) BindType(const Attribute&) { }
        };


        template<typename InputType, typename OutputType>
        class Attribute::MemberBuilder : public Attribute::BaseBuilder
        {
        public:
            template<class AttributeClass>
            struct Has
            {
                //template <typename C> $has(__test_bind_input, void (C::*)(const InputType&), &C::BindInput )
                template <typename C> $has_template(__test_bind_input, C, BindInput )
                template <typename> $no(__test_bind_input)

                template <typename C> $has(__test_bind_output, void (C::*)(const OutputType&), &C::BindOutput )
                template <typename C> $has_template(__test_bind_output, C, BindOutput )
                template <typename> $no(__test_bind_output)

                enum {
                    bind_input = $yes(__test_bind_input<AttributeClass>),
                    bind_output = $yes(__test_bind_output<AttributeClass>)
                };
            };

            MemberBuilder()
            : BaseBuilder()
            , m_input()
            , m_nOutputId(~0)
            { }

            MemberBuilder(const MemberBuilder& rhs)
            : BaseBuilder(rhs)
            , m_input(rhs.m_input)
            , m_nOutputId(rhs.m_nOutputId)
            { }

            static MemberBuilder& GetInstance() {
                static MemberBuilder s_cBuilder;

                return s_cBuilder;
            }

            inline const InputType& GetInput() const {
                return m_input;
            }

            inline const OutputType& GetOutput() const {
                return OutputType::Get(m_nOutputId);
            }

            inline const IdType& GetOutputId() const {
                return m_nOutputId;
            }

            MemberBuilder& Begin(const InputType& krInput) {
                m_input = krInput;
                m_nOutputId = OutputType::Create(m_input);
                
                Reset();

                return *this;
            }

            template<class AttributeClass>
            MemberBuilder& operator [] (const AttributeClass& attribute)
            {
                Attribute& rcAttribute = Add(attribute);

                BindInput<AttributeClass>(rcAttribute);
                BindOutput<AttributeClass>(rcAttribute);

                return *this; 
            }
            
            
            MemberBuilder& End() {
                OutputType::Build(this);

                return *this;
            }

            inline operator bool() {
                return Build(*this);
            }
        private:
            template<class AttributeClass>
            $enable_ift(Has<AttributeClass>::bind_input, void) BindInput(const Attribute& krcAttribute) {
                krcAttribute.GetPointer<AttributeClass>()->BindInput(GetInput());
            }

            template<class AttributeClass>
            $enable_ift(!Has<AttributeClass>::bind_input, void) BindInput(const Attribute&) {}

            template<class AttributeClass>
            $enable_ift(Has<AttributeClass>::bind_output, void) BindOutput(const Attribute& krcAttribute) {
                krcAttribute.GetPointer<AttributeClass>()->BindOutput(GetOutput());
            }

            template<class AttributeClass>
            $enable_ift(!Has<AttributeClass>::bind_output, void) BindOutput(const Attribute&) {}

        private:
            InputType m_input;
            IdType m_nOutputId;
        };

        #pragma endregion attribute

        #pragma region class

        class MembersInfo
        {
        public:
            typedef size_t IdType;
            typedef std::map<Word::Handle, IdType> MapType;
        private:
            struct Node
            {
                MapType m_mapByName;
                Attribute::Link::MapType m_mapByAttributes;

                Node();
                Node(const Node& rhs);

                void Add(const Attribute::VectorType& krvecAttribues, const Word::Handle& krcName, IdType nOwnerId);

                const Type::IdType* GetById(Word::IdType nNameId) const;
                const Attribute::Link::VectorType* GetByAttribute(const Type& krcAttributeType) const;
            };
        public:
            MembersInfo();
            MembersInfo(const MembersInfo& rhs);

            void Add(const Attribute::VectorType& krvecAttribues, const Word::Handle& krcName, const Type& krcOwnerType, IdType nOwnerId);
            
            const MapType& GetWithName() const;
            const MapType* GetWithName(const Type& krcOwnerType) const;

            const IdType* GetById(Word::IdType nNameId) const;
            const IdType* GetById(const Type& krcOwnerType, Word::IdType nNameId) const;

            const Attribute::Link::VectorType* GetByAttribute(const Type& krcOwnerType, const Type& krcAttributeType) const;
            const Attribute::Link::VectorType* GetByAttribute(const Type& krcAttributeType) const;
        private:
            const Node* GetByType(const Type& krcOwnerType) const;
        private:
            Node m_cFlatNode;
            std::map<Type::IdType, Node> m_mapByOwnerType;
        };


        class Method
        {
            static std::vector<Method>& GetMethods()
            {
                static std::vector<Method> s_vecMethods;

                return s_vecMethods;
            }

            template<typename T>
            static std::vector<T>& GetMethods()
            {
                static std::vector<T> s_vecMethods;

                return s_vecMethods;
            }
        public:
            typedef size_t IdType;
            typedef std::vector<Method> VectorType;
            typedef std::vector<IdType> IdVectorType;
            
            Method()
            : m_nTypeId(~0)
            , m_nMethodId(~0)
            , m_nPointerId(~0)
            , m_nNameId(~0)
            , m_vecAttributes()
            { }

            Method(const Method& rhs)
            : m_nTypeId(rhs.m_nTypeId)
            , m_nMethodId(rhs.m_nMethodId)
            , m_nPointerId(rhs.m_nPointerId)
            , m_nNameId(rhs.m_nNameId)
            , m_vecAttributes(rhs.m_vecAttributes)
            { }
            
            template<typename T>
            static IdType Create(T method) 
            {
                std::vector<T>& rvecMethodsOfType = GetMethods<T>();
                std::vector<Method>& rvecMethods = GetMethods();

                IdType nId = rvecMethods.size();
                
                rvecMethods.push_back();

                Method& rcMethod = rvecMethods.back();

                rcMethod.m_nTypeId = Type::GetOrCreateType<T>().GetId();
                rcMethod.m_nMethodId = nId;
                rcMethod.m_nPointerId = rvecMethodsOfType.size();

                rvecMethodsOfType.push_back(method);

                return nId;
            }

            template<typename T>
            static void Build(Attribute::MemberBuilder<T, Method>* pcBuilder)
            {
                Method& rcMethod = GetMethods()[pcBuilder->GetOutputId()];

                rcMethod.m_nNameId = pcBuilder->TakeId<Name>();

                pcBuilder->TakeTo(rcMethod.m_vecAttributes);
            }

            static const Method& Get(IdType nMethodId) {
                return GetMethods()[nMethodId];
            }

            static const Method* Get(const IdType* kptrnMethodId) {
                if (kptrnMethodId == NULL)
                    return NULL;
                
                return &GetMethods()[*kptrnMethodId];
            }
            
            template<typename T>
            inline const T& GetMethod() const {
                return GetMethods<T>()[m_nPointerId];
            }

            inline IdType GetId() const {
                return m_nMethodId;
            }

            inline const Name& GetName() const {
                return Attribute::GetAttributes<Name>()[m_nNameId];
            }

            inline const Type& GetType() const {
                return Type::GetTypes()[m_nTypeId];
            }

            inline const Attribute::VectorType& GetAttributes() const {
                return m_vecAttributes;
            }
        private:
            Type::IdType m_nTypeId;
            Method::IdType m_nMethodId;
            Method::IdType m_nPointerId;
            Attribute::IdType m_nNameId;
            Attribute::VectorType m_vecAttributes;
        };

        class Property
        {
        public:
            typedef size_t IdType;
            typedef std::vector<Property> VectorType;

            Property()
            : m_nTypeId(~0)
            , m_nNameId(~0)
            , m_vecAttributes()
            { }

            Property(const Property& rhs)
            : m_nTypeId(rhs.m_nTypeId)
            , m_nNameId(rhs.m_nNameId)
            , m_vecAttributes(rhs.m_vecAttributes)
            { }

            template<typename T>
            static IdType Create(T property) 
            {
                return ~0;
            }

            static const Property* Get(const IdType* kptrnId) {
                return NULL;
            }

            inline const Name& GetName() const {
                return Attribute::GetAttributes<Name>()[m_nNameId];
            }

            inline const Type& GetType() const {
                return Type::GetTypes()[m_nTypeId];
            }

            inline const Attribute::VectorType& GetAttributes() const {
                return m_vecAttributes;
            }

        private:
            Type::IdType m_nTypeId;
            Attribute::IdType m_nNameId;
            Attribute::VectorType m_vecAttributes;
        };

        class Constant
        {
            static std::vector<Constant>& GetConstants() {
                static std::vector<Constant> s_vecConstants;

                return s_vecConstants;
            }

        public:
            typedef size_t IdType;

            Constant()
                : m_nId(~0)
                , m_nValueId(~0)
                , m_nNameId(~0)
                , m_vecAttributes()
            { }

            Constant(const Constant& rhs)
                : m_nId(rhs.m_nId)
                , m_nValueId(rhs.m_nValueId)
                , m_nNameId(rhs.m_nNameId)
                , m_vecAttributes(rhs.m_vecAttributes)
            { }


            template<typename ValueType>
            static std::vector<ValueType>& GetValues() {
                static std::vector<ValueType> s_vecValues;

                return s_vecValues;
            }

            template<typename ValueType>
            static IdType Create(const ValueType& krValue)
            {
                std::vector<ValueType>& rvecValues = GetValues<ValueType>();
                std::vector<Constant>& rvecConstants = GetConstants();

                IdType nId = rvecConstants.size();

                rvecConstants.push_back();

                Constant& rcConstant = rvecConstants.back();

                rcConstant.m_nId = nId;
                rcConstant.m_nValueId = rvecValues.size();

                rvecValues.push_back(krValue);

                return nId;
            }

            template<typename T>
            static void Build(Attribute::MemberBuilder<T, Constant>* pcBuilder)
            {
                Constant& rcConstant = GetConstants()[pcBuilder->GetOutputId()];

                rcConstant.m_nNameId = pcBuilder->TakeId<Name>();

                pcBuilder->TakeTo(rcConstant.m_vecAttributes);
            }

            static const Constant* Get(const IdType* kpnId) {
                if (kpnId == NULL)
                    return NULL;

                return &GetConstants()[*kpnId];
            }

            static const Constant& Get(IdType nId) {
                return GetConstants()[nId];
            }

            inline IdType GetId() const {
                return m_nId;
            }

            inline IdType GetValueId() const {
                return m_nValueId;
            }

            template<typename T>
            inline const T& GetValue() const {
                return GetValues<T>()[m_nValueId];
            }

            inline const Name& GetName() const {
                return Attribute::GetAttributes<Name>()[m_nNameId];
            }

            inline const Attribute::VectorType& GetAttributes() const {
                return m_vecAttributes;
            }
        private:
            IdType m_nId;
            IdType m_nValueId;
            Attribute::IdType m_nNameId;
            Attribute::VectorType m_vecAttributes;
        };

        class Enum
        {
        public:
            typedef size_t IdType;
            typedef std::map<Type::IdType, Enum> MapType;

            Enum()
                : m_nTypeId(~0)
                , m_nNameId(~0)
                , m_vecAttributes()
                , m_cConstants()
            { }

            Enum(const Enum& rhs)
                : m_nTypeId(rhs.m_nTypeId)
                , m_nNameId(rhs.m_nNameId)
                , m_vecAttributes(rhs.m_vecAttributes)
                , m_cConstants(rhs.m_cConstants)
            { }

            static MapType& GetEnums()
            {
                static MapType s_mapEnums;

                return s_mapEnums;
            }

            static Enum* GetEnum(const Type::IdType knClassType)
            {
                MapType& rmapClasses = GetEnums();
                MapType::iterator it = rmapClasses.find(knClassType);

                if (it == rmapClasses.end())
                    return NULL;

                return &it->second;
            }

            static const Enum* GetEnum(const Type& krcEnumType) {
                return GetEnum(krcEnumType.GetDecayId());
            }

            static Enum* Get(const Type::IdType* kpnEnumType)
            {
                if (kpnEnumType == NULL)
                    return NULL;

                return GetEnum(*kpnEnumType);
            }

            template<class EnumType>
            static const Enum* Get() {
                return GetEnum(Type::GetOrCreateType<EnumType>());
            }

            template<class EnumType>
            static Enum& GetOrCreate()
            {
                typedef Type::Decay<EnumType>::Type DecayType;

                Type& krcEnumType = Type::GetOrCreateType<DecayType>();

                MapType& rmapEnums = GetEnums();
                MapType::iterator it = rmapEnums.find(krcEnumType.GetDecayId());

                if (it == rmapEnums.end())
                    it = rmapEnums.insert(std::make_pair(krcEnumType.GetDecayId(), Enum().Create(krcEnumType))).first;

                return it->second;
            }

            Enum& Create(Type& rcEnumType);


            template<typename T>
            static void Build(Attribute::MemberBuilder<T, Enum>* pcBuilder)
            {
                Enum& rcEnum = GetOrCreate<T>();

                rcEnum.m_nNameId = pcBuilder->TakeId<Name>();

                pcBuilder->TakeId<Registry::Class::Member>();

                pcBuilder->TakeTo(rcEnum.m_vecAttributes);
            }

            template<typename ValueType>
            Enum& Build(const Attribute::MemberBuilder<ValueType, Constant>& krcBuilder)
            {
                Constant::IdType nConstantId = krcBuilder.GetOutputId();
                const Constant& krcConstant = Constant::Get(nConstantId);
                const Type& krcType = Type::GetOrCreateType<ValueType>();
                const Word::Handle kcName(krcConstant.GetName().GetWordId());

                const Attribute::VectorType& krvecAttribues = krcConstant.GetAttributes();

                m_cConstants.Add(krvecAttribues, kcName, krcType, nConstantId);

                return *this;
            }


            inline Type& GetType() {
                return Type::GetTypes()[m_nTypeId];
            }
            inline const Type& GetType() const {
                return Type::GetTypes()[m_nTypeId];
            }

            inline const Name& GetName() const {
                return Attribute::GetAttributes<Name>()[m_nNameId];
            }

            inline const Attribute::VectorType& GetAttributes() const {
                return m_vecAttributes;
            }

            inline const MembersInfo& GetConstants() const {
                return m_cConstants;
            }

            inline const Constant* FindConstant(const Word::Handle& krcName) const {
                return Constant::Get(m_cConstants.GetById(krcName.GetId()));
            }
        private:
            Type::IdType m_nTypeId;
            Attribute::IdType m_nNameId;
            Attribute::VectorType m_vecAttributes;

            MembersInfo m_cConstants;
        };


        class Class
        {

        public:
            typedef std::vector<Type::IdType> IdVectorType;
            typedef std::vector<const Class*> PtrVectorType;
            typedef std::map<Type::IdType, Class> MapType;

            Class()
            : m_nTypeId(~0)
            , m_cEnums()
            , m_cMethods()
            , m_cProperties()
            , m_vecAttributes()
            { }

            Class(const Class& rhs)
            : m_nTypeId(rhs.m_nTypeId)
            , m_cEnums(rhs.m_cEnums)
            , m_cMethods(rhs.m_cMethods)
            , m_cProperties(rhs.m_cProperties)
            , m_vecAttributes(rhs.m_vecAttributes)
            { }

            static MapType& GetClasses()
            {
                static MapType s_mapClasses;

                return s_mapClasses;
            }

            static Class* GetClass(const Type::IdType knClassType)
            {
                MapType& rmapClasses = GetClasses();
                MapType::iterator it = rmapClasses.find(knClassType);

                if (it == rmapClasses.end())
                    return NULL;

                return &it->second;
            }

            static const Class* GetClass(const Type& krcClassType) {
                return GetClass(krcClassType.GetDecayId());
            }

            template<class ClassType>
            static Class& GetOrCreate()
            {
                typedef Type::Decay<ClassType>::Type DecayType;

                Type& krcClassType = Type::GetOrCreateType<DecayType>();

                MapType& rmapClasses = GetClasses();
                MapType::iterator it = rmapClasses.find(krcClassType.GetDecayId());

                if (it == rmapClasses.end())
                    it =  rmapClasses.insert(std::make_pair(krcClassType.GetDecayId(), Class().Create(krcClassType))).first;

                return it->second;
            }

            Class& Create(Type& krcClassType);

            template<typename T>
            static void Build(Attribute::ClassBuilder<T>& rcBuilder)
            {
                Class& rcClass = GetOrCreate<T>();

                rcBuilder.TakeTo(rcClass.m_vecAttributes);
            }

            template<typename MethodType>
            Class& Build(Attribute::MemberBuilder<MethodType, Method>& rcBuilder)
            {
                Method::IdType nMethodId = rcBuilder.GetOutputId();
                const Method& krcMethod = Method::Get(nMethodId);

                const Type& krcType = krcMethod.GetType();
                const Attribute::VectorType& krvecAttribues = krcMethod.GetAttributes();

                m_cMethods.Add(krvecAttribues, krcMethod.GetName().GetWordId(), krcType, nMethodId);

                return *this;
            }

            template<typename EnumType>
            Class& Build(Attribute::MemberBuilder<EnumType, Enum>& rcBuilder)
            {
                const Enum& krcEnum = Enum::GetOrCreate<EnumType>();
                const Type& krcType = krcEnum.GetType();

                const Attribute::VectorType& krvecAttribues = krcEnum.GetAttributes();

                m_cEnums.Add(krvecAttribues, krcEnum.GetName().GetWordId(), krcType, krcType.GetDecayId());

                return *this;
            }

            inline Type& GetType() {
                return Type::GetTypes()[m_nTypeId];
            }
            inline const Type& GetType() const {
                return Type::GetTypes()[m_nTypeId];
            }
            
            
            const Method* GetMethod(Word::IdType nMethodNameId) const {
                return Method::Get(m_cMethods.GetById(nMethodNameId));
            }
            const Method* GetMethod(const Type& krcMethodType, Word::IdType nMethodNameId) const {
                return Method::Get(m_cMethods.GetById(krcMethodType, nMethodNameId));
            }
            const Attribute::Link::VectorType* GetMethods(const Type& krcMethodType, const Type& krcAttributeType) const {
                return m_cMethods.GetByAttribute(krcMethodType, krcAttributeType);
            }
            const Attribute::Link::VectorType* GetAttributedMethods(const Type& krcAttributeType) const {
                return m_cMethods.GetByAttribute(krcAttributeType);
            }
            const MembersInfo::MapType* GetNamedMethods(const Type& krcMethodType) const {
                return m_cMethods.GetWithName(krcMethodType);
            }
            const MembersInfo::MapType& GetNamedMethods() const {
                return m_cMethods.GetWithName();
            }
            
            const Property* GetProperty(const Type& krcPropertyType, Word::IdType nPropertyNameId) const {
                return Property::Get(m_cProperties.GetById(krcPropertyType, nPropertyNameId));
            }
            const Attribute::Link::VectorType* GetProperties(const Type& krcPropertyType, const Type& krcAttributeType) const {
                return m_cProperties.GetByAttribute(krcPropertyType, krcAttributeType);
            }
            const Attribute::Link::VectorType* GetAttributedProperties(const Type& krcPropertyType) const {
                return m_cProperties.GetByAttribute(krcPropertyType);
            }
            const MembersInfo::MapType* GetNamedProperties(const Type& krcPropertyType) const {
                return m_cProperties.GetWithName(krcPropertyType);
            }
            const MembersInfo::MapType& GetNamedProperties() const {
                return m_cProperties.GetWithName();
            }

            inline const Attribute::VectorType& GetAttributes() const {
                return m_vecAttributes;
            }

            inline const Enum* GetEnum(const Word::IdType knWordId) const {
                return Enum::Get(m_cEnums.GetById(knWordId));
            }
        private:
            Type::IdType m_nTypeId;
            MembersInfo m_cEnums;
            MembersInfo m_cMethods;
            MembersInfo m_cProperties;
            Attribute::VectorType m_vecAttributes;
        };

        #pragma endregion class

        class Namespace
        {
        public:
            typedef size_t IdType;
            typedef std::vector<IdType> IdVectorType;
            typedef std::map<Word::Handle, IdType> MapType;
            typedef std::vector<Namespace> VectorType;

            Namespace();
            Namespace(const Namespace& rhs);

            static IdType FindId(const Word::Handle::VectorType& krvecWordIds);
            static Namespace& Get(const IdType knId = 0);

            const IdType GetId() const;
            const IdType GetParentId() const;
            const Word::Handle& GetWord() const;

            const Type::IdType GetTypeId(const Word::Handle& krcWord) const;
            const IdType GetChildId(const Word::Handle& krcWord) const;

            template<typename EnumType>
            Namespace& Build(Attribute::MemberBuilder<EnumType, Enum>& rcBuilder)
            {
                const Enum& krcEnum = Enum::GetOrCreate<EnumType>();
                const Type& krcType = krcEnum.GetType();

                const Attribute::VectorType& krvecAttribues = krcEnum.GetAttributes();

                m_cEnums.Add(krvecAttribues, krcEnum.GetName().GetWordId(), krcType, krcType.GetDecayId());

                return *this;
            }

            Namespace& Set(const Word::Handle& krcWord, const IdType knId, const IdType knParentId);
            Namespace& SetType(const Word::Handle& krcWord, Type& rcType);
            
            const IdType GetOrCreateChildId(const Word::Handle& krcWord);


            inline const Enum* FindEnum(const Word::IdType knWordId) const {
                return Enum::Get(m_cEnums.GetById(knWordId));
            }

            template<typename AttributeClass>
            inline AttributeClass* FindAttribute(const Word::IdType knTypeWordId) const {

                const Type::IdType knTypeId = GetTypeId(knTypeWordId);

                if (knTypeId == ~0)
                    return NULL;

                const Type& krcType = Type::Get(knTypeId);
                const Class* kpcClass = Class::GetClass(krcType);

                if (kpcClass == NULL)
                    return NULL;

                const Attribute::VectorType& krvecAttributes = kpcClass->GetAttributes();

                return Attribute::FindPointer<AttributeClass>(krvecAttributes);
            }

            template<typename AttributeClass>
            inline AttributeClass* FindMethodAttribute(const Word::IdType knMethodWordId, Method::IdType& rnOutMethodId) const {
                for (MapType::const_iterator type = m_mapTypeIds.begin(); type != m_mapTypeIds.end(); ++type) {
                    const Type& krcType = Type::Get(type->second);
                    const Class* kpcClass = Class::GetClass(krcType);

                    if (kpcClass == NULL)
                        continue;

                    const Method* kpcMethod = kpcClass->GetMethod(knMethodWordId);

                    if (kpcMethod == NULL)
                        continue;
                    
                    AttributeClass* pcAttribute = Attribute::FindPointer<AttributeClass>(kpcMethod->GetAttributes());

                    if (pcAttribute == NULL)
                        continue;

                    rnOutMethodId = kpcMethod->GetId();

                    return pcAttribute;
                }

                return NULL;
            }

            template<typename AttributeClass, typename VisitorClass>
            inline void VisitMethodAttributes(VisitorClass& rcVisitor) const {
                const Type& krcAttributeType = Type::GetOrCreateType<AttributeClass>();

                for (MapType::const_iterator type = m_mapTypeIds.begin(); type != m_mapTypeIds.end(); ++type) {
                    const Type& krcType = Type::Get(type->second);
                    const Class* kpcClass = Class::GetClass(krcType);

                    if (kpcClass == NULL)
                        continue;

                    const Attribute::Link::VectorType* kpvecAttributes = kpcClass->GetAttributedMethods(krcAttributeType);

                    if (kpvecAttributes == NULL)
                        continue;

                    for (Reflection::Attribute::Link::VectorType::const_iterator link = kpvecAttributes->begin(); link != kpvecAttributes->end(); ++link) {
                        const AttributeClass& krcAttribute = link->GetAttribute<AttributeClass>();
                        const Method& krcMethod = link->GetOwner<Reflection::Method>();

                        rcVisitor(krcType, krcMethod, krcAttribute);
                    }
                }
            }

            template<typename VisitorClass>
            inline void VisitTypes(VisitorClass& rcVisitor) const {
                for (MapType::const_iterator it = m_mapTypeIds.begin(); it != m_mapTypeIds.end(); ++it) {
                    const Type& krcType = Type::Get(it->second);

                    rcVisitor(krcType);
                }   
            }

            template<typename VisitorClass>
            inline void VisitChilds(VisitorClass& rcVisitor) const {
                for (MapType::const_iterator it = m_mapChildIds.begin(); it != m_mapChildIds.end(); ++it) {
                    const Namespace& krcNamespace = Get(it->second);

                    rcVisitor(krcNamespace);
                }
            }

            static bool TryParse(Type& rcType, IdType* pnOutId = NULL);
            static bool TryParse(Visitor::Slices& rcSlices, Type::IdType* pnOutTypeId = NULL, IdType* pnOutNamespaceId = NULL);
        private:
            static VectorType& GetNamespaces();
        private:
            Word::Handle m_cWord;
            IdType m_nId;
            IdType m_nParentId;
            MapType m_mapChildIds;
            MapType m_mapTypeIds;
            MembersInfo m_cEnums;
        };

        class Registry
        {

        public:
            Registry(...) { }

            inline const Registry& operator[] (bool) const { return *this; }

            inline operator bool() const { return true; }
        };


        template<typename T>
        static typename System::SFINE::EnableIf<System::IsClass<T>::value,
            Attribute::ClassBuilder<T>&
        >::type Builder(const char* szName) {
            return Attribute::SetBuilder<T>()[Name(szName)];
        }

        template<typename T>
        static typename System::SFINE::EnableIf<System::IsEnum<T>::value,
            Attribute::MemberBuilder<T, Reflection::Constant>& 
        >::type Builder(const char* szName, const T knValue ) {
            return Attribute::SetBuilder<Reflection::Constant>(knValue)[Name(szName)];
        }

        template<typename T>
        static typename System::SFINE::EnableIf<System::IsMethod<T>::value,
            Attribute::MemberBuilder<T, Reflection::Method>&
        >::type Builder(const char* szName, T input) {
            return Attribute::SetBuilder<Reflection::Method>(input)[Name(szName)];
        }

        template<typename T>
        static typename System::SFINE::EnableIf<System::IsField<T>::value,
            Attribute::MemberBuilder<T, Reflection::Property>&
        >::type Builder(const char* szName, T input) {
            return Attribute::SetBuilder<Reflection::Property>(input)[Name(szName)];
        }


        template<typename ClassType>
        static typename System::SFINE::EnableIf<System::IsClass<ClassType>::value, bool>::type Build(
            Attribute::ClassBuilder<ClassType>& rcBuilder
        ) {
            rcBuilder.End();

            return true;
        }

        template<typename EnumType>
        static typename System::SFINE::EnableIf<System::IsEnum<EnumType>::value, bool>::type Build(
            Attribute::MemberBuilder<EnumType, Reflection::Constant>& rcBuilder
        ) {
            rcBuilder.End();

            Reflection::Enum::GetOrCreate<EnumType>().Build(rcBuilder);

            return true;
        }

        template<typename InputType, typename OutputType>
        static typename System::SFINE::EnableIf<System::IsMember<InputType>::value, bool>::type Build(
            Attribute::MemberBuilder<InputType, OutputType>& rcBuilder
        ) {
            rcBuilder.End();

            Reflection::Class::GetOrCreate<typename MemberClass<InputType>::type>().Build(rcBuilder);

            return true;
        }
    }
}


#define $concat_impl( x, y ) x##y
#define $concat( x, y ) $concat_impl( x, y )

#define $type(_type) System::Reflection::Type::GetOrCreateType< _type >()
#define $typeof(_instance) System::Reflection::Type::GetTypeOf(_instance)
#define $typeid(_type) System::Reflection::Type::GetId< _type >()
#define $typename(_type) System::Reflection::Type::GetName< _type >()

#define $classof(_instance) System::Reflection::Class::GetClass($typeof(_instance))

#define $source_location System::Reflection::SourceLocation(__FILE__, __LINE__)
#define $name(_name) System::Reflection::Name(_name)

#define $builder System::Reflection::Builder

#define $registry_thing(_thing) $concat(static bool __system_reflection_registry_,__COUNTER__)
#define $registry $registry_thing(__) = System::Reflection::Registry

#define $method(_attributes, _return, _class, _method) $registry()[$builder(#_method, &_class::_method)[$source_location]_attributes];_return _class::_method
#define $class(_attributes, _class) class _class; $registry()[$builder<_class>(#_class)[$source_location][System::Data::Factory()]_attributes]; class _class
