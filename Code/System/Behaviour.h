#pragma once

#include "Runner.h"

namespace System
{
    namespace Behaviour
    {
        class Counter
        {
        public:
            Counter();
            Counter(const Counter& rhs);

            size_t Get() const;

            void Next();
            void Reset();

            bool IsValid(const Data::Node& krcNode) const;
            bool IsValid(const size_t knLimit) const;
        private:
            size_t m_nCurrent;
        }; 

        class Randomizer
        {
        public:
            Randomizer();
            Randomizer(const Randomizer& rhs);

            size_t Get() const;
            void Reset(const Data::Node& krcNode);
            bool IsValid(const Data::Node& krcNode) const;
        private:
            size_t m_nCurrent;
            bool m_bAreValidWeights;
        };
        
        $class(,Core)
        {
        public:
            Runner::EStatus Run(const Word::Handle& krcNameWord);
            Runner::EStatus Fallback();
            Runner::EStatus Not();
            Runner::EStatus Parallel();
            Runner::EStatus Repeat(const size_t knLimit);
            Runner::EStatus Race();
            Runner::EStatus Random();
            Runner::EStatus Sequence();
            Runner::EStatus While();
            Runner::EStatus If();
            Runner::EStatus Mute();
            Runner::EStatus Fail();
            Runner::EStatus Success();
        };

    }
}