#pragma once

#include <string>
#include <vector>
#include <stack>
#include <map>

#pragma warning( disable : 4068)
#pragma warning( disable : 4305)

namespace System
{
    namespace Word
    {
        typedef size_t IdType;
        typedef const char* PointerType;
        typedef std::string StringType;
        typedef std::vector<PointerType> PointerVectorType;
        typedef std::vector<IdType> IdVectorType;
        typedef std::stack<IdType> IdStackType;
        typedef std::vector<StringType> StringVectorType;
        typedef std::map<StringType, IdType> StringMapType;

        class Table
        {
        public:
            static Table& Get();

            IdType GetOrCreateId(const StringType& krString);

            bool HasWithId(const IdType knWordId) const;
            const StringType& GetById(const IdType knWordId) const;

            inline StringMapType& GetMap() {
                return m_mapLookup;
            }

            inline StringVectorType& GetVector() {
                return m_vecLoopup;
            };
        private:
            StringMapType m_mapLookup;
            StringVectorType m_vecLoopup;
        };

        class Handle
        {
        public:
            typedef std::vector<Handle> VectorType;

            Handle();
            Handle(const Handle& rhs);
            Handle(const IdType knWordId);
            Handle(const StringType& krstrWord);
            Handle(PointerType kpstrWord);

            void SetId(const IdType knWordId);
            void SetString(const StringType& krstrWord);

            operator bool() const;

            bool operator == (const Handle& rhs) const;
            bool operator != (const Handle& rhs) const;
            bool operator >= (const Handle& rhs) const;
            bool operator <= (const Handle& rhs) const;
            bool operator > (const Handle& rhs) const;
            bool operator < (const Handle& rhs) const;

            const IdType& GetId() const;
            const StringType& GetString() const;
            PointerType c_str() const;
        private:
            IdType m_nWordId;
        };

        inline IdType Id(const StringType& krString) {
            return Table::Get().GetOrCreateId(krString);
        }

        inline const StringType& String(const IdType knId) {
            return Table::Get().GetById(knId);
        }

        inline bool Has(const IdType knId) {
            return Table::Get().HasWithId(knId);
        }
    }
}