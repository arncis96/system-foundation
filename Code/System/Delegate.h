#pragma once

namespace System
{
    template<typename ToType, typename FromType>
    inline ToType Cast(FromType from) {
        union
        {
            FromType m_From;
            ToType m_To;
        } cast;

        cast.m_From = from;

        return cast.m_To;
    }


    template<typename S, typename M, typename A>
    class FuncBase
    {
    protected:
        typedef FuncBase<S, M, A> BaseType;
    public:
        typedef S SignatureType;

        FuncBase()
            : m_pTarget(NULL)
            , m_nTargetIndex(~0)
            , m_kpMethod(NULL)
        { }

        FuncBase(const FuncBase& rhs)
            : m_pTarget(rhs.m_pTarget)
            , m_nTargetIndex(rhs.m_nTargetIndex)
            , m_kpMethod(rhs.m_kpMethod)
        { }

        template <typename TargetType>
        inline void Set(TargetType* pTarget) {
            m_pTarget = pTarget;
            m_nTargetIndex = ~0;
            m_kpMethod = &A::InvokeFromInstanceFor<TargetType>;
        }

        template <typename ContainerType>
        inline void Set(ContainerType* pContainer, const size_t knIndex) {
            m_pTarget = pContainer;
            m_nTargetIndex = knIndex;
            m_kpMethod = &A::InvokeFromContainerFor<ContainerType>;
        }

        inline FuncBase& operator = (const FuncBase& rhs) {
            m_pTarget = rhs.m_pTarget;
            m_nTargetIndex = rhs.m_nTargetIndex;
            m_kpMethod = rhs.m_kpMethod;

            return *this;
        }

        inline operator bool() const {
            return m_kpMethod != NULL;
        }
    protected:
        void* m_pTarget;
        size_t m_nTargetIndex;
        M m_kpMethod;
    };

#define $_(_exp) _exp,
#define _$(_exp) _exp
#define $class_for_func(_tmp, _params, _args) \
class Func<R(*)(_tmp)> : public FuncBase< R(*)(_tmp), R(Func< R(*)(_tmp) >::*)(_tmp) const, Func<R(*)(_tmp)> > { \
    friend class BaseType; \
public:\
    inline R operator () (_params) const {\
        return (this->*m_kpMethod)(_args);\
    } \
    inline R Invoke(_params) const { \
        return (this->*m_kpMethod)(_args); \
    }\
    protected:\
        template<typename TargetType>\
        R InvokeFromInstanceFor(_params) const {\
            return static_cast<TargetType*>(m_pTarget)->operator() (_args);\
        }\
        template<typename ContainerType>\
        R InvokeFromContainerFor(_params) const {\
            return static_cast<ContainerType*>(m_pTarget)->operator[] (m_nTargetIndex)(_args);\
        }\
    }

    template<typename S>
    class Func;

    template<typename R>
    $class_for_func(,,);

    template<typename R, typename T1>
    $class_for_func(
        _$(T1),
        _$(T1 a1),
        _$(a1)
    );
    template<typename R, typename T1, typename T2>
    $class_for_func(
        $_(T1)_$(T2),
        $_(T1 a1)_$(T2 a2),
        $_(a1)_$(a2)
    );

    template<typename R, typename T1, typename T2, typename T3>
    $class_for_func(
        $_(T1)$_(T2)_$(T3),
        $_(T1 a1)$_(T2 a2)_$(T3 a3),
        $_(a1)$_(a2)_$(a3)
    );

    template<typename R, typename T1, typename T2, typename T3, typename T4>
    $class_for_func(
        $_(T1)$_(T2)$_(T3)_$(T4), 
        $_(T1 a1)$_(T2 a2)$_(T3 a3)_$(T4 a4),
        $_(a1)$_(a2)$_(a3)_$(a4)
    );

    template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
    $class_for_func(
        $_(T1)$_(T2)$_(T3)$_(T4)_$(T5), 
        $_(T1 a1)$_(T2 a2)$_(T3 a3)$_(T4 a4)_$(T5 a5),
        $_(a1)$_(a2)$_(a3)$_(a4)_$(a5)
    );

    template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
    $class_for_func(
        $_(T1)$_(T2)$_(T3)$_(T4)$_(T5)_$(T6),
        $_(T1 a1)$_(T2 a2)$_(T3 a3)$_(T4 a4)$_(T5 a5)_$(T6 a6),
        $_(a1)$_(a2)$_(a3)$_(a4)$_(a5)_$(a6)
    );

    template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
    $class_for_func(
        $_(T1)$_(T2)$_(T3)$_(T4)$_(T5)$_(T6)_$(T7),
        $_(T1 a1)$_(T2 a2)$_(T3 a3)$_(T4 a4)$_(T5 a5)$_(T6 a6)_$(T7 a7),
        $_(a1)$_(a2)$_(a3)$_(a4)$_(a5)$_(a6)_$(a7)
    );

    template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
    $class_for_func(
        $_(T1)$_(T2)$_(T3)$_(T4)$_(T5)$_(T6)$_(T7)_$(T8),
        $_(T1 a1)$_(T2 a2)$_(T3 a3)$_(T4 a4)$_(T5 a5)$_(T6 a6)$_(T7 a7)_$(T8 a8),
        $_(a1)$_(a2)$_(a3)$_(a4)$_(a5)$_(a6)$_(a7)_$(a8)
    );

    template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
    $class_for_func(
        $_(T1)$_(T2)$_(T3)$_(T4)$_(T5)$_(T6)$_(T7)$_(T8)_$(T9),
        $_(T1 a1)$_(T2 a2)$_(T3 a3)$_(T4 a4)$_(T5 a5)$_(T6 a6)$_(T7 a7)$_(T8 a8)_$(T9 a9),
        $_(a1)$_(a2)$_(a3)$_(a4)$_(a5)$_(a6)$_(a7)$_(a8)_$(a9)
    );

    template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
    $class_for_func(
        $_(T1)$_(T2)$_(T3)$_(T4)$_(T5)$_(T6)$_(T7)$_(T8)$_(T9)_$(T10),
        $_(T1 a1)$_(T2 a2)$_(T3 a3)$_(T4 a4)$_(T5 a5)$_(T6 a6)$_(T7 a7)$_(T8 a8)$_(T9 a9)_$(T10 a10),
        $_(a1)$_(a2)$_(a3)$_(a4)$_(a5)$_(a6)$_(a7)$_(a8)$_(a9)_$(a10)
    );

    template<typename R>
    class Action : public Func<R(*)()> {};
}

#undef $class_for_func
#undef $_
#undef _$