#pragma once

#include <stdarg.h>
#include <vector>

namespace System
{
    class Error
    {
    public:
        typedef std::vector<const char*> MessageVectorType;

        Error()
        : m_nCode(0)
        , m_vecMessages(NULL)
        { }

        Error(const Error& rhs)
        : m_nCode(rhs.m_nCode)
        , m_vecMessages(rhs.m_vecMessages)
        { }

        Error(const char* szMessage)
        : m_nCode(0)
        , m_vecMessages(1, szMessage)
        { }

        Error(int nCode, const char* szMessage)
        : m_nCode(nCode)
        , m_vecMessages(1, szMessage)
        { }

        Error(int nCode, const char* szMessage, const char* szDetails, ...)
        : m_nCode(0)
        , m_vecMessages()
        {
            m_vecMessages.push_back(szMessage);
            m_vecMessages.push_back(szDetails);

            va_list args;
            va_start(args, szDetails);

            for (const char* detail; (detail = va_arg(args, const char*)) && detail[0];) {
                m_vecMessages.push_back(detail);
            }

            va_end(args);

            if (nCode)
                m_nCode = nCode;
        }

        inline operator bool() const {
            return m_nCode && m_vecMessages.size() && m_vecMessages[0];
        }

        inline int GetCode() const {
            return m_nCode;
        }

        inline const MessageVectorType& GetMessages() const {
            return m_vecMessages;
        }

        inline void AddMessage(const char* szMessage) {
            m_vecMessages.push_back(szMessage);
        }
    private:
        int m_nCode;
        MessageVectorType m_vecMessages;
    };
}

#define $return_on_error(expr) { System::Error __err = expr; if (__err) return __err; }