#pragma once

#include <list>
#include <istream>
#include <ostream>

namespace System
{
    namespace Visitor
    {
        struct Slice
        {
            typedef std::list<Slice> ListType;

            Slice()
            : m_nOffset(~0)
            , m_nLength(~0)
            , m_nType(~0)
            , m_nWordId(~0)
            , m_nTokenId(~0)
            { }

            Slice(const Slice& rhs)
            : m_nOffset(rhs.m_nOffset)
            , m_nLength(rhs.m_nLength)
            , m_nType(rhs.m_nType)
            , m_nWordId(rhs.m_nWordId)
            , m_nTokenId(rhs.m_nTokenId)
            { }

            Slice(
                const size_t knOffset, 
                const size_t knLenght, 
                const size_t knType, 
                const size_t knWordId, 
                const size_t knTokenId = ~0)
            : m_nOffset(knOffset)
            , m_nLength(knLenght)
            , m_nType(knType)
            , m_nWordId(knWordId)
            , m_nTokenId(knTokenId)
            { }

            inline operator bool() const {
                return m_nOffset < ~0 && m_nLength < ~0;
            }

            size_t m_nOffset;
            size_t m_nLength;
            size_t m_nType;
            size_t m_nWordId;
            size_t m_nTokenId;
        };
        
        struct ZeroSetter
        {
            template<typename T>
            inline void operator() (T& value) {
                memset(&value, 0, sizeof(T));
            }
        };


        
        struct Reader
        {	
            Reader(std::ostream& rStream)
            : m_rStream(rStream)
            { }

            template<typename T>
            inline void operator() (const T& value) {
                m_rStream << value;
            }

            std::ostream& m_rStream;
        };

        struct Writer
        {	
            Writer(std::istream& rStream)
            : m_rStream(rStream)
            { }

            template<typename T>
            inline void operator() (T& value) {
                m_rStream >> value;
            }

            std::istream& m_rStream;
        };

        
        class Slices
        {
        public:
            Slices()
            : m_lstSlices()
            { }

            Slices(const Slices& rhs)
            : m_lstSlices(rhs.m_lstSlices)
            { }

            inline operator bool() const {
                return m_lstSlices.size() > 0;
            }

            inline bool TryPushSlice(const Slice& krsSlice) {
                if (!krsSlice)
                    return false;
                
                m_lstSlices.push_back(krsSlice);
                    
                return true;
            }

            inline bool TryPopSlice(Slice& rsSlice) {
                if (m_lstSlices.size() < 1)
                    return false;
                
                rsSlice = m_lstSlices.front();

                m_lstSlices.pop_front();

                return true;
            }
        private:
            Slice::ListType m_lstSlices;
        };
        
        class Getters : public Slices
        {	
        public:
            Getters(std::ostream& rStream)
            : Slices()
            , m_rStream(rStream)
            { }

            template<typename T>
            inline void operator() (const T& value) {
                Slice sSlice;

                if (TryPopSlice(sSlice)) {
                    m_rStream.seekp(sSlice.m_nOffset);
                    m_rStream << value;
                }
            }
        private:
            std::ostream& m_rStream;
        };

        class Setters : public Slice
        {	
        public:
            Setters(std::istream& rStream)
            : Slice()
            , m_rStream(rStream)
            { }

            template<typename T>
            inline void operator() (T& value) {
                Slice sSlice;

                if (TryPopSlice(sSlice)) {
                    m_rStream.seekg(sSlice.m_nOffset);
                    m_rStream >> value;
                }
            }
        private:
            std::istream& m_rStream;
        };
    }
}