#pragma once

#include "Error.h"
#include "Data.h"
#include "Visitor.h"

namespace System
{
    namespace Serialization
    {
        class Keyword
        {
        public:
            enum EFlags
            {
                None = 0,
                AllowWordAsParameter = 1,
                AllowProxy = 2,
                AllowSelfAsKey = 4,
                IsolatedRootKey = 8
            };

            enum EType
            {
                Unknown,
                Structural,
                Root
            };

            Keyword();
            Keyword(const Keyword& rhs);

            Keyword& Key(const size_t knParamIndex);
            Keyword& Type(const EType keValue);
            Keyword& Flag(const EFlags keValue);
            Keyword& Params(const size_t knValue);
            Keyword& Childs(const size_t knValue);
            Keyword& MinParams(const size_t knValue);
            Keyword& MaxParams(const size_t knValue);
            Keyword& MinChilds(const size_t knValue);
            Keyword& MaxChilds(const size_t knValue);

            bool TryGetMinParams(size_t& reValue) const;
            bool TryGetMaxParams(size_t& reValue) const;
            bool TryGetMinChilds(size_t& reValue) const;
            bool TryGetMaxChilds(size_t& reValue) const;
            bool TryGetKeyParam(size_t& rnValue) const;
            bool HasFlag(const EFlags keFlag) const;
            const EType GetType() const;

            static const Keyword* Get(const Word::IdType knWordId);
        private:
            EType m_eType;

            size_t m_nFlags;

            size_t m_nMinParams;
            size_t m_nMaxParams;

            size_t m_nMinChilds;
            size_t m_nMaxChilds;

            size_t m_nKeyParam;
        };

        class Keywords
        {
            typedef std::map<Word::IdType, Reflection::Attribute::IdType> MapType;
        public:
            static Keywords& GetInstance();
            const Keyword* GetByWordId(const Word::IdType knWordId);
        private:
            Error BindClasses();
            Error BindKeywords(const Reflection::Class& krcClass);
        private:
            MapType m_mapKeywords;
        };

        template<typename StreamType>
        static inline size_t SafePos(StreamType& rStream) {
            size_t pos;

            if (rStream.eof())
            {
                rStream.clear();
                rStream.seekg(0, rStream.end);
                pos = size_t(rStream.tellg());
                rStream.setstate(rStream.eofbit);
            }
            else
                pos = (size_t(rStream.tellg()) - 1);

            return pos;
        }

        template<typename StreamType, typename ValueType>
        static inline bool TryParse(StreamType& rStream, ValueType& rValue, bool* eof = NULL) {
            
            size_t i = rStream.tellg();

            rStream >> rValue;

            if (eof && (*eof = rStream.eof()))
                return true;

            if (rStream.good())
                return true;

            rStream.clear();
            rStream.seekg(i);

            return false;
        }
        
        template<typename StreamType>
        static inline bool TryRead(StreamType& rStream, const size_t knOffset, const size_t knLength, char* ptrOutputBuffer) {
            size_t i = rStream.tellg();
            bool bOk = false;
            bool eof = rStream.eof();

            if (eof)
                rStream.clear();

            rStream.seekg(knOffset);
            rStream.read(ptrOutputBuffer, knLength);
            
            if (eof)
            {
                bOk = true;

                rStream.setstate(rStream.eofbit);
            }
            else
            {
                bOk = rStream.good();

                if (!bOk)
                    rStream.clear();

                rStream.seekg(i);
            }

            
            return bOk;
        }
        
        template<typename StreamType, size_t N>
        static inline bool TryConsume(StreamType& rStream, const char (&kracInput)[N]) {
            size_t i = rStream.tellg();

            char acOutput[N] = {0};

            rStream.read(acOutput, N-1);

            bool bOk = rStream.good();

            if (bOk) 
                bOk = !strncmp(kracInput, acOutput, N-1);

            if (!bOk) {
                rStream.clear();
                rStream.seekg(i);
            }

            return bOk;
        }


        class Token
        {
        public:
            typedef size_t IdType;
            typedef std::vector<Token> VectorType;
            typedef std::vector<IdType> IdVectorType;

            enum EType
            {
                Namespace,
                Word,
                Coma,
                Comment,
                EOL,
                Indent,
                Parenthesis_Open,
                Parenthesis_Closed,
                Value,
                Root,
                RootProxy,
                Structural,
                Invalid,
            };

            enum EValueType
            {
                None,
                Boolean,
                Integer,
                Float,
                String,
                Enum,
                Variable,
                Property
            };

            Token()
            : m_eType(Invalid)
            , m_nOffset(~0)
            , m_nLenght(~0)
            , m_nLine(0)
            , m_eValueType(None)
            , m_nWordId(~0)
            { }

            Token(const Token& rhs)
            : m_eType(rhs.m_eType)
            , m_nOffset(rhs.m_nOffset)
            , m_nLenght(rhs.m_nLenght)
            , m_nLine(rhs.m_nLine)
            , m_eValueType(rhs.m_eValueType)
            , m_nWordId(rhs.m_nWordId)
            { }
            
            
            inline operator bool () const {
                return m_eType != Token::Invalid;
            }

            template<typename StreamType>
            friend inline StreamType& operator >> (StreamType& stream, Token& rhs) { 
                if (stream.eof())
                    stream.setstate(stream.failbit);

                char c = 0;
                bool back = false;

                rhs.m_nOffset = stream.tellg();
                rhs.m_nLenght = 1;
                rhs.m_eType = Token::Invalid;
                rhs.m_eValueType = Token::None;
                rhs.m_nWordId = ~0;


                stream >> c;

                if (c == ' ') {
                    stream >> c;

                    if (c == ' ') {
                        rhs.m_eType = Token::Indent;
                        rhs.m_nLenght = size_t(stream.tellg()) - rhs.m_nOffset;
                    } else {
                        ++rhs.m_nOffset;
                    }
                }

                switch (c)
                {
                    case '\"': {
                        
                        do {
                            stream >> c;
                        } while (!stream.eof() && c != '\"' && c != '\n');

                        rhs.m_eType = Token::Value;
                        rhs.m_eValueType = Token::String;
                        rhs.m_nOffset += 1;
                        rhs.m_nLenght = (size_t(stream.tellg()) - 1) - rhs.m_nOffset;

                    } break;

                    case '/': {
                        
                        stream >> c;
                        
                        switch(c) {
                            case '/': {

                                do  {
                                    stream >> c;
                                } while(!stream.eof() && c != '\n');

                                rhs.m_eType = Token::Comment;
                                rhs.m_nOffset += 2;
                                rhs.m_nLenght = (size_t(stream.tellg()) - 1) - rhs.m_nOffset;

                                back = true;
                            } break;

                            case '*' : {
                                
                                do  {
                                    stream >> c;

                                    if (c == '*') {
                                        stream >> c;

                                        if (c == '/')
                                            break;
                                    } else if (c == '\n') 
                                        ++rhs.m_nLine;

                                } while(!stream.eof());

                                rhs.m_eType = Token::Comment;
                                rhs.m_nOffset += 2;
                                rhs.m_nLenght = (size_t(stream.tellg()) - 2) - rhs.m_nOffset;

                            } break;
                        }
                    } break;

                    case '\t': {
                        rhs.m_eType = Token::Indent;
                    } break;

                    case '\r': 
                    case '\n': {
                        rhs.m_eType = Token::EOL;
                        ++rhs.m_nLine;
                    } break;

                    case ',': {
                        rhs.m_eType = Token::Coma;
                    } break;

                    case '(': {
                        rhs.m_eType = Token::Parenthesis_Open;
                    } break;

                    case ')': {
                        rhs.m_eType = Token::Parenthesis_Closed;
                    } break;

                    default: {
                        if (c >= '0' && c <= '9') {
                            rhs.m_eType = Token::Value;
                            rhs.m_eValueType = Token::Integer;

                            do  {
                                stream >> c;

                                if (c == '.') {
                                    rhs.m_eValueType = Token::Float;
                                } else if (c >= '0' && c <= '9') {
                                    continue;
                                } else {
                                    back = true;
                                    break;
                                }
                                
                            } while(!stream.eof());

                            rhs.m_nLenght = (size_t(stream.tellg()) - 1) - rhs.m_nOffset;
                        } else if ((c == 't' && TryConsume(stream, "rue")) || (c == 'f' && TryConsume(stream, "alse"))) {
                            rhs.m_eType = Token::Value;
                            rhs.m_eValueType = Token::Boolean;
                            
                            rhs.m_nLenght = size_t(stream.tellg()) - rhs.m_nOffset;
                        } else if ((c == ':') || (c == '_') || (c == '@') || (c == '$') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                            
                            if (c == ':') {
                                ++rhs.m_nOffset;
                                rhs.m_eType = Token::Namespace;
                            }
                            else if (c == '$') {
                                ++rhs.m_nOffset;
                                rhs.m_eType = Token::Value;
                                rhs.m_eValueType = Token::Variable;
                            }
                            else if (c == '@') {
                                ++rhs.m_nOffset;
                                rhs.m_eType = Token::Value;
                                rhs.m_eValueType = Token::Property;
                            }
                            else
                                rhs.m_eType = Token::Word;

                            do  {
                                stream >> c;

                                if (c == ':') {
                                    rhs.m_eType = Token::Namespace;
                                    break;
                                } else if (c == '.') {
                                    rhs.m_eType = Token::Value;
                                    rhs.m_eValueType = Token::Enum;
                                    continue;
                                } else if (c >= '0' && c <= '9') {
                                    continue;
                                } else if (c >= 'a' && c <= 'z') {
                                    continue;
                                } else if (c >= 'A' && c <= 'Z') {
                                    continue;
                                } else if (c == '_') {
                                    continue;
                                } else {
                                    back = true;
                                    break;
                                }
                                
                            } while(!stream.eof());
                            
                            rhs.m_nLenght = SafePos(stream) - rhs.m_nOffset;
                        } 
                    } break;
                }

                if (rhs.m_eType == Token::Invalid) {
                    //char invalid[1024] = { 0 };

                    //TryRead(stream, 0, rhs.m_nOffset, invalid);

                    stream.setstate(stream.failbit);
                } else {
                    rhs.TryGetOrCreateWord(stream);

                    if (back)
                        stream.seekg(-1, stream.cur);
                }
                
                return stream;
            }

            inline bool TrySetType(const Keyword* kpcKeyword) {
                if (kpcKeyword == NULL)
                    return false;

                switch (m_eType)
                {
                    case Token::Word:
                        switch (kpcKeyword->GetType())
                        {
                        case Keyword::Root:
                            m_eType = Token::Root;
                            break;
                        case Keyword::Structural:
                            m_eType = Token::Structural;
                            break;
                        default:
                            return false;
                        } break;

                    case Token::Root:
                        switch (kpcKeyword->GetType())
                        {
                        case Keyword::Root:
                            if (kpcKeyword->HasFlag(Keyword::AllowProxy))
                                m_eType = Token::RootProxy;
                            else
                                return false;

                            break;
                        default:
                            return false;
                        } break;
                    
                    default:
                        return false;
                }

                return true;
            }

            template <typename StreamType, size_t N>
            inline bool TryGetString(StreamType& stream, char(&buffer)[N]) const {
                if (m_eType == Token::Invalid)
                    return false;
                
                if (N < m_nLenght)
                    return false;
                
                return TryRead(stream, m_nOffset, m_nLenght, buffer);
            }

            template <typename StreamType>
            inline bool TryGetString(StreamType& stream, std::string& str) const {
                if (m_eType == Token::Invalid)
                    return false;

                str.resize(m_nLenght);
                
                return TryRead(stream, m_nOffset, m_nLenght, &str[0]);
            }

            template <typename StreamType>
            inline bool TryGetOrCreateWord(StreamType& stream) {
                switch (m_eType) {
                    case Token::Value:
                        if (HasWordInValueType(m_eValueType))
                            break;

                        return false;

                    default:
                        if (HasWordInTokenType(m_eType))
                            break;

                        return false;
                }
                
                std::string strWord;
                
                if (!TryGetString(stream, strWord))
                    return false;

                m_nWordId = Word::Id(strWord);

                return true;
            }

            inline bool TryPushSlice(Visitor::Slices& rcSlices, const size_t knTokenId) const {
                if (m_eType == Token::Invalid)
                    return false;
                
                return rcSlices.TryPushSlice(Visitor::Slice(
                    m_nOffset, 
                    m_nLenght, 
                    m_eValueType, 
                    m_nWordId, 
                    knTokenId
                ));
            }

            inline EType GetType() const {
                return m_eType;
            }

            inline EValueType GetValueType() const {
                return m_eType == Token::Value ? m_eValueType : Token::None;
            }

            inline size_t GetLine() const {
                return m_nLine;
            }

            inline Word::IdType GetWordId() const {
                return m_nWordId;
            }

            inline Word::PointerType GetWordPointer() const {
                return Word::String(m_nWordId).c_str();
            }

            inline bool HasWord() const {
                return Word::Has(m_nWordId);
            }

            static inline bool HasWordInTokenType(const EType knTokenType) {
                switch (knTokenType)
                {
                    case Token::Word:
                    case Token::Root:
                    case Token::RootProxy:
                    case Token::Structural:
                    case Token::Namespace:
                        return true;
                    default:
                        return false;
                }
            }

            static inline bool HasWordInValueType(const EValueType knValueType) {
                switch (knValueType)
                {
                    case Token::String:
                    case Token::Enum:
                    case Token::Variable:
                    case Token::Property:
                        return true;
                    default:
                        return false;
                }
            }
        private:
            EType m_eType;
            EValueType m_eValueType;
            size_t m_nOffset;
            size_t m_nLenght;
            size_t m_nLine;
            Word::IdType m_nWordId;
        };

        class Tokenizer
        {
        public:
            Tokenizer()
            : m_vecTokens()
            { }

            Tokenizer(const Tokenizer& rhs)
            : m_vecTokens(rhs.m_vecTokens)
            { }
            
            inline void Clear() {
                m_vecTokens.clear();
            }


            template<typename StreamType>
            inline StreamType& operator << (StreamType& stream) const {
                
                return stream;
            }

            template<typename StreamType>
            friend inline StreamType& operator >> (StreamType& stream, Tokenizer& rhs) {
                
                stream >> std::noskipws;

                bool eof;
                Token cToken;

                while(TryParse(stream, cToken, &eof) && cToken.GetType() != Token::Invalid) {
                    rhs.m_vecTokens.push_back(cToken);

                    if (eof)
                        return stream;
                }

                return stream;
            }

            inline Token& GetToken(const Token::IdType knTokenId) {
                return m_vecTokens[knTokenId];
            }

            inline const Token::VectorType& GetTokens() const {
                return m_vecTokens;
            }
        private:
            Token::VectorType m_vecTokens;
        };


        class Descriptor
        {
        public:
            typedef size_t IdType;
            typedef std::vector<Descriptor> VectorType;

            Descriptor();
            Descriptor(const Descriptor& rhs);
            Descriptor(const Token::IdType knTokenId);

            operator bool() const;
            bool operator == (const Descriptor& rhs) const;
            bool operator != (const Descriptor& rhs) const;
            
            void SetToken(const Token::IdType knTokenId);
            Token::IdType GetTokenId() const;
            const Token& GetToken(const Token::VectorType& krvecTokens) const;

            bool HasParameter(const Token::IdType knTokenId) const;
            void AddParameter(const Token::IdType knTokenId);
            bool TryAddParameter(const Token::IdType knTokenId);
            const Token& GetParameter(const Token::VectorType& krvecTokens, const size_t knIndex) const;
            const Token::IdVectorType& GetParameters() const;
            size_t GetParametersCount() const;
            
            bool TryPushSlices(Visitor::Slices& rcSlices, const Serialization::Token::VectorType& krvecTokens) const;
            
            bool TryGetKeyWordId(Word::IdType& rnKeyWordId, const Token::VectorType& krvecTokens, const Keyword* kpcKeyword, const Keyword::EType keAllowedType) const;
        private:
            Token::IdType m_nTokenId;
            Token::IdVectorType m_vecParameters;
        };

        class Descriptors
        {
        public:
            Descriptor::IdType AddDescriptor(const Token::IdType knTokenId);
            
            Descriptor& GetDescriptor(const Descriptor::IdType knId);
            const Descriptor& GetDescriptor(const Descriptor::IdType knId) const;
            
            size_t GetDescriptorCount() const;

            void Clear();
        private:
            Descriptor::VectorType m_vecDescriptors;
        };

        class Parser
        {
        public:
            inline void Reset() {
                m_lstIndentParents.clear();
                m_lstLineParents.clear();
                m_lstIndents.clear();
                m_nIndent = 0;
            }

            template<typename StreamType>
            inline Error TryParse(Tokenizer& rcTokenizer, Data::Tree& rcTree, Descriptors& rcDescriptors, StreamType& rcStream) {
                Error cError;
                Reset();

                const Token::VectorType& krcTokens = rcTokenizer.GetTokens();
                Data::Node::IdType nLastNodeId = ~0;
                const Keyword* kpcLastKeyword = NULL;
                bool bParenthesisOpened = false;

                for (Token::IdType nTokenId = 0, nTokens = krcTokens.size(); nTokenId < nTokens; ++nTokenId) {
                    Token& rcToken = rcTokenizer.GetToken(nTokenId);
                    const Keyword* kpcKeyword = Keyword::Get(rcToken.GetWordId());

                    rcToken.TrySetType(kpcKeyword);

                    const Token::EType keTokenType = rcToken.GetType();
                    const size_t knTokenLine = rcToken.GetLine();

                    Data::Node::IdType nNodeId(~0);
                    bool bIsParameter = false;

                    switch (keTokenType)
                    {
                        case Token::Indent: {
                            ++m_nIndent;  
                        } break;

                        case Token::EOL: {
                            m_nIndent = 0;
                            m_lstLineParents.clear();
                            
                            nLastNodeId = ~0;
                            kpcLastKeyword = NULL;
                        } break;

                        case Token::Parenthesis_Open: {
                            if (!bParenthesisOpened)
                                bParenthesisOpened = true;
                            else
                                return Error(knTokenLine, "Unexpected open parenthesis.");
                        } break;

                        case Token::Parenthesis_Closed: {
                            if (bParenthesisOpened)
                                bParenthesisOpened = false;
                            else
                                return Error(knTokenLine, "Unexpected closed parenthesis.");
                        } break;

                        case Token::Namespace:
                        case Token::Value: {
                            bIsParameter = true;
                        } break;

                        case Token::Root: {
                            if (!m_nIndent && !m_lstLineParents.size()) {
                                m_lstIndents.clear();
                                m_lstIndentParents.clear();
                            }
                            else if (!rcToken.TrySetType(kpcKeyword)) {
                                return Error(knTokenLine, "proxy not allowed");
                            }
                        }

                        case Token::Structural: {
                            nNodeId = PushNode(rcDescriptors, rcTree, nTokenId);
                        } break;

                        case Token::Word: {
                            if (kpcLastKeyword && kpcLastKeyword->HasFlag(Keyword::AllowWordAsParameter))
                                bIsParameter = true;
                            else
                                nNodeId = PushNode(rcDescriptors, rcTree, nTokenId);
                        } break;
                        
                        default:
                            break;
                    }

                    if (nNodeId < ~0)
                        PushParent(nNodeId, keTokenType == Token::Structural);


                    if (keTokenType == Token::EOL || keTokenType == Token::Indent)
                        continue;

                    if (keTokenType == Token::Comment)
                        continue;

                    if (keTokenType == Token::Coma || keTokenType == Token::Parenthesis_Open || keTokenType == Token::Parenthesis_Closed)
                        continue;


                    char cTokenString[512] = {0};

                    if (!rcToken.TryGetString(rcStream, cTokenString))
                        cTokenString[0] = 0;

                    if (bIsParameter || bParenthesisOpened) {
                        if (nLastNodeId < ~0)
                            rcDescriptors.GetDescriptor(nLastNodeId).AddParameter(nTokenId);
                        else
                            return Error(knTokenLine, "Unexpected parameter value.", cTokenString, NULL);
                    } else {
                        if (nNodeId == ~0)
                            return Error(knTokenLine, "Absent node", cTokenString, NULL);

                        Data::Node::IdType nParentId = FindLastParent(nNodeId, m_lstLineParents);

                        if (!m_lstLineParents.size() && (m_lstIndentParents.size() > 0 && m_lstIndentParents.back() != nNodeId))
                            PopParentToIndent(m_nIndent);

                        if (nParentId == ~0) 
                            nParentId = FindLastParent(nNodeId, m_lstIndentParents);

                        if (nParentId < ~0)
                            rcTree.GetNode(nParentId).TryAddChild(nNodeId);
                        else if (rcToken.GetType() != Token::Root) 
                            return Error(knTokenLine, "Unexpected node type. Only tree nodes are expected on top of the hierarchy. (Indentation typo?)", cTokenString, NULL);

                        nLastNodeId = nNodeId;
                        kpcLastKeyword = kpcKeyword;
                    }

                }
                
                for (Descriptor::IdType id = 0; id < rcDescriptors.GetDescriptorCount(); ++id) {
                    const Descriptor& krcDescriptor = rcDescriptors.GetDescriptor(id);
                    const Token& krcToken = krcDescriptor.GetToken(krcTokens);
                    const Token::EType keTokenType = krcToken.GetType();

                    switch (keTokenType) {
                        case Token::Root:
                        case Token::RootProxy: {
                            const Word::IdType knSymbolWordId = krcToken.GetWordId();
                            const Keyword* kpcKeyword = Keyword::Get(knSymbolWordId);
                            
                            Word::IdType nKeyWordId;

                            if (krcDescriptor.TryGetKeyWordId(nKeyWordId, krcTokens, kpcKeyword, Keyword::Root)) {
                                Data::Symbol& rcSymbol = rcTree.GetSymbol(knSymbolWordId);

                                rcSymbol.SetWordId(knSymbolWordId);
                                rcSymbol.SetShared(!kpcKeyword->HasFlag(Keyword::IsolatedRootKey));

                                if (keTokenType == Token::Root)
                                    rcSymbol.SetDefinition(id, nKeyWordId);
                                else
                                    rcSymbol.SetDeclaration(id, nKeyWordId);
                            }
                        } break;

                        default:
                            break;
                    }
                }

                rcTree.ResolveInternalDeclarations();

                return cError;
            }

        private:


            inline Data::Node::IdType FindFirstParent(const Data::Node::IdType knNodeId, const Data::Node::IdListType& krlstParents) const {
                for (Data::Node::IdListType::const_iterator it = krlstParents.begin(); it != krlstParents.end(); it++) {
                    const Data::Node::IdType& krnParentId = *it;

                    if (krnParentId != knNodeId) {
                        return krnParentId;
                    }
                }

                return ~0;
            }

            inline Data::Node::IdType FindLastParent(const Data::Node::IdType knNodeId, const Data::Node::IdListType& krlstParents) const {
                for (Data::Node::IdListType::const_reverse_iterator it = krlstParents.rbegin(); it != krlstParents.rend(); it++) {
                    const Data::Node::IdType& krnParentId = *it;

                    if (krnParentId != knNodeId) {
                        return krnParentId;
                    }
                }

                return ~0;
            }

            inline void PopParentToIndent(const size_t knIndent) {
                while( m_lstIndents.size() > 0 && m_lstIndents.back() >= knIndent )
                {
                    m_lstIndents.pop_back();
                    m_lstIndentParents.pop_back();
                }
            }
            
            inline void PushParent(const Data::Node::IdType knParentId, const bool kbIsStructural) {
                if (!m_lstLineParents.size()) {
                    PopParentToIndent(m_nIndent);

                    m_lstIndents.push_back(m_nIndent);
                    m_lstIndentParents.push_back(knParentId);
                }

                if (kbIsStructural) {
                    m_lstLineParents.push_back(knParentId);
                }
            }

            inline Data::Node::IdType PushNode(Descriptors& rcDescriptors, Data::Tree& rcTree, const Token::IdType knTokenId) {
                rcDescriptors.AddDescriptor(knTokenId);
                return rcTree.AddNode();
            }

        private:
            Data::Node::IdListType m_lstIndentParents;
            Data::Node::IdListType m_lstLineParents;
            std::list<int> m_lstIndents;
            size_t m_nIndent;
        };

    }
}
