#pragma once

#include <map>
#include <set>
#include <stack>
#include <deque>
#include <vector>

#include "Reflection.h"
#include "Error.h"
#include "Visitor.h"

namespace System
{
    namespace Serialization
    {
        class Descriptor;
    }

    namespace Runner
    {
        class Processor;
    }

    namespace Binding
    {
        struct Setter;
    }

    namespace Data
    {
        class Node;
        class Tree;
        class Link;
        class Linker;

        typedef size_t IndexType;
        typedef std::set<IndexType> IndexSetType;
        typedef std::vector<IndexType> IndexVectorType;

        template<typename ObjectType>
        inline ObjectType* GetPointerFor(std::vector<ObjectType>& rvecContainer, IndexType nIndex, bool bSafe) { 
            if (bSafe)
                if (nIndex >= rvecContainer.size())
                    return NULL;
            else if (nIndex > rvecContainer.size())
                return NULL;
            else if (rvecContainer.size() == 0)
                return NULL;

            return &rvecContainer[nIndex];
        }

        template<typename T>
        class Variable
        {
        public:
            typedef T Type;

            Variable()
            : m_Value(Type())
            { }

            Variable(const Variable& rhs)
            : m_Value(rhs.m_Value)
            { }

            Variable(const Type& krValue)
            : m_Value(krValue)
            { }

            inline Variable& operator = (const Type& krValue) {
                m_Value = krValue;

                return *this;
            }

            inline operator const Type&() const {
                return m_Value;
            }

            inline void Set(const Type& krValue) {
                m_Value = krValue;
            }

            inline const Type& Get() const {
                return m_Value;
            }

            inline Type& GetReference() {
                return m_Value;
            }

            inline Type* GetPointer() {
                return &m_Value;
            }
        private:
            Type m_Value;
        };

        class Reference
        {
        public:
            typedef std::vector<Reference> VectorType;
            typedef std::map<Word::IdType, Reference> MapType;

            Reference()
            : m_nTypeId(~0)
            , m_nObjectIndex(~0)
            { }
            Reference(const Reference& rhs)
            : m_nTypeId(rhs.m_nTypeId)
            , m_nObjectIndex(rhs.m_nObjectIndex)
            { }

            Reference(const Reflection::Type::IdType knTypeId, IndexType knIndex)
            : m_nTypeId(knTypeId)
            , m_nObjectIndex(knIndex)
            { }
            Reference(const Reflection::Type& krcType, IndexType knIndex)
            : m_nTypeId(krcType.GetId())
            , m_nObjectIndex(knIndex)
            { }

            inline operator bool() const {
                return m_nTypeId < ~0 && m_nObjectIndex < ~0;
            }

            inline bool HasType() const {
                return m_nTypeId < ~0;
            }

            inline bool MatchType(const Reflection::Type::IdType knTypeId) const {
                return m_nTypeId == knTypeId;
            }

            template<typename ObjectType>
            inline bool MatchType() const {
                return m_nTypeId == Reflection::Type::GetId<ObjectType>();
            }

            inline const Reflection::Type& GetType() const {
                return Reflection::Type::Get(m_nTypeId);
            }

            inline IndexType GetIndex() const {
                return m_nObjectIndex;
            }

            template<typename ObjectType>
            inline ObjectType* GetPointer(std::vector<ObjectType>* ptrvecContainer) const {
                if (MatchType<ObjectType>())
                    return GetPointerFor(*ptrvecContainer, m_nObjectIndex, true);
                return NULL;
            }
        private:
            Reflection::Type::IdType m_nTypeId;
            IndexType m_nObjectIndex;
        };

        class Span
        {
        public:
            Span()
            : m_nTypeId(~0)
            , m_nFromIndex(~0)
            , m_nToIndex(~0)
            { }
            Span(const Span& rhs)
            : m_nTypeId(rhs.m_nTypeId)
            , m_nFromIndex(rhs.m_nFromIndex)
            , m_nToIndex(rhs.m_nToIndex)
            { }

            Span(const Reflection::Type& krcType, IndexType knFromIndex, IndexType knToIndex)
            : m_nTypeId(krcType.GetId())
            , m_nFromIndex(knFromIndex)
            , m_nToIndex(knToIndex)
            { }

            template<typename ObjectType>
            Span(std::vector<ObjectType>* ptrvecContainer)
            : m_nTypeId(Reflection::Type::GetOrCreateType<ObjectType>().GetId())
            , m_nFromIndex(0)
            , m_nToIndex(ptrvecContainer->size())
            { }

            inline operator bool() const {
                return m_nTypeId < ~0 && m_nFromIndex < ~0 && m_nToIndex < ~0;
            }

            template<typename ObjectType>
            inline bool MatchType() const {
                return m_nTypeId == Reflection::Type::GetId<ObjectType>();
            }

            inline const Reflection::Type& GetType() const {
                return Reflection::Type::Get(m_nTypeId);
            }

            inline IndexType GetFromIndex() const {
                return m_nFromIndex;
            }

            inline IndexType GetToIndex() const {
                return m_nToIndex;
            }

            template<typename ObjectType>
            inline ObjectType* GetFromPointer(std::vector<ObjectType>* ptrvecContainer) const {
                if (MatchType<ObjectType>())
                    return GetPointerFor(*ptrvecContainer, m_nFromIndex, true);
                return NULL;
            }
            
            template<typename ObjectType>
            inline ObjectType* GetToPointer(std::vector<ObjectType>* ptrvecContainer) const {
                if (MatchType<ObjectType>())
                    return GetPointerFor(*ptrvecContainer, m_nToIndex, false);
                return NULL;
            }
        private:
            Reflection::Type::IdType m_nTypeId;
            IndexType m_nFromIndex;
            IndexType m_nToIndex;
        };

        template<typename ObjectType>
        class Iterator
        {
        public:
            Iterator()
            : m_it(NULL)
            , m_end(NULL)
            { }
            Iterator(const Iterator& rhs)
            : m_it(rhs.m_it)
            , m_end(rhs.m_end)
            { }

            Iterator(std::vector<ObjectType>* ptrvecContainer, const Span& krcSpan)
            : m_it(krcSpan.GetFromPointer<ObjectType>(ptrvecContainer))
            , m_end(krcSpan.GetToPointer<ObjectType>(ptrvecContainer))
            { }

            inline operator bool () const {
                return m_it != m_end;
            }

            inline Iterator& operator ++ () {
                ++m_it;
                return *this;
            }

            inline ObjectType* operator -> () const {
                return m_it;
            }

            inline operator ObjectType* () const {
                return m_it;
            }

            inline ObjectType* GetPointer() const {
                return m_it;
            }

            inline ObjectType& GetReference() const {
                return *m_it;
            }

        private:
            ObjectType* m_it;
            ObjectType* m_end;
        };

        class Storage
        {
            template<typename T>
            void* GetPointerFor(const Reference& krcReference) const {
                std::vector<T>* pvecObjects = GetVector<T>();

                if (pvecObjects)
                    return krcReference.GetPointer(pvecObjects);

                return NULL;
            }
            template<typename T>
            size_t GetSizeFor() const {
                return GetVector<T>()->size();
            }
            template<typename T>
            void DtorFor() {
                delete GetVector<T>();
            }

            template<typename T>
            bool NewWith(IndexType& rnIndex, const T& krcObject) {
                std::vector<T>& rvecObjects = *GetVector<T>();

                if (rnIndex < ~0) {
                    rvecObjects[rnIndex] = krcObject;

                    IndexSetType::iterator it = m_setDeletedIndicies.find(rnIndex);

                    if (it != m_setDeletedIndicies.end())
                        m_setDeletedIndicies.erase(it);
                }
                else {
                    IndexSetType::iterator it = m_setDeletedIndicies.upper_bound(0);

                    if (it == m_setDeletedIndicies.end()) {
                        rnIndex = rvecObjects.size();

                        rvecObjects.push_back(krcObject);
                    }
                    else {
                        rnIndex = *it;

                        rvecObjects[rnIndex] = krcObject;

                        m_setDeletedIndicies.erase(it);
                    }
                }

                return true;
            }

            template<typename T>
            bool NewFor(IndexType& rnIndex) {
                return NewWith(rnIndex, T());
            }

            template<typename StreamType, typename T>
            bool ParseNewFor(StreamType& rStream, IndexType& rnIndex, const Visitor::Slice& krsSlice) {
                T value;

                Reflection::ParseValue(rStream, value, krsSlice);

                return NewWith(rnIndex, value);
            }

            template<typename StreamType, typename T>
            bool ParseOverrideFor(StreamType& rStream, const IndexType& krnIndex, const Visitor::Slice& krsSlice) {
                std::vector<T>& rvecObjects = *GetVector<T>();

                if (rvecObjects.size() <= krnIndex)
                    return false;

                Reflection::ParseValue(rStream, rvecObjects[krnIndex], krsSlice);

                return true;
            }

            template<typename T>
            bool DeleteFor(const IndexType knIndex) {
                GetVector<T>()->operator[](knIndex).~T();

                return true;
            }
        public:
            typedef std::map<Reflection::Type::IdType, Storage*> MapType;

            Storage()
                : m_nTypeId(~0)
                , m_setDeletedIndicies()
                , m_ptrVector(NULL)
                , m_mptrGetPointer(NULL)
                , m_mptrGetSize(NULL)
                , m_mptrDtor(NULL)
                , m_mptrNew(NULL)
                , m_mptrDelete(NULL)
                , m_mptrParseNew(NULL)
                , m_mptrParseOverride(NULL)
            { }
            ~Storage() {
                (this->*m_mptrDtor)();
            }

            template<typename T>
            static Storage* Create(const Reflection::Type& krcType) {
                Storage* self = new Storage();
                
                self->m_nTypeId = krcType.GetId();
                self->m_ptrVector = new std::vector<T>();
                self->m_mptrGetPointer = Storage::GetPointerFor<T>;
                self->m_mptrGetSize = Storage::GetSizeFor<T>;
                self->m_mptrDtor = Storage::DtorFor<T>;
                self->m_mptrNew = Storage::NewFor<T>;
                self->m_mptrDelete = Storage::DeleteFor<T>;
                self->m_mptrParseNew = Storage::ParseNewFor<std::istream, T>;
                self->m_mptrParseOverride = Storage::ParseOverrideFor<std::istream, T>;
                return self;
            }

            template<typename T>
            inline std::vector<T>* GetVector() const {
                typedef std::vector<T>* VectorType;

                return static_cast<VectorType>(m_ptrVector);
            }
            template<typename T>
            inline T* GetPointer(const Reference& krcReference) const {
                std::vector<T>* pvecObjects = GetVector<T>();

                if (pvecObjects)
                    return krcReference.GetPointer(pvecObjects);

                return NULL;
            }
            inline void* GetPointer(const Reference& krcReference) const {
                return (this->*m_mptrGetPointer)(krcReference);
            }
            inline size_t GetSize() const {
                return (this->*m_mptrGetSize)();
            }

            inline bool New(Reference& rcReference) {
                if (rcReference.HasType() && !rcReference.MatchType(m_nTypeId))
                    return false;

                IndexType nIndex = rcReference.GetIndex();

                if ((this->*m_mptrNew)(nIndex)) {
                    rcReference = Reference(m_nTypeId, nIndex);

                    return true;
                }

                return false;
            }

            template<typename T>
            inline bool New(Reference& rcReference, const T& krcObject = T()) {
                if (rcReference.HasType() && !rcReference.MatchType(m_nTypeId))
                    return false;

                IndexType nIndex = rcReference.GetIndex();

                if (NewWith(nIndex, krcObject)) {
                    rcReference = Reference(m_nTypeId, nIndex);

                    return true;
                }

                return false;
            }

            inline bool Delete(const Reference& krcReference) {
                if (krcReference && krcReference.MatchType(m_nTypeId)) {
                    const IndexType knIndex = krcReference.GetIndex();

                    if (m_setDeletedIndicies.insert(knIndex).second) {
                        return (this->*m_mptrDelete)(knIndex);
                    }
                }

                return false;
            }
            
            inline bool ParseNew(std::istream& rcStream, IndexType& rnIndex, const Visitor::Slice& krcSlice) {
                return (this->*m_mptrParseNew)(rcStream, rnIndex, krcSlice);
            }

            inline bool ParseOverride(std::istream& rcStream, const IndexType& krnIndex, const Visitor::Slice& krcSlice) {
                return (this->*m_mptrParseOverride)(rcStream, krnIndex, krcSlice);
            }

        private:
            Reflection::Type::IdType m_nTypeId;
            IndexSetType m_setDeletedIndicies;
            void* m_ptrVector;
            void* (Storage::* m_mptrGetPointer)(const Reference&) const;
            size_t(Storage::* m_mptrGetSize)() const;
            void(Storage::* m_mptrDtor)();
            bool(Storage::* m_mptrNew)(IndexType&);
            bool(Storage::* m_mptrDelete)(const IndexType);
            bool(Storage::* m_mptrParseNew)(std::istream&, IndexType&, const Visitor::Slice&);
            bool(Storage::* m_mptrParseOverride)(std::istream&, const IndexType&, const Visitor::Slice&);
        };

        template<typename T>
        class Parameter
        {
        public:
            Parameter()
                : m_kpcStorage(NULL)
                , m_nIndex(~0)
            { }
            Parameter(const Parameter& rhs)
                : m_kpcStorage(rhs.m_kpcStorage)
                , m_nIndex(rhs.m_nIndex)
            { }

            inline bool IsValid() const {
                return m_kpcStorage != NULL && m_nIndex < ~0 && m_nIndex < m_kpcStorage->GetSize();
            }

            inline void Set(const Storage* kpcStorage, const IndexType knIndex) {   
                m_kpcStorage = kpcStorage;
                m_nIndex = knIndex;
            }

            inline T& GetReference() const {
                return m_kpcStorage->GetVector<T>()->operator[](m_nIndex);
            }

            inline T* GetPointer() const {
                return &m_kpcStorage->GetVector<T>()->operator[](m_nIndex);
            }

            inline operator bool() const {
                return IsValid();
            }

            inline operator T& () const {
                return GetReference();
            }

            inline T& operator -> () const {
                return GetReference();
            }
        private:
            const Storage* m_kpcStorage;
            IndexType m_nIndex;
        };

        class Repository
        {
        public:
            Repository()
                : m_mapTypeStorages()
                , m_mapVariableReferences()
            { }
            Repository(const Repository& rhs)
                : m_mapTypeStorages(rhs.m_mapTypeStorages)
                , m_mapVariableReferences(rhs.m_mapVariableReferences)
            { }

            void ClearStorages() {
                for (Storage::MapType::iterator it = m_mapTypeStorages.begin(); it != m_mapTypeStorages.end(); ++it) {
                    delete it->second;
                }

                m_mapTypeStorages.clear();
                m_mapVariableReferences.clear();
            }

            template<typename T>
            Storage* GetOrCreateStorage() {
                const Reflection::Type& krcType = Reflection::Type::GetOrCreateType<T>();

                Storage::MapType::iterator it = m_mapTypeStorages.find(krcType.GetId());

                if (it == m_mapTypeStorages.end())
                    it = m_mapTypeStorages.insert(std::make_pair(krcType.GetId(), Storage::Create<T>(krcType))).first;

                return it->second;
            }

            template<typename T>
            Storage* GetStorage() const {
                const Reflection::Type& krcType = Reflection::Type::GetOrCreateType<T>();

                Storage::MapType::const_iterator it = m_mapTypeStorages.find(krcType.GetId());

                if (it == m_mapTypeStorages.end())
                    return NULL;

                return it->second;
            }


            inline Storage* FindStorage(const Reflection::Type::IdType knTypeId) {
                Storage::MapType::iterator it = m_mapTypeStorages.find(knTypeId);

                if (it == m_mapTypeStorages.end())
                    return NULL;

                return it->second;
            }

            inline const Storage* FindStorage(const Reflection::Type::IdType knTypeId) const {
                Storage::MapType::const_iterator it = m_mapTypeStorages.find(knTypeId);

                if (it == m_mapTypeStorages.end())
                    return NULL;

                return it->second;
            }


            template<typename T>
            std::vector<T>* GetOrCreateObjects() {
                return GetOrCreateStorage<T>()->GetVector<T>();
            }

            template<typename T>
            std::vector<T>* GetObjects() const {
                Storage* pStorage = GetStorage<T>();
                
                if (pStorage)
                    return pStorage->GetVector<T>();
              
                return NULL;
            }

            template<typename T>
            T* GetPointer(const IndexType knIndex = 0) const {
                std::vector<T>* pvecObjects = GetObjects<T>();

                if (pvecObjects == NULL)
                    return NULL;

                return GetPointerFor(*pvecObjects, knIndex, true);
            }

            template<typename T>
            T* GetInstance(const IndexType knIndex = 0) const {
                std::vector<T*>* pvecObjects = GetObjects<T*>();

                if (pvecObjects == NULL)
                    return NULL;

                T** ptrInstance = GetPointerFor(*pvecObjects, knIndex, true);

                if (ptrInstance == NULL)
                    return NULL;

                return *ptrInstance;
            }
            template<typename T>
            T* GetInstance(const Reference& krcReference) const {
                return krcReference.GetPointer(GetObjects<T*>());
            }

            template<typename T>
            T* GetObjectByReference(const Reference& krcReference) const {
                return krcReference.GetPointer(GetObjects<T>());
            }

            template<typename T>
            T* GetFirstObject() const {
                std::vector<T>* ptrObjects = GetObjects<T>();
                
                if (ptrObjects)
                    return &ptrObjects->front();
                
                return NULL;
            }

            template<typename T>
            T* GetLastObject() const {
                std::vector<T>* ptrObjects = GetObjects<T>();
                
                if (ptrObjects)
                    return &ptrObjects->back();
                
                return NULL;
            }

            template<typename T>
            Reference AddInstance(T* pInstance) {
                Reference cReference;

                GetOrCreateStorage<T*>()->New(cReference, krObject);

                return kcReference;
            }

            template<typename T>
            void SetInstance(T* pInstance, const IndexType knIndex = 0) {
                std::vector<T*>& rvecObjects = *GetOrCreateObjects<T*>();

                if (rvecObjects.size() <= knIndex)
                    rvecObjects.resize(knIndex + 1);

                rvecObjects[knIndex] = pInstance;
            }


            template<typename T>
            Reference CreateObject(const T& krObject = T()) {
                Reference cReference;
              
                GetOrCreateStorage<T>()->New(cReference, krObject);

                return cReference;
            }

            template<typename T>
            Reference CreateObject(const T* kpObject ) {
                return CreateObject(*kpObject);
            }

            template<typename T>
            Reference CloneObject(const Reference& krcReference) {
                return CreateObject<T>(GetObjectByReference<T>(krcReference));
            }

            template<typename FromType, typename ToType>
            Reference ConvertObject(const Reference& krcFrom) {
                return CreateObject<ToType>(GetObjectByReference<FromType>(krcFrom));
            }

            template<typename FromType, typename ToType>
            Span ConvertObjects(const Span& krcFromSpan) {
                
                std::vector<FromType>* ptrFromObjects = GetOrCreateObjects<FromType>();
                std::vector<ToType>* ptrToObjects = GetOrCreateObjects<ToType>();

                const Span kcToSpan(Reflection::Type::GetOrCreateType<ToType>(), ptrToObjects->size(), ptrToObjects->size() + ptrFromObjects->size());
                    
                for (Iterator<FromType> it(ptrFromObjects, krcFromSpan); it; ++it) {
                    ptrToObjects->push_back(ToType(it.GetPointer()));
                }

                return kcToSpan;
            }

            template<typename FromType, typename ToType>
            Span ConvertObjects() {
                return ConvertObjects<FromType, ToType>(GetOrCreateObjects<FromType>());
            }

            template<typename ObjectType>
            Iterator<ObjectType> CreateIterator(const Span& krcSpan) const {
                return Iterator<ObjectType>(GetObjects<ObjectType>(), krcSpan);
            }

            template<typename ObjectType>
            Iterator<ObjectType> CreateIterator() const {
                std::vector<ObjectType>* ptrObjects = GetObjects<ObjectType>();

                return Iterator<ObjectType>(ptrObjects, ptrObjects);
            }

            template<typename ObjectType>
            ObjectType& GetOrCreateObject(Reference& rcReference) {
                Storage* pcStorage = GetOrCreateStorage<ObjectType>();

                if (!rcReference)
                    pcStorage->New<ObjectType>(rcReference);
                
                return *pcStorage->GetPointer<ObjectType>(rcReference);
            }

            template<typename Visitor>
            void VisitStorages(Visitor& rVisitor) const {
                for (Storage::MapType::const_iterator it = m_mapTypeStorages.begin(); it != m_mapTypeStorages.end(); ++it) {
                    rVisitor(it->second);
                }
            }

            void SetVariableReference(const Word::IdType knNameWordId, const Reference& krcReference) {
                m_mapVariableReferences[knNameWordId] = krcReference;
            }

            const Reference* FindVariableReference(const Word::IdType knNameWordId) const {
                if (knNameWordId == ~0)
                    return NULL;

                Reference::MapType::const_iterator it = m_mapVariableReferences.find(knNameWordId);

                if (it != m_mapVariableReferences.end())
                    return &it->second;

                return NULL;
            }

            template<typename ObjectType>
            const Reference& GetOrCreateVariableObject(const Word::IdType knNameWordId) {
                Reference::MapType::const_iterator it = m_mapVariableReferences.find(knNameWordId);

                if (it == m_mapVariableReferences.end())
                    it = m_mapVariableReferences.insert(std::make_pair(knNameWordId, CreateObject<ObjectType>())).first;
                
                return it->second;
            }

            template<typename ObjectType>
            ObjectType* GetObjectByName(const Word::IdType knNameWordId) const {
                const Reference* kpcReference = FindVariableReference(knNameWordId);

                if (kpcReference == NULL)
                    return NULL;

                return GetObjectByReference<ObjectType>(*kpcReference);
            }


        private:
            Storage::MapType m_mapTypeStorages;
            Reference::MapType m_mapVariableReferences;
        };

        
        class Node
        {
        public:
            typedef size_t IdType;
            typedef std::list<IdType> IdListType;
            typedef std::stack<IdType> IdStackType;
            typedef std::deque<IdType> IdDequeType;
            typedef std::vector<IdType> IdVectorType;
            typedef std::vector<Node> VectorType;
            typedef std::map<Word::IdType, IdType> IdMapType;

            enum {
                MARK,
                WORD,
                NODE,
                SYMBOL,
                TARGET
            };

            Node()
            : m_nNodeId(~0)
            , m_vecChildren()
            { }

            Node(const Node& rhs)
            : m_nNodeId(rhs.m_nNodeId)
            , m_vecChildren(rhs.m_vecChildren)
            { }

            Node(const Node::IdType knNodeId)
            : m_nNodeId(knNodeId)
            , m_vecChildren()
            { }

            inline operator bool () const {
                return m_nNodeId < ~0;
            }

            inline bool operator == (const Node& rhs) const {
                return m_nNodeId == rhs.m_nNodeId;
            }

            inline bool operator != (const Node& rhs) const {
                return m_nNodeId != rhs.m_nNodeId;
            }


            inline bool HasChild(const Node::IdType knNodeId) const {
                for (Node::IdVectorType::const_iterator it = m_vecChildren.begin(); it != m_vecChildren.end(); ++it)
                    if (*it == knNodeId) 
                        return true;
                
                return false;
            }
            

            inline void AddChild(const Node::IdType knNodeId) {
                m_vecChildren.push_back(knNodeId);
            } 


            inline bool TryAddChild(const Node::IdType knNodeId) {
                if (HasChild(knNodeId))
                    return false;

                m_vecChildren.push_back(knNodeId);

                return true;
            } 

            inline const Node::IdVectorType& GetChildren() const {
                return m_vecChildren;
            }


            inline size_t GetChildrenCount() const {
                return m_vecChildren.size();
            }

            inline Node::IdType GetNodeId() const {
                return m_nNodeId;
            }

            inline const Node& GetNode(const Node::VectorType& krvecNodes) const {
                return krvecNodes[m_nNodeId];
            }

            inline const Node& GetChild(const Node::VectorType& krvecNodes, const size_t knIndex) const {
                const Node::IdType knNodeId(m_vecChildren[knIndex]);

                return krvecNodes[knNodeId];
            }

            inline const Node* GetOptionalChild(const Node::VectorType& krvecNodes, const size_t knIndex) const {

                if (m_vecChildren.size() <= knIndex)
                    return NULL;

                const Node::IdType knNodeId(m_vecChildren[knIndex]);

                if (krvecNodes.size() <= knNodeId)
                    return NULL;

                return &krvecNodes[knNodeId];
            }

            inline void SetInternalTarget(const Word::IdType knSymbolWordId, const Word::IdType knWordId, const Node::IdType knNodeId) {
                m_vecChildren.resize(4, ~0);
                m_vecChildren[WORD] = knWordId;
                m_vecChildren[NODE] = knNodeId;
                m_vecChildren[SYMBOL] = knSymbolWordId;
            }
            
            inline bool HasInternalTarget() const {
                return m_vecChildren.size() == 4
                    && m_vecChildren[MARK] == ~0
                    && m_vecChildren[WORD]  < ~0
                    && m_vecChildren[NODE] < ~0
                    && m_vecChildren[SYMBOL]  < ~0;
            }

            inline void SetExternalMark(const Word::IdType knSymbolWordId, const Word::IdType knWordId) {
                m_vecChildren.resize(5, ~0);
                m_vecChildren[WORD] = knWordId;
                m_vecChildren[SYMBOL] = knSymbolWordId;
            }

            inline void SetExternalTarget(const IndexType knTargetId, const Word::IdType knNodeId) {
                m_vecChildren[NODE] = knNodeId;
                m_vecChildren[TARGET] = knTargetId;   
            }

            inline bool HasExternalMark() const {
                return m_vecChildren.size() == 5
                    && m_vecChildren[MARK] == ~0
                    && m_vecChildren[WORD] < ~0
                    && m_vecChildren[NODE] == ~0
                    && m_vecChildren[SYMBOL] < ~0
                    && m_vecChildren[TARGET]  == ~0;
            }
            
            inline bool HasExternalTarget() const {
                return m_vecChildren.size() == 5
                    && m_vecChildren[MARK] == ~0
                    && m_vecChildren[WORD]  < ~0
                    && m_vecChildren[NODE]  < ~0
                    && m_vecChildren[SYMBOL] < ~0
                    && m_vecChildren[TARGET]  < ~0;
            }

            inline bool HasWordId() const {
                return m_vecChildren.size() > WORD
                    && m_vecChildren[MARK] == ~0
                    && m_vecChildren[WORD]  < ~0;
            }

            inline bool HasSymbolWordId() const {
                return m_vecChildren.size() > SYMBOL
                    && m_vecChildren[MARK] == ~0
                    && m_vecChildren[SYMBOL] < ~0;
            }

            inline bool HasTargetMark() const {
                return m_vecChildren.size() > MARK
                    && m_vecChildren[MARK] == ~0;
            }
            
            inline const Word::IdType& GetWordId() const {
                return m_vecChildren[WORD];
            }

            inline const Word::IdType& GetSymbolWordId() const {
                return m_vecChildren[SYMBOL];
            }

            inline Node::IdType GetTargetNodeId() const {
                return m_vecChildren[NODE];
            }

            inline IndexType GetExternalTargetId() const {
                return m_vecChildren[TARGET];
            }
        private:
            Node::IdType m_nNodeId;
            Node::IdVectorType m_vecChildren;
        };


        class Link
        {
        public:
            typedef std::list<Link> ListType;
            typedef std::stack<Link> StackType;
            typedef std::map<Word::IdType, Link> MapType;

            Link();
            Link(const Link& rhs);
            Link(const IndexType knTargetIndex, const Node::IdType knNodeId);

            operator bool() const;

            bool operator == (const Link& rhs) const;
            bool operator != (const Link& rhs) const;
        public:
            IndexType m_nTargetIndex;
            Node::IdType m_nNodeId;
        };

        class Linker
        {
        public:
            typedef std::map<Word::IdType, Link::MapType> MapType;

            Linker();
            Linker(const Linker& rhs);

            void Clear();

            const Link* FindLink(const Word::IdType knWordId, const Word::IdType knSymbolWordId = ~0) const;

            template<typename TreeProvider>
            inline Error SetDefinitions(const TreeProvider& krTreeProvider) {
                m_mapSymbolLinks.clear();

                for (IndexType nTargetIndex = 0; nTargetIndex < krTreeProvider.size(); ++nTargetIndex) {
                    const Tree& krcTree = krTreeProvider[nTargetIndex];
                    const Symbol::MapType& krmapSymbols = krcTree.GetSymbols();

                    for (Symbol::MapType::const_iterator symbol = krmapSymbols.begin(); symbol != krmapSymbols.end(); ++symbol) {
                        Link::MapType& rmapLinks = m_mapSymbolLinks[symbol->first];
                        const Node::IdMapType& krmapDefinitions = symbol->second.GetDefinitions();

                        for (Node::IdMapType::const_iterator definition = krmapDefinitions.begin(); definition != krmapDefinitions.end(); ++definition) {
                            Link::MapType::iterator link = rmapLinks.find(definition->first);

                            if (link == rmapLinks.end())
                                rmapLinks.insert(std::make_pair(definition->first, Link(nTargetIndex, definition->second)));
                            else
                                return Error(1, "definition duplicate");
                        }
                    }
                }

                return NULL;
            }

            template<typename TreeProvider>
            inline Error CheckCircularity(const TreeProvider& krTreeProvider) const {
                for (MapType::const_iterator links = m_mapSymbolLinks.begin(); links != m_mapSymbolLinks.end(); ++links) {
                    for (Link::MapType::const_iterator link = links->second.begin(); link != links->second.end(); ++link) {

                        Link::ListType lstStack;

                        lstStack.push_back(link->second);

                        do {
                            const Link kcDefinitionLink = lstStack.front();
                            lstStack.pop_front();

                            if (!kcDefinitionLink)
                                continue;

                            if (kcDefinitionLink.m_nTargetIndex >= krTreeProvider.size())
                                continue;

                            const Tree& krcDefinitionTree = krTreeProvider[kcDefinitionLink.m_nTargetIndex];

                            if (!krcDefinitionTree.HasNode(kcDefinitionLink.m_nNodeId))
                                continue;

                            const Node& krcDefinitionNode = krcDefinitionTree.GetNode(kcDefinitionLink.m_nNodeId);

                            const Node::IdVectorType& krvecChildren = krcDefinitionNode.GetChildren();

                            for (Node::IdVectorType::const_iterator child = krvecChildren.begin(); child != krvecChildren.end(); ++child) {
                                const Node& krcChildNode = krcDefinitionTree.GetNode(*child);

                                if (krcChildNode.HasWordId() && krcChildNode.HasSymbolWordId()) {
                                    const Word::IdType knDeclarationWordId = krcChildNode.GetWordId();
                                    const Word::IdType knDeclarationSymbolWordId = krcChildNode.GetSymbolWordId();

                                    const Link* kpcDeclarationLink = FindLink(krcChildNode.GetWordId(), krcChildNode.GetSymbolWordId());

                                    if (kpcDeclarationLink == NULL)
                                        continue;

                                    if (kpcDeclarationLink->m_nTargetIndex >= krTreeProvider.size())
                                        continue;

                                    const Tree& krcDeclarationTree = krTreeProvider[kpcDeclarationLink->m_nTargetIndex];
                                    
                                    if (!krcDeclarationTree.HasNode(kpcDeclarationLink->m_nNodeId))
                                        continue;

                                    const Node& krcDeclarationNode = krcDeclarationTree.GetNode(kpcDeclarationLink->m_nNodeId);


                                    for (Link::ListType::iterator stack = lstStack.begin(); stack != lstStack.end(); ++stack) {
                                        if (*stack == kcDefinitionLink)
                                            return Error(1, "Circular definition is invalid");

                                        if (*stack == *kpcDeclarationLink)
                                            return Error(2, "Circular declaration is invalid");
                                    }

                                    lstStack.push_back(*kpcDeclarationLink);
                                }
                            }
                        } while (lstStack.size());
                    }
                }

                return Error();
            }

            template<typename TreeProvider>
            inline Error CheckExternalDeclarations(const TreeProvider& krTreeProvider, const bool kbRetarget = false) const {
                for (MapType::const_iterator links = m_mapSymbolLinks.begin(); links != m_mapSymbolLinks.end(); ++links) {

                    Link::MapType::const_iterator link = links->second.begin();

                    if (link == links->second.end())
                        continue;

                    if (link->second.m_nTargetIndex >= krTreeProvider.size())
                        continue;

                    const Tree& krcDefinitionTree = krTreeProvider[link->second.m_nTargetIndex];

                    const Symbol* kpcDefinitionSymbol = krcDefinitionTree.FindSymbol(links->first);

                    if (kpcDefinitionSymbol == NULL)
                        continue;

                    const Node::IdMapType& krmapDeclarations = kpcDefinitionSymbol->GetExternalDeclarations();

                    for (Node::IdMapType::const_iterator declaration = krmapDeclarations.begin(); declaration != krmapDeclarations.end(); ++declaration) {
                        Link::MapType::const_iterator link = links->second.find(declaration->first);

                        if (link == links->second.end())
                            return Error(1, "unresolved external declaration");
                    }
                }

                return Error();
            }

            template<typename TreeProvider>
            inline Error LinkExternalDeclarations(TreeProvider& rTreeProvider, const bool kbRetarget = false) const {
                for (MapType::const_iterator links = m_mapSymbolLinks.begin(); links != m_mapSymbolLinks.end(); ++links) {

                    Link::MapType::const_iterator link = links->second.begin();

                    if (link == links->second.end())
                        continue;

                    if (link->second.m_nTargetIndex >= rTreeProvider.size())
                        continue;

                    Tree& rcDefinitionTree = rTreeProvider[link->second.m_nTargetIndex];

                    const Symbol* kpcDefinitionSymbol = rcDefinitionTree.FindSymbol(links->first);

                    if (kpcDefinitionSymbol == NULL)
                        continue;

                    const Node::IdMapType& krmapDeclarations = kpcDefinitionSymbol->GetExternalDeclarations();

                    if (krmapDeclarations.empty())
                        continue;

                    IndexType nLinkedCount = 0;

                    for (Node::IdMapType::const_iterator declaration = krmapDeclarations.begin(); declaration != krmapDeclarations.end(); ++declaration) {

                        const Word::IdType knWordId = declaration->first;
                        const Node::IdType knDeclarationNodeId = declaration->second;
                        Node& rcDeclarationNode = rcDefinitionTree.GetNode(knDeclarationNodeId);

                        if (rcDeclarationNode.HasExternalMark() || (kbRetarget && rcDeclarationNode.HasExternalTarget())) {
                            Link::MapType::const_iterator definitionLink = links->second.find(knWordId);

                            if (definitionLink == links->second.end())
                                continue;

                            const IndexType knTargetIndex = definitionLink->second.m_nTargetIndex;
                            const Node::IdType knNodeId = definitionLink->second.m_nNodeId;

                            if (knTargetIndex >= rTreeProvider.size())
                                continue;

                            const Tree& krcDefinitionTree = rTreeProvider[knTargetIndex];

                            if (!krcDefinitionTree.HasNode(knNodeId))
                                continue;

                            rcDeclarationNode.SetExternalTarget(knTargetIndex, knNodeId);
                        }

                        if (rcDeclarationNode.HasExternalTarget())
                            ++nLinkedCount;
                    }
                }

                return Error();
            }
        private:
            MapType::iterator FindLinks(const Word::IdType knSymbolWordId = ~0);
            MapType::const_iterator FindLinks(const Word::IdType knSymbolWordId = ~0) const;
        private:
            MapType m_mapSymbolLinks;
        };

        class Symbol
        {
        public:
            typedef std::map<Word::IdType, Symbol> MapType;

            Symbol();
            Symbol(const Symbol& rhs);

            Symbol& operator = (const Symbol& rhs);

            void Clear();

            const Word::IdType& GetWordId() const;

            bool IsShared() const;


            Node::IdMapType& GetDefinitions();
            Node::IdMapType& GetInternalDeclarations();
            Node::IdMapType& GetExternalDeclarations();

            const Node::IdMapType& GetDefinitions() const;
            const Node::IdMapType& GetInternalDeclarations() const;
            const Node::IdMapType& GetExternalDeclarations() const;

            Node* FindDefinitionNode(Node::VectorType& krvecNodes, const Word::IdType knWordId);
            Node* FindInternalDeclarationNode(Node::VectorType& krvecNodes, const Word::IdType knWordId);
            Node* FindExternalDeclarationNode(Node::VectorType& krvecNodes, const Word::IdType knWordId);

            const Node* FindDefinitionNode(const Node::VectorType& krvecNodes, const Word::IdType knWordId) const;
            const Node* FindInternalDeclarationNode(const Node::VectorType& krvecNodes, const Word::IdType knWordId) const;
            const Node* FindExternalDeclarationNode(const Node::VectorType& krvecNodes, const Word::IdType knWordId) const;

            void SetShared(const bool kbShared);
            void SetWordId(const Word::IdType knWordId);
            void SetDefinition(
                const Node::IdType knNodeId,
                const Word::IdType knWordId
            );
            void SetDeclaration(
                const Node::IdType knNodeId,
                const Word::IdType knWordId
            );

            bool HasUnresolvedDeclarations() const;
            bool ResolveInternalDeclarations(Node::VectorType& rvecNodes);
        private:
            Node* FindNode(Node::VectorType& krvecNodes, const Node::IdMapType& krmap, const Word::IdType knWordId);
            const Node* FindNode(const Node::VectorType& krvecNodes, const Node::IdMapType& krmap, const Word::IdType knWordId) const;
        private:
            Node::IdMapType m_mapDefinitions;
            Node::IdMapType m_mapUnresolvedDeclarations;
            Node::IdMapType m_mapInternalDeclarations;
            Node::IdMapType m_mapExternalDeclarations;
            Word::IdType m_nWordId;
            bool m_bIsShared;
        };

        class Tree
        {
        public:
            Tree();
            Tree(const Tree& rhs);
            Tree& operator = (const Tree& rhs);

            void Clear();

            Node::IdType AddNode();

            bool HasNode(const Node::IdType knNodeId) const;
            Symbol::MapType& GetSymbols();

            Symbol& GetSymbol(const Word::IdType knSymbolWordId);
            Node::VectorType& GetNodes();

            Node& GetNode(const Node::IdType knNodeId);

            Symbol* FindSymbol(const Word::IdType knSymbolWordId);

            Node* FindDefinitionNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId = ~0);
            Node* FindInternalDeclarationNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId = ~0);
            Node* FindExternalDeclarationNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId = ~0);

            const Symbol::MapType& GetSymbols() const;
            const Node::VectorType& GetNodes() const;

            const Node& GetNode(const Node::IdType knNodeId) const;

            const Symbol* FindSymbol(const Word::IdType knTypeWordId) const;

            const Node* FindDefinitionNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId = ~0) const;
            const Node* FindInternalDeclarationNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId = ~0) const;
            const Node* FindExternalDeclarationNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId = ~0) const;

            
            bool HasUnresolvedDeclarations() const;
            bool HasExternalDeclarations() const;

            bool ResolveInternalDeclarations();
        private:
            Symbol::MapType::iterator FirstSymbol(const Word::IdType knSymbolWordId);
            Symbol::MapType::const_iterator FirstSymbol(const Word::IdType knSymbolWordId) const;
        private:
            Node::VectorType m_vecNodes;
            Symbol::MapType m_mapSymbols;
        };

        class Factory
        {
            template<typename T>
            Reference CreateObjectFor(Repository& rcRepository) const {
                return rcRepository.CreateObject<T>();
            }
            template<typename T>
            Reference CloneObjectFor(Repository& rcRepository, const Reference& krcReference) const {
                return rcRepository.CloneObject<T>(krcReference);
            }
            template<typename T>
            inline void* GetObjectFor(Repository& rcRepository, const Reference& krcReference) const {
                return rcRepository.GetObjectByReference<T>(krcReference);
            }
            template<typename T>
            inline void* GetObjectAtIndexFor(Repository& rcRepository, const IndexType knIndex) const {
                return rcRepository.GetPointer<T>(knIndex);
            }
        public:
            typedef size_t IdType;

            typedef std::vector<Factory> VectorType;
            typedef std::vector<const Factory*> PtrVectorType;

            Factory()
                : m_nTypeId(~0)
                , m_mptrCreateObject(NULL)
                , m_mptrCloneObject(NULL)
                , m_mptrGetObject(NULL)
                , m_mptrGetObjectAtIndex(NULL)
            { }

            Factory(const Factory& rhs)
                : m_nTypeId(rhs.m_nTypeId)
                , m_mptrCreateObject(rhs.m_mptrCreateObject)
                , m_mptrCloneObject(rhs.m_mptrCloneObject)
                , m_mptrGetObject(rhs.m_mptrGetObject)
                , m_mptrGetObjectAtIndex(rhs.m_mptrGetObjectAtIndex)
            { }

            $use_template(void, T, BindType) (const Reflection::Type& krcType) {
                m_nTypeId = krcType.GetId();
                m_mptrCreateObject = &Factory::CreateObjectFor<T>;
                m_mptrCloneObject = &Factory::CloneObjectFor<T>;
                m_mptrGetObject = &Factory::GetObjectFor<T>;
                m_mptrGetObjectAtIndex = &Factory::GetObjectAtIndexFor<T>;
            }


            inline const Reflection::Type& GetType() const {
                return Reflection::Type::Get(m_nTypeId);
            }

            inline const Reflection::Class* GetClass() const {
                return Reflection::Class::GetClass(GetType());
            }

            inline Reference CreateObject(Repository& rcRepository) const {
                return (this->*m_mptrCreateObject)(rcRepository);
            }
            inline Reference CloneObject(Repository& rcRepository, const Reference& krcReference) const {
                return (this->*m_mptrCloneObject)(rcRepository, krcReference);
            }
            inline void* GetObjectByReference(Repository& rcRepository, const Reference& krcReference) const {
                return (this->*m_mptrGetObject)(rcRepository, krcReference);
            }
            inline void* GetObjectAtIndex(Repository& rcRepository, const IndexType knIndex = 0) const {
                return (this->*m_mptrGetObjectAtIndex)(rcRepository, knIndex);
            }
        private:
            Reflection::Type::IdType m_nTypeId;

            Reference(Factory::* m_mptrCreateObject)(Repository&) const;
            Reference(Factory::* m_mptrCloneObject)(Repository&, const Reference&) const;
            void*(Factory::* m_mptrGetObject)(Repository&, const Reference&) const;
            void*(Factory::* m_mptrGetObjectAtIndex)(Repository&, const IndexType) const;
        };

        class Builder
        {
        public:
            Builder();
            Builder(const Builder& rhs);

            bool Build();
            bool Using(const Word::Handle::VectorType& krvecNamespaceWords);
            bool New(const Word::Handle& krcTypeWord, const Serialization::Descriptor& krcDescriptor);
            bool Var(const Reflection::Type& krcType, const Word::Handle& krcName, const Visitor::Slice& krsValue);
            bool Set(const Word::Handle& krcName, const Visitor::Slice& krsValue);

            inline const Reflection::Namespace::IdVectorType& GetNamespaceIds() const {
                return m_vecNamespaceIds;
            }
        private:
            bool CreateObject(
                Runner::Processor& rcProcessor, 
                const Factory* kpcFactory,
                const Word::Handle& krcMethodName,
                const Serialization::Descriptor& krcDescriptor
            );

            bool Dispatch(
                Runner::Processor& rcProcessor, 
                const Word::Handle& krcMethodName, 
                const Serialization::Descriptor& krcDescriptor
            );
        private:
            Data::Reference::VectorType m_vecCreatedObjectRefs;
            Reflection::Namespace::IdVectorType m_vecNamespaceIds;
            Error m_cError;
        };
    }
}