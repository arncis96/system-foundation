#include "Binding.h"


namespace System
{
    namespace Binding
    {
        References::References()
            : m_vecInvokers()
            , m_vecActions()
            , m_vecMethodIds()
        { }
        References::References(const References& rhs)
            : m_vecInvokers(rhs.m_vecInvokers)
            , m_vecActions(rhs.m_vecActions)
            , m_vecMethodIds(rhs.m_vecMethodIds)
        { }

        void References::Resize(const size_t knSize) {
            if (m_vecActions.size() == knSize)
                return;

            m_vecInvokers.resize(knSize);
            m_vecActions.resize(knSize);
            m_vecMethodIds.resize(knSize, ~0);
        }
        const size_t References::GetSize() const {
            return m_vecInvokers.size();
        }

        size_t References::Add(
            const Data::Reference& krcInvoker, 
            const Data::Reference& krcAction,
            const Reflection::Method::IdType& knMethodId
        ) {
            size_t knIndex = m_vecInvokers.size();

            m_vecInvokers.push_back(krcInvoker);
            m_vecActions.push_back(krcAction);
            m_vecMethodIds.push_back(knMethodId);
            
            return knIndex;
        }

        Data::Reference& References::GetInvoker(const size_t knIndex) {
            return m_vecInvokers[knIndex];
        }
        Data::Reference& References::GetAction(const size_t knIndex) {
            return m_vecActions[knIndex];
        }
        Reflection::Method::IdType& References::GetMethodId(const size_t knIndex) {
            return m_vecMethodIds[knIndex];
        }

        const Data::Reference& References::GetInvoker(const size_t knIndex) const {
            return m_vecInvokers[knIndex];
        }
        const Data::Reference& References::GetAction(const size_t knIndex) const {
            return m_vecActions[knIndex];
        }
        const Reflection::Method::IdType& References::GetMethodId(const size_t knIndex) const {
            return m_vecMethodIds[knIndex];
        }

        Targets::Targets()
            : m_cReferences()
        { }

        Targets::Targets(const Targets& rhs)
            : m_cReferences(rhs.m_cReferences)
        { }

        bool Targets::Bind(
            Parser & rcBindingParser,
            Data::Reference & rcTargetRef,
            Data::Reference & rcActionRef,
            Reflection::Method::IdType & rnMethodId,
            const Reflection::Class * kpcClass,
            const Word::Handle & krcMethodName,
            const Serialization::Descriptor & krcDescriptor,
            const Serialization::Token::VectorType & krvecTokens,
            const Data::IndexType knObjectInstanceIndex
        ) {
            const Factory* kpcFactory = FindFactory(kpcClass, krcMethodName.GetId(), rnMethodId);

            if (kpcFactory == NULL)
                return false;

            return kpcFactory->Bind(
                rcBindingParser,
                rcTargetRef,
                rcActionRef,
                krcDescriptor,
                krvecTokens,
                knObjectInstanceIndex
            );
        }

        void Targets::Clear() {
            m_cReferences.Resize(0);
        }

        bool Targets::Bind(
            Parser& rcBindingParser,
            const Serialization::Descriptors& krcDescriptors,
            const Serialization::Token::VectorType& krvecTokens,
            Data::IndexVectorType& rvecAbsentIds
        ) {
            bool bComplete = true;

            size_t c = krcDescriptors.GetDescriptorCount();

            m_cReferences.Resize(c);

            for (size_t i = 0; i < c; ++i) {
                if (Bind(rcBindingParser, krcDescriptors, krvecTokens, i))
                    continue;

                rvecAbsentIds.push_back(i);

                bComplete = false;
            }
            
            return bComplete;
        }


        bool Targets::Rebind(
            Parser& rcBindingParser,
            const Serialization::Descriptors& krcDescriptors,
            const Serialization::Token::VectorType& krvecTokens,
            Data::IndexVectorType& rvecAbsentIds,
            Data::IndexVectorType& rvecReadyIds
        ) {
            bool bComplete = true;

            for (Data::IndexVectorType::iterator it = rvecAbsentIds.begin(); it != rvecAbsentIds.end(); ) {
                const Data::IndexType knIndex = *it;

                if (Bind(rcBindingParser, krcDescriptors, krvecTokens, knIndex)) {
                    it = rvecAbsentIds.erase(it);

                    rvecReadyIds.push_back(knIndex);
                }
                else {
                    ++it;

                    bComplete = false;
                }
            }


            return bComplete;
        }

        bool Targets::Bind(
            Parser& rcBindingParser,
            const Serialization::Descriptors& krcDescriptors,
            const Serialization::Token::VectorType& krvecTokens,
            const Data::IndexType knIndex
        ) {
            Reflection::Method::IdType& rnMethodId = m_cReferences.GetMethodId(knIndex);

            const Serialization::Descriptor& krcDescriptor = krcDescriptors.GetDescriptor(knIndex);
            const Factory* kpcFactory = NULL;
            
            if (rnMethodId < ~0)
                kpcFactory = GetFactory(knIndex);
            else
                kpcFactory = FindFactory(
                    rcBindingParser.m_kpcBuilder->GetNamespaceIds(), 
                    krcDescriptor.GetToken(krvecTokens).GetWordId(),
                    rnMethodId
                );

            if (kpcFactory == NULL)
                return false;

            return kpcFactory->Bind(
                rcBindingParser,
                m_cReferences.GetInvoker(knIndex),
                m_cReferences.GetAction(knIndex),
                krcDescriptor,
                krvecTokens
            );
        }

        bool Targets::Bind(
            Data::Repository& rcRepository,
            const Serialization::Descriptor::IdType knId,
            const Data::IndexType knThisIndex
        ) {
            const Factory* kpcFactory = GetFactory(knId);
            void* ptrThis = kpcFactory->GetThis(rcRepository, knThisIndex);

            return kpcFactory->SetThis(rcRepository, m_cReferences.GetInvoker(knId), ptrThis);
        }

        bool Targets::Bind(
            Data::Repository& rcRepository,
            const Data::IndexType knThisIndex
        ) {
            for (size_t i = 0, c = m_cReferences.GetSize(); i < c; ++i) {
                const Factory* kpcFactory = GetFactory(i);

                if (kpcFactory == NULL)
                    return false;

                void* ptrThis = kpcFactory->GetThis(rcRepository, knThisIndex);

                if (!kpcFactory->SetThis(rcRepository, m_cReferences.GetInvoker(i), ptrThis))
                    return false;
            }

            return true;
        }

        const Reflection::Method& Targets::GetMethod(const Serialization::Descriptor::IdType knId) const {
            return Reflection::Method::Get(m_cReferences.GetMethodId(knId));
        }

        const Data::Reference& Targets::GetAction(const Serialization::Descriptor::IdType knId) const {
            return m_cReferences.GetAction(knId);
        }

        bool Targets::Check(Data::Repository& rcRepository, const Serialization::Descriptor::IdType knId) const {
            const Reflection::Method::IdType& krnMethodId = m_cReferences.GetMethodId(knId);
            const Data::Reference& krcTargetRef = m_cReferences.GetInvoker(knId);
            const Data::Reference& krcActionRef = m_cReferences.GetAction(knId);

            if (krnMethodId == ~0)
                return false;

            if (!krcTargetRef)
                return false;

            if (!krcTargetRef)
                return false;

            const Factory* kpcFactory = GetFactory(knId);

            if (kpcFactory == NULL)
                return false;

            void* ptrThis = kpcFactory->GetThis(rcRepository, krcTargetRef);

            if (ptrThis == NULL)
                return false;

            return true;
        }

        size_t Targets::GetActionsCount() const {
            return m_cReferences.GetSize();
        }

        const Factory* Targets::GetFactory(const Serialization::Descriptor::IdType knId) const {
            const Reflection::Method& krcMethod = GetMethod(knId);

            return Reflection::Attribute::FindPointer<Factory>(krcMethod.GetAttributes());
        }

        const Factory* Targets::FindFactory(
            const Reflection::Namespace::IdVectorType& krvecNamespaceIds,
            const Word::IdType knMethodWordId,
            Reflection::Method::IdType& rnOutMethodId
        ) {
            const Factory* kpcFactory = NULL;
            for (Reflection::Namespace::IdVectorType::const_iterator it = krvecNamespaceIds.begin(); !kpcFactory && it != krvecNamespaceIds.end(); ++it) {
                kpcFactory = Reflection::Namespace::Get(*it).FindMethodAttribute<Factory>(knMethodWordId, rnOutMethodId);
            }

            return kpcFactory;
        }
        const Factory* Targets::FindFactory(
            const Reflection::Class* kpcClass,
            const Word::IdType knMethodWordId,
            Reflection::Method::IdType& rnOutMethodId
        ) {
            if (kpcClass == NULL)
                return NULL;

            const Reflection::Method* kpcMethod = kpcClass->GetMethod(knMethodWordId);

            if (kpcMethod == NULL)
                return NULL;

            const Factory* kpcFactory = Reflection::Attribute::FindPointer<Factory>(kpcMethod->GetAttributes());

            if (kpcFactory == NULL)
                return NULL;

            rnOutMethodId = kpcMethod->GetId();

            return kpcFactory;
        }



        Interfaces::Interfaces()
            : m_mapReferences()
        { }
        Interfaces::Interfaces(const Interfaces& rhs)
            : m_mapReferences(rhs.m_mapReferences)
        { }

        void Interfaces::Clear() {
            m_mapReferences.clear();
        }

        struct Interfaces::Binder {
            Data::Repository* repo;
            References::MapType* refs;

            Binder(
                Data::Repository* pcRepository,
                const Reflection::Namespace::IdVectorType& krvecNamespaceIds,
                References::MapType& rmapRefs
            ) 
            : repo(pcRepository)
            , refs(&rmapRefs) {
                for (Reflection::Namespace::IdVectorType::const_iterator it = krvecNamespaceIds.begin(); it != krvecNamespaceIds.end(); ++it) {
                    Reflection::Namespace::Get(*it).VisitMethodAttributes<Interface>(*this);
                }
            }

            inline operator bool() const {
                return refs->size() > 0;
            }

            inline void operator() (
                const Reflection::Type& krcType, 
                const Reflection::Method& krcMethod, 
                const Interface& krcInterface
            ) {
                const Word::IdType knNameId = krcMethod.GetName().GetWordId();
                void* ptrThis = krcInterface.GetThis(*repo, 0);
                
                if (!ptrThis) {
                    return;
                }

                const Data::Reference kcInvoker = krcInterface.CreateInvoker(*repo, ptrThis);
                const Data::Reference kcAction = krcInterface.CreateFunc(*repo, kcInvoker);

                References& rcReferences = refs->operator[](knNameId);

                size_t knIndex = rcReferences.Add(kcInvoker, kcAction, krcMethod.GetId());
            }
        };



        bool Interfaces::Bind(
            Data::Repository* pcRepository,
            const Reflection::Namespace::IdVectorType& krvecNamespaceIds
        ) {
            return Binder(pcRepository, krvecNamespaceIds, m_mapReferences);
        }


        const References* Interfaces::FindReferences(const Word::IdType knInterfaceWordId) const {
            References::MapType::const_iterator it = m_mapReferences.find(knInterfaceWordId);

            if (it != m_mapReferences.end())
                return &it->second;

            return NULL;
        }
    }
}