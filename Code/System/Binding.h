#pragma once

#include "Data.h"
#include "Delegate.h"
#include "Visitor.h"
#include "Serialization.h"
#include "TypeTraits.h"

namespace System
{
    namespace Binding
    {

        #pragma region this
        template<typename C>
        struct This
        {
            This()
            : m_ptr(NULL)
            { }
            This(const This& rhs)
            : m_ptr(rhs.m_ptr)
            { }
            This(void* ptr)
            : m_ptr(static_cast<C*>(ptr))
            { }

            inline This& operator = (void* ptr) {
                m_ptr = static_cast<C*>(ptr);

                return *this;
            }

            inline operator bool() const {
                return m_ptr != NULL;
            }
            
            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                rVisitor(*this);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                rVisitor(*this);
            }

            C* m_ptr;
        };

        template<>
        struct This<void> 
        {
            This() : m_ptr(NULL) { }
            This(const This&) { }
            This(void*) { }

            inline This& operator = (void*) {
                return *this;
            }

            inline operator bool() const {
                return true;
            }
            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) { }
            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const { }

            void* m_ptr;
        };
        #pragma endregion

        template<typename M>
        struct Method
        {
            typedef M MethodType;

            Method()
                : m_ptr(NULL)
            { }

            Method(const MethodType& krptr)
                : m_ptr(krptr)
            { }

            Method(const Method& rhs)
                : m_ptr(rhs.m_ptr)
            { }

            inline operator bool() const {
                return m_ptr != NULL;
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                rVisitor(*this);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                rVisitor(*this);
            }

            MethodType m_ptr;
        };

        template<typename I>
        struct InterfaceBase
        {
            typedef I InterfaceType;
        };

        template<typename C, typename R, typename S, typename M>
        struct InvokerBase
        {
            typedef R ResultType;
            typedef C ThisType;
            typedef S SignatureType;
            typedef M MethodType;

            InvokerBase()
                : m_this()
                , m_method()
            { }

            InvokerBase(const InvokerBase& rhs)
                : m_this(rhs.m_this)
                , m_method(rhs.m_method)
            { }

            inline void SetMethod(const M& krMethod) {
                m_method = krMethod;
            }

            inline void SetThis(const This<C>& krcThis) {
                m_this = krcThis;
            }
            inline void Set(const This<C>& krcThis, const M& krMethod) {
                m_this = krcThis;
                m_method = krMethod;
            }

            inline void* GetThis() const {
                return m_this.m_ptr;
            }

            inline operator bool() const {
                return m_this && m_method;
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                m_this.Visit(rVisitor);
                m_method.Visit(rVisitor);;
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                m_this.Visit(rVisitor);
                m_method.Visit(rVisitor);
            }

            This<C> m_this;
            Method<M> m_method;
        };

        template<typename T>
        struct Slot
        {
            typedef T Type;
            typedef std::vector<T> VectorType;

            VectorType* m_pvecContainer;
            Data::IndexType m_nIndex; 

            Slot() {}
            Slot(const Slot& rhs)
                : m_pvecContainer(rhs.m_pvecContainer)
                , m_nIndex(rhs.m_nIndex)
            { }

            inline T& Create(Data::Repository* pcRepository) {
                m_pvecContainer = pcRepository->GetOrCreateObjects<T>();
                m_nIndex = m_pvecContainer->size();

                m_pvecContainer->push_back(T());

                return m_pvecContainer->back();
            }

            inline void Use(const Reflection::Constant& krcConstant) {
                m_pvecContainer = &Reflection::Constant::GetValues<T>();
                m_nIndex = krcConstant.GetValueId();
            }

            inline void Use(Data::Repository* pcRepository, const Data::Reference& krcReference) {
                m_pvecContainer = pcRepository->GetOrCreateObjects<T>();
                m_nIndex = krcReference.GetIndex();
            }


            inline operator T& () const {
                return m_pvecContainer->operator[](m_nIndex);
            }
            inline operator T* () const {
                return &m_pvecContainer->operator[](m_nIndex);
            }
            inline operator const T& () const {
                return m_pvecContainer->operator[](m_nIndex);
            }
            inline operator const T* () const {
                return &m_pvecContainer->operator[](m_nIndex);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                rVisitor(*this);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                rVisitor(*this);
            }

            inline static void Visit() { }
        };

        template<typename T>
        struct Value
        {
            typedef T Type;

            T m_value;

            Value() {}
            Value(const Value& rhs)
                : m_value(rhs.m_value)
            { }
            explicit Value(const T& value)
                : m_value(value)
            { }

            inline operator const T& () const {
                return m_value;
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                rVisitor(*this);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                rVisitor(*this);
            }

            inline static void Visit() {
                Reflection::Type::GetOrCreateType<T>();
            }
        };

        template<typename T>
        struct Handle
        {
            typedef T Type;
            typedef size_t IdType;

            IdType m_nId;

            Handle()
                : m_nId(~0)
            { }
            Handle(const Handle& rhs)
                : m_nId(rhs.m_nId)
            { }
            explicit Handle(const T& krcInstance)
                : m_nId(krcInstance.GetId())
            { }

            inline operator const T& () const {
                return T::Get(m_nId);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                rVisitor(*this);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                rVisitor(*this);
            }

            inline static void Visit() { }
        };



        #pragma region argument
        template<typename T>
        struct Argument : Value<T> { };

        template<typename T>
        struct Argument<const T> : Value<T> { };

        template<>
        struct Argument<Word::Handle> : Value<Word::Handle> { };

        template<>
        struct Argument<Word::Handle::VectorType> : Value<Word::Handle::VectorType> { };

        template<>
        struct Argument<const Word::Handle> : Value<Word::Handle> { };

        template<>
        struct Argument<const Word::Handle::VectorType> : Value<Word::Handle::VectorType> { };

        template<>
        struct Argument<const Word::Handle&> : Value<Word::Handle> { };

        template<>
        struct Argument<const Word::Handle::VectorType&> : Value<Word::Handle::VectorType> { };

        template<>
        struct Argument<const char*> : Value<std::string> {

            inline operator const char* () const {
                return m_value.c_str();
            }
        };

        template<>
        struct Argument<const Visitor::Slice&> : Value<Visitor::Slice> { };
        template<>
        struct Argument<const Visitor::Slices&> : Value<Visitor::Slices> { };

        template<>
        struct Argument<Serialization::Descriptor&> : Value<Serialization::Descriptor> { };
        template<>
        struct Argument<const Serialization::Descriptor&> : Value<Serialization::Descriptor> { };

        template<typename T>
        struct Argument<Data::Parameter<T>&> : Value< Data::Parameter<T> > { };
        template<typename T>
        struct Argument<const Data::Parameter<T>&> : Value< Data::Parameter<T> > { };


        template<typename T>
        struct Argument<T&> : Slot<T> { };

        template<typename T>
        struct Argument<T*> : Slot<T> { };


        template<typename T>
        struct Argument<const T&> : Slot<T> { };

        template<typename T>
        struct Argument<const T*> : Slot<T> { };

        template<>
        struct Argument<const Reflection::Type&> : Handle<Reflection::Type> { };

        template<>
        struct Argument<const Reflection::Namespace&> : Handle<Reflection::Namespace> { };

        template<>
        struct Argument<const Reflection::Method&> : Handle<Reflection::Method> { };

        #pragma endregion
        
        #pragma region arguments
        template<typename T>
        struct ArgumentsPack;

        template<typename R>
        struct ArgumentsPack<R(*)()>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs) { }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) { }
            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const { }

            inline static void Visit() { }
        };

        template<typename R, typename T1>
        struct ArgumentsPack<R(*)(T1)>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs)
            : a1(rhs.a1)
            { }
            
            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                a1.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                a1.Visit(rVisitor);
            }

            inline static void Visit() {
                Argument<T1>::Visit();
            }

            Argument<T1> a1;
        };

        template<typename R, typename T1, typename T2>
        struct ArgumentsPack<R(*)(T1,T2)>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs)
            : a1(rhs.a1)
            , a2(rhs.a2)
            { }
            
            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
            }

            inline static void Visit() {
                Argument<T1>::Visit();
                Argument<T2>::Visit();
            }

            Argument<T1> a1;
            Argument<T2> a2;
        };

        template<typename R, typename T1, typename T2, typename T3>
        struct ArgumentsPack<R(*)(T1,T2,T3)>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs)
            : a1(rhs.a1)
            , a2(rhs.a2)
            , a3(rhs.a3)
            { }
            
            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
            }

            inline static void Visit() {
                Argument<T1>::Visit();
                Argument<T2>::Visit();
                Argument<T3>::Visit();
            }

            Argument<T1> a1;
            Argument<T2> a2;
            Argument<T3> a3;
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4>
        struct ArgumentsPack<R(*)(T1,T2,T3,T4)>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs)
            : a1(rhs.a1)
            , a2(rhs.a2)
            , a3(rhs.a3)
            , a4(rhs.a4)
            { }
            
            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
            }

            inline static void Visit() {
                Argument<T1>::Visit();
                Argument<T2>::Visit();
                Argument<T3>::Visit();
                Argument<T4>::Visit();
            }

            Argument<T1> a1;
            Argument<T2> a2;
            Argument<T3> a3;
            Argument<T4> a4;
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
        struct ArgumentsPack<R(*)(T1,T2,T3,T4,T5)>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs)
            : a1(rhs.a1)
            , a2(rhs.a2)
            , a3(rhs.a3)
            , a4(rhs.a4)
            , a5(rhs.a5)
            { }
            
            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
            }

            inline static void Visit() {
                Argument<T1>::Visit();
                Argument<T2>::Visit();
                Argument<T3>::Visit();
                Argument<T4>::Visit();
                Argument<T5>::Visit();
            }

            Argument<T1> a1;
            Argument<T2> a2;
            Argument<T3> a3;
            Argument<T4> a4;
            Argument<T5> a5;
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
        struct ArgumentsPack<R(*)(T1, T2, T3, T4, T5, T6)>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs)
                : a1(rhs.a1)
                , a2(rhs.a2)
                , a3(rhs.a3)
                , a4(rhs.a4)
                , a5(rhs.a5)
                , a6(rhs.a6)
            { }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
                a6.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
                a6.Visit(rVisitor);
            }

            inline static void Visit() {
                Argument<T1>::Visit();
                Argument<T2>::Visit();
                Argument<T3>::Visit();
                Argument<T4>::Visit();
                Argument<T5>::Visit();
                Argument<T6>::Visit();
            }

            Argument<T1> a1;
            Argument<T2> a2;
            Argument<T3> a3;
            Argument<T4> a4;
            Argument<T5> a5;
            Argument<T6> a6;
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
        struct ArgumentsPack<R(*)(T1, T2, T3, T4, T5, T6, T7)>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs)
                : a1(rhs.a1)
                , a2(rhs.a2)
                , a3(rhs.a3)
                , a4(rhs.a4)
                , a5(rhs.a5)
                , a6(rhs.a6)
                , a7(rhs.a7)
            { }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
                a6.Visit(rVisitor);
                a7.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
                a6.Visit(rVisitor);
                a7.Visit(rVisitor);
            }

            inline static void Visit() {
                Argument<T1>::Visit();
                Argument<T2>::Visit();
                Argument<T3>::Visit();
                Argument<T4>::Visit();
                Argument<T5>::Visit();
                Argument<T6>::Visit();
                Argument<T7>::Visit();
            }

            Argument<T1> a1;
            Argument<T2> a2;
            Argument<T3> a3;
            Argument<T4> a4;
            Argument<T5> a5;
            Argument<T6> a6;
            Argument<T7> a7;
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
        struct ArgumentsPack<R(*)(T1, T2, T3, T4, T5, T6, T7, T8)>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs)
                : a1(rhs.a1)
                , a2(rhs.a2)
                , a3(rhs.a3)
                , a4(rhs.a4)
                , a5(rhs.a5)
                , a6(rhs.a6)
                , a7(rhs.a7)
                , a8(rhs.a8)
            { }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
                a6.Visit(rVisitor);
                a7.Visit(rVisitor);
                a8.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
                a6.Visit(rVisitor);
                a7.Visit(rVisitor);
                a8.Visit(rVisitor);
            }

            inline static void Visit() {
                Argument<T1>::Visit();
                Argument<T2>::Visit();
                Argument<T3>::Visit();
                Argument<T4>::Visit();
                Argument<T5>::Visit();
                Argument<T6>::Visit();
                Argument<T7>::Visit();
                Argument<T8>::Visit();
            }

            Argument<T1> a1;
            Argument<T2> a2;
            Argument<T3> a3;
            Argument<T4> a4;
            Argument<T5> a5;
            Argument<T6> a6;
            Argument<T7> a7;
            Argument<T8> a8;
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
        struct ArgumentsPack<R(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9)>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs)
                : a1(rhs.a1)
                , a2(rhs.a2)
                , a3(rhs.a3)
                , a4(rhs.a4)
                , a5(rhs.a5)
                , a6(rhs.a6)
                , a7(rhs.a7)
                , a8(rhs.a8)
                , a9(rhs.a9)
            { }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
                a6.Visit(rVisitor);
                a7.Visit(rVisitor);
                a8.Visit(rVisitor);
                a9.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
                a6.Visit(rVisitor);
                a7.Visit(rVisitor);
                a8.Visit(rVisitor);
                a9.Visit(rVisitor);
            }

            inline static void Visit() {
                Argument<T1>::Visit();
                Argument<T2>::Visit();
                Argument<T3>::Visit();
                Argument<T4>::Visit();
                Argument<T5>::Visit();
                Argument<T6>::Visit();
                Argument<T7>::Visit();
                Argument<T8>::Visit();
                Argument<T9>::Visit();
            }

            Argument<T1> a1;
            Argument<T2> a2;
            Argument<T3> a3;
            Argument<T4> a4;
            Argument<T5> a5;
            Argument<T6> a6;
            Argument<T7> a7;
            Argument<T8> a8;
            Argument<T9> a9;
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
        struct ArgumentsPack<R(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)>
        {
            ArgumentsPack() { }
            ArgumentsPack(const ArgumentsPack& rhs)
                : a1(rhs.a1)
                , a2(rhs.a2)
                , a3(rhs.a3)
                , a4(rhs.a4)
                , a5(rhs.a5)
                , a6(rhs.a6)
                , a7(rhs.a7)
                , a8(rhs.a8)
                , a9(rhs.a9)
                , a10(rhs.a10)
            { }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
                a6.Visit(rVisitor);
                a7.Visit(rVisitor);
                a8.Visit(rVisitor);
                a9.Visit(rVisitor);
                a10.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                a1.Visit(rVisitor);
                a2.Visit(rVisitor);
                a3.Visit(rVisitor);
                a4.Visit(rVisitor);
                a5.Visit(rVisitor);
                a6.Visit(rVisitor);
                a7.Visit(rVisitor);
                a8.Visit(rVisitor);
                a9.Visit(rVisitor);
                a10.Visit(rVisitor);
            }

            inline static void Visit() {
                Argument<T1>::Visit();
                Argument<T2>::Visit();
                Argument<T3>::Visit();
                Argument<T4>::Visit();
                Argument<T5>::Visit();
                Argument<T6>::Visit();
                Argument<T7>::Visit();
                Argument<T8>::Visit();
                Argument<T9>::Visit();
                Argument<T10>::Visit();
            }

            Argument<T1>  a1;
            Argument<T2>  a2;
            Argument<T3>  a3;
            Argument<T4>  a4;
            Argument<T5>  a5;
            Argument<T6>  a6;
            Argument<T7>  a7;
            Argument<T8>  a8;
            Argument<T9>  a9;
            Argument<T10>  a10;
        };

        #pragma endregion arguments

        #pragma region invoker
        template<typename M>
        struct Invoker;

        template<typename R>
        struct Invoker<R(*)()> : InvokerBase<void, R, R(*)(), R(*)()>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr();
            }
            inline R operator() () const {
                return m_method.m_ptr();
            }
        };

        template<typename R, typename T1>
        struct Invoker<R(*)(T1)> : InvokerBase<void, R, R(*)(T1), R(*)(T1)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr(a.a1);
            }
            inline R operator() (T1 a1) const {
                return m_method.m_ptr(a1);
            }
        };

        template<typename R, typename T1, typename T2>
        struct Invoker<R(*)(T1,T2)> : InvokerBase<void, R, R(*)(T1,T2), R(*)(T1, T2)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr(a.a1, a.a2);
            }
            inline R operator() (T1 a1, T2 a2) const {
                return m_method.m_ptr(a1, a2);
            }
        };

        template<typename R, typename T1, typename T2, typename T3>
        struct Invoker<R(*)(T1,T2,T3)> : InvokerBase<void, R, R(*)(T1,T2,T3), R(*)(T1, T2, T3)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr(a.a1, a.a2, a.a3);
            }

            inline R operator() (T1 a1, T2 a2, T3 a3) const {
                return m_method.m_ptr(a1, a2, a3);
            }
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4>
        struct Invoker<R(*)(T1,T2,T3,T4)> : InvokerBase<void, R, R(*)(T1,T2,T3,T4), R(*)(T1, T2, T3, T4)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr(a.a1, a.a2, a.a3, a.a4);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4) const {
                return m_method.m_ptr(a1, a2, a3, a4);
            }
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
        struct Invoker<R(*)(T1,T2,T3,T4,T5)> : InvokerBase<void, R, R(*)(T1,T2,T3,T4,T5), R(*)(T1, T2, T3, T4, T5)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr(a.a1, a.a2, a.a3, a.a4, a.a5);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5) const {
                return m_method.m_ptr(a1, a2, a3, a4, a5);
            }
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
        struct Invoker<R(*)(T1, T2, T3, T4, T5, T6)> : InvokerBase<void, R, R(*)(T1, T2, T3, T4, T5, T6), R(*)(T1, T2, T3, T4, T5, T6)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr(a.a1, a.a2, a.a3, a.a4, a.a5, a.a6);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6) const {
                return m_method.m_ptr(a1, a2, a3, a4, a5, a6);
            }
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
        struct Invoker<R(*)(T1, T2, T3, T4, T5, T6, T7)> : InvokerBase<void, R, R(*)(T1, T2, T3, T4, T5, T6, T7), R(*)(T1, T2, T3, T4, T5, T6, T7)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr(a.a1, a.a2, a.a3, a.a4, a.a5, a.a6, a.a7);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7) const {
                return m_method.m_ptr(a1, a2, a3, a4, a5, a6, a7);
            }
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
        struct Invoker<R(*)(T1, T2, T3, T4, T5, T6, T7, T8)> : InvokerBase<void, R, R(*)(T1, T2, T3, T4, T5, T6, T7, T8), R(*)(T1, T2, T3, T4, T5, T6, T7, T8)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr(a.a1, a.a2, a.a3, a.a4, a.a5, a.a6, a.a7, a.a8);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8) const {
                return m_method.m_ptr(a1, a2, a3, a4, a5, a6, a7, a8);
            }
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
        struct Invoker<R(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9)> : InvokerBase<void, R, R(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9), R(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr(a.a1, a.a2, a.a3, a.a4, a.a5, a.a6, a.a7, a.a8, a.a9);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9) const {
                return m_method.m_ptr(a1, a2, a3, a4, a5, a6, a7, a8, a9);
            }
        };

        template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
        struct Invoker<R(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> : InvokerBase<void, R, R(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), R(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return m_method.m_ptr(a.a1, a.a2, a.a3, a.a4, a.a5, a.a6, a.a7, a.a8, a.a9, a.a10);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9, T10 a10) const {
                return m_method.m_ptr(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10);
            }
        };

        template<typename C, typename R>
        struct Invoker<R(C::*)()> : InvokerBase<C, R, R(*)(), R(C::*)()>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)();
            }
            inline R operator() () const {
                return (m_this.m_ptr->*m_method.m_ptr)();
            }
        };

        template<typename C, typename R, typename T1>
        struct Invoker<R(C::*)(T1)> : InvokerBase<C, R, R(*)(T1), R(C::*)(T1)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a.a1);
            }
            inline R operator() (T1 a1) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a1);
            }
        };

        template<typename C, typename R, typename T1, typename T2>
        struct Invoker<R(C::*)(T1,T2)> : InvokerBase<C, R, R(*)(T1,T2), R(C::*)(T1, T2)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a.a1, a.a2);
            }
            inline R operator() (T1 a1, T2 a2) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a1, a2);
            }
        };

        template<typename C, typename R, typename T1, typename T2, typename T3>
        struct Invoker<R(C::*)(T1,T2,T3)> : InvokerBase<C, R, R(*)(T1,T2,T3), R(C::*)(T1, T2, T3)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a.a1, a.a2, a.a3);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a1, a2, a3);
            }
        };

        template<typename C, typename R, typename T1, typename T2, typename T3, typename T4>
        struct Invoker<R(C::*)(T1,T2,T3,T4)> : InvokerBase<C, R, R(*)(T1,T2,T3,T4), R(C::*)(T1, T2, T3, T4)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a.a1, a.a2, a.a3, a.a4);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a1, a2, a3, a4);
            }
        };

        template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
        struct Invoker<R(C::*)(T1,T2,T3,T4,T5)> : InvokerBase<C, R, R(*)(T1,T2,T3,T4,T5), R(C::*)(T1, T2, T3, T4, T5)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a.a1, a.a2, a.a3, a.a4, a.a5);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a1, a2, a3, a4, a5);
            }
        };

        template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
        struct Invoker<R(C::*)(T1, T2, T3, T4, T5, T6)> : InvokerBase<C, R, R(*)(T1, T2, T3, T4, T5, T6), R(C::*)(T1, T2, T3, T4, T5, T6)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a.a1, a.a2, a.a3, a.a4, a.a5, a.a6);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a1, a2, a3, a4, a5, a6);
            }
        };

        template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
        struct Invoker<R(C::*)(T1, T2, T3, T4, T5, T6, T7)> : InvokerBase<C, R, R(*)(T1, T2, T3, T4, T5, T6, T7), R(C::*)(T1, T2, T3, T4, T5, T6, T7)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a.a1, a.a2, a.a3, a.a4, a.a5, a.a6, a.a7);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a1, a2, a3, a4, a5, a6, a7);
            }
        };

        template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
        struct Invoker<R(C::*)(T1, T2, T3, T4, T5, T6, T7, T8)> : InvokerBase<C, R, R(*)(T1, T2, T3, T4, T5, T6, T7, T8), R(C::*)(T1, T2, T3, T4, T5, T6, T7, T8)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a.a1, a.a2, a.a3, a.a4, a.a5, a.a6, a.a7, a.a8);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a1, a2, a3, a4, a5, a6, a7, a8);
            }
        };

        template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
        struct Invoker<R(C::*)(T1, T2, T3, T4, T5, T6, T7, T8, T9)> : InvokerBase<C, R, R(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9), R(C::*)(T1, T2, T3, T4, T5, T6, T7, T8, T9)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a.a1, a.a2, a.a3, a.a4, a.a5, a.a6, a.a7, a.a8, a.a9);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a1, a2, a3, a4, a5, a6, a7, a8, a9);
            }
        };

        template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
        struct Invoker<R(C::*)(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> : InvokerBase<C, R, R(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10), R(C::*)(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)>
        {
            inline R InvokeWithArgumentsPack(const ArgumentsPack<SignatureType>& a) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a.a1, a.a2, a.a3, a.a4, a.a5, a.a6, a.a7, a.a8, a.a9, a.a10);
            }
            inline R operator() (T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9, T10 a10) const {
                return (m_this.m_ptr->*m_method.m_ptr)(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10);
            }
        };
        #pragma endregion invoker

        
        template<typename M>
        class Target
        {
        public:
            typedef typename Invoker<M>::ThisType ThisType;
            typedef typename Invoker<M>::ResultType ResultType;
            typedef typename Invoker<M>::SignatureType SignatureType;
            typedef Action<ResultType> ActionType;

            Target()
            : m_invoker()
            , m_arguments()
            { }

            Target(const Target& rhs)
            : m_invoker(rhs.m_invoker)
            , m_arguments(rhs.m_arguments)
            { }
            
            Target(const M& krMethod)
            : m_arguments() { 
                m_invoker.SetMethod(krMethod);
            }

            Target(const This<ThisType>& krcThis, const M& krMethod)
            : m_arguments() {
                m_invoker.Set(krcThis, krMethod);
            }

            inline operator bool() const {
                return m_invoker;
            }
            
            inline ResultType operator() () const {
                return m_invoker.InvokeWithArgumentsPack(m_arguments);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) {
                m_invoker.Visit(rVisitor);
                m_arguments.Visit(rVisitor);
            }

            template<typename VisitorType>
            inline void Visit(VisitorType& rVisitor) const {
                m_invoker.Visit(rVisitor);
                m_arguments.Visit(rVisitor);
            }

            inline static void Visit() {
                ArgumentsPack<SignatureType>::Visit();
            }

            inline void SetThis(const This<ThisType>& krcThis) {
                m_invoker.SetThis(krcThis);
            }

            inline void SetMethod(const M& krMethod) {
                m_invoker.SetMethod(krMethod);
            }

            inline void* GetThis() const {
                return m_invoker.GetThis();
            }
        private:
            Invoker<M> m_invoker;
            ArgumentsPack<SignatureType> m_arguments;
        };

        struct Parser : public Visitor::Slices
        {
            Parser(std::istream& rStream)
                : m_kpcClass(NULL)
                , m_kpcMethod(NULL)
                , m_kpcBuilder(NULL)
                , m_pcRepository(NULL)
                , m_rStream(rStream)
            { }

            template<typename T>
            inline void operator() (This<T>& value) {
                m_kpcClass = &Reflection::Class::GetOrCreate<T>();
            }

            template<typename T>
            inline void operator() (Method<T>& value) {
                Visitor::Slice sSlice;

                if (!TryPopSlice(sSlice))
                    return;

                const Reflection::Type& krcMethodType = Reflection::Type::GetOrCreateType<T>();

                m_kpcMethod = m_kpcClass->GetMethod(krcMethodType, sSlice.m_nWordId);
            }

            template<typename T>
            inline void operator() (Value<T>& value) {
                Visitor::Slice sSlice;

                if (!TryPopSlice(sSlice))
                    return;

                SetValue(value.m_value, sSlice);
            }

            inline void operator() (Value<Word::Handle>& value) {
                Visitor::Slice sSlice;

                if (TryPopSlice(sSlice))
                {
                    std::string str;

                    SetValue(str, sSlice);

                    value.m_value.SetString(str);
                }
            }

            inline void operator() (Value<Word::Handle::VectorType>& value) {
                Visitor::Slice sSlice;

                while (TryPopSlice(sSlice))
                {
                    std::string str;

                    SetValue(str, sSlice);

                    value.m_value.push_back(Word::Handle(str));
                }
            }
            inline void operator() (Value<Serialization::Descriptor>& value) {
                Visitor::Slice sSlice;

                while (TryPopSlice(sSlice))
                    value.m_value.AddParameter(sSlice.m_nTokenId);
            }

            template<typename T>
            inline void operator() (Value< Data::Parameter<T> >& value) {
                Data::IndexType nIndex = 0;

                value.m_value.Set(m_pcRepository->GetOrCreateStorage<T>(), nIndex);
            }

            inline void operator() (Value<Visitor::Slice>& slice) {
                TryPopSlice(slice.m_value);
            }

            inline void operator() (Value<Visitor::Slices>& slices) {
                for (Visitor::Slice slice; TryPopSlice(slice);)
                    slices.m_value.TryPushSlice(slice);
            }

            template<typename T>
            inline void operator() (Slot<T>& slot) {
                Visitor::Slice sSlice;

                if (!TryPopSlice(sSlice))
                    return;

                if (sSlice.m_nWordId < ~0)
                {
                    switch (sSlice.m_nType)
                    {
                        case Serialization::Token::Variable: {
                            UseVariable<T>(slot, sSlice);
                        } return;

                        case Serialization::Token::Property: {
                        
                        } return;

                        default:
                            break;
                    }

                }

                SetValue(slot.Create(m_pcRepository), sSlice);
            }


            inline void operator() (Slot<std::string>& slot) {
                Visitor::Slice sSlice;

                if (!TryPopSlice(sSlice))
                    return;

                if (sSlice.m_nWordId < ~0)
                {
                    switch (sSlice.m_nType)
                    {
                        case Serialization::Token::Variable: {
                            UseVariable<std::string>(slot, sSlice);
                        } return;

                        case Serialization::Token::String: {
                            slot.m_pvecContainer = &Word::Table::Get().GetVector();
                            slot.m_nIndex = sSlice.m_nWordId;
                        } return;

                        default:
                            break;
                    }

                }

                SetValue(slot.Create(m_pcRepository), sSlice);
            }

            inline void operator() (Handle<Reflection::Type>& handle) {
                Reflection::Namespace::TryParse(*this, &handle.m_nId);
            }

            inline void operator() (Handle<Reflection::Namespace>& handle) {
                Reflection::Namespace::TryParse(*this, NULL, &handle.m_nId);
            }

            inline void operator() (Value<Parser*>& value) {
                value.m_value = this;
            }


            template <typename T>
            inline void UseVariable(Slot<T>& slot, const Visitor::Slice& krsSlice) {
                slot.Use(m_pcRepository, m_pcRepository->GetOrCreateVariableObject<T>(krsSlice.m_nWordId));
            }



            template<typename T>
            inline void SetValue(T& rValue, const Visitor::Slice& krsSlice) {
                Reflection::ParseValue(m_rStream, rValue, krsSlice);
            }

            template<typename T>
            inline void SetFor(const Data::Reference& krcRef, const Visitor::Slice& krsSlice) {
                T* ptrValue = m_pcRepository->GetObjectByReference<T>(krcRef);

                if (ptrValue)
                    SetValue(*ptrValue, krsSlice);
            }
            template<typename T>
            inline const Data::Reference& GetOrCreateFor(const Visitor::Slice& krsSlice) {
                return m_pcRepository->GetOrCreateVariableObject<T>(krsSlice.m_nWordId);
            }


            inline Data::Storage* FindStorage(const Data::Reference& krcReference) {
                return m_pcRepository->FindStorage(krcReference.GetType().GetId());
            }

            const Reflection::Class* m_kpcClass;
            const Reflection::Method* m_kpcMethod;
            const Data::Builder* m_kpcBuilder;
            Data::Repository* m_pcRepository;
            std::istream& m_rStream;
        };


        class Factory
        {
            template<typename T>
            Data::Reference CreateActionFor(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetReference) const {
                typedef Target<T> TargetType;
                typedef typename TargetType::ActionType ActionType;
                
                std::vector<TargetType>* pvecObjects = rcRepository.GetObjects<TargetType>();
                const size_t knIndex = krcBindingTargetReference.GetIndex();

                ActionType cAction;

                cAction.Set(pvecObjects, knIndex);

                return rcRepository.CreateObject(cAction);
            }

            template<typename T>
            Data::Reference CreateTargetFor(Data::Repository& rcRepository, Parser& rcParser, void* ptrThis) const {
                Target<T> cBindingTarget(ptrThis, Reflection::Method::Get(m_nMethodId).GetMethod<T>());

                cBindingTarget.Visit(rcParser);

                return rcRepository.CreateObject(cBindingTarget);
            }

            template<typename T>
            void* GetThisAtIndexFor(Data::Repository& rcRepository, const Data::IndexType knThisIndex) const {
                typedef typename Target<T>::ThisType ThisType;

                void* ptrThis = NULL;

                ptrThis = rcRepository.GetInstance<ThisType>(knThisIndex);

                if (ptrThis == NULL)
                    ptrThis = rcRepository.GetPointer<ThisType>(knThisIndex);

                return ptrThis;
            }
            template<typename T>
            void* GetThisFor(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetReference) const {
                typedef typename Target<T>::ThisType ThisType;
                typedef Target<T> TargetType;

                TargetType* pcBindingTarget = rcRepository.GetObjectByReference<TargetType>(krcBindingTargetReference);

                return pcBindingTarget->GetThis();
            }

            template<typename T>
            bool SetThisFor(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetReference, void* ptrThis) const {
                typedef Target<T> TargetType;

                TargetType* pcBindingTarget = rcRepository.GetObjectByReference<TargetType>(krcBindingTargetReference);

                if (pcBindingTarget == NULL)
                    return false;

                pcBindingTarget->SetThis(ptrThis);

                return true;
            }
        public:
            typedef size_t IdType;

            typedef std::vector<Factory> VectorType;
            typedef std::vector<const Factory*> PtrVectorType;

            Factory()
            : m_nMethodId(~0)
            , m_mptrCreateAction(NULL)
            , m_mptrCreateTarget(NULL)
            , m_mptrGetThisAtIndex(NULL)
            , m_mptrGetThis(NULL)
            , m_mptrSetThis(NULL)
            { }

            Factory(const Factory& rhs)
            : m_nMethodId(rhs.m_nMethodId)
            , m_mptrCreateAction(rhs.m_mptrCreateAction)
            , m_mptrCreateTarget(rhs.m_mptrCreateTarget)
            , m_mptrGetThisAtIndex(rhs.m_mptrGetThisAtIndex)
            , m_mptrGetThis(rhs.m_mptrGetThis)
            , m_mptrSetThis(rhs.m_mptrSetThis)
            { }

            $use_template(void, T, BindInput) (const T& krMethod) {
                m_mptrCreateAction = &Factory::CreateActionFor<T>;
                m_mptrCreateTarget = &Factory::CreateTargetFor<T>;
                m_mptrGetThisAtIndex = &Factory::GetThisAtIndexFor<T>;
                m_mptrGetThis = &Factory::GetThisFor<T>;
                m_mptrSetThis = &Factory::SetThisFor<T>;

                Target<T>::Visit();
            }

            void BindOutput(const Reflection::Method& krcMethod) {
                m_nMethodId = krcMethod.GetId();
            }

            inline const Reflection::Method& GetMethod() const {
                return Reflection::Method::Get(m_nMethodId);
            }

            inline Data::Reference CreateAction(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetRefence) const {
                return (this->*m_mptrCreateAction)(rcRepository, krcBindingTargetRefence);
            }

            inline Data::Reference CreateTarget(Data::Repository& rcRepository, Parser& rcParser, void* ptrThis) const {
                return (this->*m_mptrCreateTarget)(rcRepository, rcParser, ptrThis);
            }

            inline void* GetThis(Data::Repository& rcRepository, const Data::IndexType knThisIndex) const {
                return (this->*m_mptrGetThisAtIndex)(rcRepository, knThisIndex);
            }

            inline void* GetThis(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetRefence) const {
                return (this->*m_mptrGetThis)(rcRepository, krcBindingTargetRefence);
            }

            inline bool SetThis(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetRefence, void* ptrThis) const {
                return (this->*m_mptrSetThis)(rcRepository, krcBindingTargetRefence, ptrThis);
            }

            inline bool Bind(
                Parser& rcBindingParser,
                Data::Reference& rcTargetRef,
                Data::Reference& rcActionRef,
                const Serialization::Descriptor& krcDescriptor,
                const Serialization::Token::VectorType& krvecTokens,
                const Data::IndexType knObjectInstanceIndex = 0
            ) const {
                Data::Repository& rcRepository = *rcBindingParser.m_pcRepository;
                bool bComplete = true;

                void* ptrThis = GetThis(rcRepository, knObjectInstanceIndex);

                if (ptrThis == NULL)
                    bComplete = false;

                if (rcTargetRef && rcActionRef) {
                    SetThis(rcRepository, rcTargetRef, ptrThis);
                }
                else if (krcDescriptor.TryPushSlices(rcBindingParser, krvecTokens)) {
                    rcTargetRef = CreateTarget(rcRepository, rcBindingParser, ptrThis);
                    rcActionRef = CreateAction(rcRepository, rcTargetRef);
                }
                else {
                    bComplete = false;
                }

                return bComplete;
            }
        private:
            Reflection::Method::IdType m_nMethodId;

            Data::Reference(Factory::*m_mptrCreateAction)(Data::Repository&, const Data::Reference&) const;
            Data::Reference(Factory::*m_mptrCreateTarget)(Data::Repository&, Parser&, void*) const;
            void*(Factory::*m_mptrGetThisAtIndex)(Data::Repository&, const Data::IndexType) const;
            void*(Factory::*m_mptrGetThis)(Data::Repository&, const Data::Reference&) const;
            bool(Factory::*m_mptrSetThis)(Data::Repository&, const Data::Reference&, void*) const;
        };

        class Interface
        {
            template<typename T>
            Data::Reference CreateFuncFor(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetReference) const {
                typedef Invoker<T> InvokerType;
                typedef typename InvokerType::SignatureType SignatureType;

                std::vector<InvokerType>* pvecObjects = rcRepository.GetObjects<InvokerType>();
                const size_t knIndex = krcBindingTargetReference.GetIndex();

                Func<SignatureType> cFunc;

                cFunc.Set(pvecObjects, knIndex);

                return rcRepository.CreateObject(cFunc);
            }

            template<typename T>
            Data::Reference CreateInvokerFor(Data::Repository& rcRepository, void* ptrThis) const {
                Invoker<T> cInvoker;

                cInvoker.Set(ptrThis, Reflection::Method::Get(m_nMethodId).GetMethod<T>());

                return rcRepository.CreateObject(cInvoker);
            }

            template<typename T>
            void* GetThisAtIndexFor(Data::Repository& rcRepository, const Data::IndexType knThisIndex) const {
                typedef typename Invoker<T>::ThisType ThisType;

                void* ptrThis = NULL;

                ptrThis = rcRepository.GetInstance<ThisType>(knThisIndex);

                if (ptrThis == NULL)
                    ptrThis = rcRepository.GetPointer<ThisType>(knThisIndex);

                return ptrThis;
            }
            template<typename T>
            void* GetThisFor(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetReference) const {
                typedef Invoker<T> InvokerType;

                InvokerType* pcInvoker = rcRepository.GetObjectByReference<InvokerType>(krcBindingTargetReference);

                return pcInvoker->GetThis();
            }

            template<typename T>
            bool SetThisFor(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetReference, void* ptrThis) const {
                typedef Invoker<T> InvokerType;

                InvokerType* pcInvoker = rcRepository.GetObjectByReference<InvokerType>(krcBindingTargetReference);

                if (pcInvoker == NULL)
                    return false;

                pcInvoker->SetThis(ptrThis);

                return true;
            }
        public:
            typedef size_t IdType;

            typedef std::vector<Interface> VectorType;
            typedef std::vector<const Interface*> PtrVectorType;

            Interface()
                : m_nMethodId(~0)
                , m_mptrCreateFunc(NULL)
                , m_mptrCreateInvoker(NULL)
                , m_mptrGetThisAtIndex(NULL)
                , m_mptrGetThis(NULL)
                , m_mptrSetThis(NULL)
            { }

            Interface(const Interface& rhs)
                : m_nMethodId(rhs.m_nMethodId)
                , m_mptrCreateFunc(rhs.m_mptrCreateFunc)
                , m_mptrCreateInvoker(rhs.m_mptrCreateInvoker)
                , m_mptrGetThisAtIndex(rhs.m_mptrGetThisAtIndex)
                , m_mptrGetThis(rhs.m_mptrGetThis)
                , m_mptrSetThis(rhs.m_mptrSetThis)
            { }

            $use_template(void, T, BindInput) (const T& krMethod) {
                m_mptrCreateFunc = &Interface::CreateFuncFor<T>;
                m_mptrCreateInvoker = &Interface::CreateInvokerFor<T>;
                m_mptrGetThisAtIndex = &Interface::GetThisAtIndexFor<T>;
                m_mptrGetThis = &Interface::GetThisFor<T>;
                m_mptrSetThis = &Interface::SetThisFor<T>;
            }

            void BindOutput(const Reflection::Method& krcMethod) {
                m_nMethodId = krcMethod.GetId();
            }

            inline const Reflection::Method& GetMethod() const {
                return Reflection::Method::Get(m_nMethodId);
            }

            inline Data::Reference CreateFunc(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetRefence) const {
                return (this->*m_mptrCreateFunc)(rcRepository, krcBindingTargetRefence);
            }

            inline Data::Reference CreateInvoker(Data::Repository& rcRepository, void* ptrThis) const {
                return (this->*m_mptrCreateInvoker)(rcRepository, ptrThis);
            }

            inline void* GetThis(Data::Repository& rcRepository, const Data::IndexType knThisIndex) const {
                return (this->*m_mptrGetThisAtIndex)(rcRepository, knThisIndex);
            }

            inline void* GetThis(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetRefence) const {
                return (this->*m_mptrGetThis)(rcRepository, krcBindingTargetRefence);
            }

            inline bool SetThis(Data::Repository& rcRepository, const Data::Reference& krcBindingTargetRefence, void* ptrThis) const {
                return (this->*m_mptrSetThis)(rcRepository, krcBindingTargetRefence, ptrThis);
            }
        private:
            Reflection::Method::IdType m_nMethodId;

            Data::Reference(Interface::* m_mptrCreateFunc)(Data::Repository&, const Data::Reference&) const;
            Data::Reference(Interface::* m_mptrCreateInvoker)(Data::Repository&, void*) const;
            void* (Interface::* m_mptrGetThisAtIndex)(Data::Repository&, const Data::IndexType) const;
            void* (Interface::* m_mptrGetThis)(Data::Repository&, const Data::Reference&) const;
            bool(Interface::* m_mptrSetThis)(Data::Repository&, const Data::Reference&, void*) const;
        };

        class References
        {
        public:
            typedef Data::IndexVectorType IndexVectorType;
            typedef std::map<Word::IdType, References> MapType;
            typedef std::map<Word::IdType, IndexVectorType> IndexMapType;

            References();
            References(const References& rhs);

            void Resize(const size_t knSize);
            const size_t GetSize() const;

            size_t Add(
                const Data::Reference& krcInvoker,
                const Data::Reference& krcAction,
                const Reflection::Method::IdType& knMethodId
            );

            Data::Reference& GetInvoker(const size_t knIndex);
            Data::Reference& GetAction(const size_t knIndex);
            Reflection::Method::IdType& GetMethodId(const size_t knIndex);

            const Data::Reference& GetInvoker(const size_t knIndex) const;
            const Data::Reference& GetAction(const size_t knIndex) const;
            const Reflection::Method::IdType& GetMethodId(const size_t knIndex) const;
        private:
            Data::Reference::VectorType m_vecInvokers;
            Data::Reference::VectorType m_vecActions;
            Reflection::Method::IdVectorType m_vecMethodIds;
        };

        class Targets
        {
        public:
            Targets();
            Targets(const Targets& rhs);

            static bool Bind(
                Parser& rcBindingParser,
                Data::Reference& rcTargetRef,
                Data::Reference& rcActionRef,
                Reflection::Method::IdType& rnMethodId,
                const Reflection::Class* kpcClass,
                const Word::Handle& krcMethodName,
                const Serialization::Descriptor& krcDescriptor,
                const Serialization::Token::VectorType& krvecTokens,
                const Data::IndexType knObjectInstanceIndex = 0
            );

            void Clear();

            bool Bind(
                Parser& rcBindingParser,
                const Serialization::Descriptors& krcDescriptors,
                const Serialization::Token::VectorType& krvecTokens,
                Data::IndexVectorType& rvecAbsentIds
            );

            bool Rebind(
                Parser& rcBindingParser,
                const Serialization::Descriptors& krcDescriptors,
                const Serialization::Token::VectorType& krvecTokens,
                Data::IndexVectorType& rvecAbsentIds,
                Data::IndexVectorType& rvecReadyIds
            );

            bool Bind(
                Parser& rcBindingParser,
                const Serialization::Descriptors& krcDescriptors,
                const Serialization::Token::VectorType& krvecTokens,
                const Data::IndexType knIndex
            );
            bool Bind(
                Data::Repository& rcRepository,
                const Serialization::Descriptor::IdType knId,
                const Data::IndexType knThisIndex = 0
            );
            bool Bind(
                Data::Repository& rcRepository,
                const Data::IndexType knThisIndex = 0
            );
            const Reflection::Method& GetMethod(const Serialization::Descriptor::IdType knId) const;
            const Data::Reference& GetAction(const Serialization::Descriptor::IdType knId) const;
            bool Check(Data::Repository& rcRepository, const Serialization::Descriptor::IdType knId) const;
            size_t GetActionsCount() const;
        private:
            const Factory* GetFactory(const Serialization::Descriptor::IdType knId) const;

            static const Factory* FindFactory(
                const Reflection::Namespace::IdVectorType& krvecNamespaceIds,
                const Word::IdType knMethodWordId,
                Reflection::Method::IdType& rnOutMethodId
            );
            
            static const Factory* FindFactory(
                const Reflection::Class* kpcClass,
                const Word::IdType knMethodWordId,
                Reflection::Method::IdType& rnOutMethodId
            );
        private:
            References m_cReferences;
        };

        class Interfaces
        {
            struct Binder;
        public:
            Interfaces();
            Interfaces(const Interfaces& rhs);

            void Clear();

            bool Bind(
                Data::Repository* pcRepository,
                const Reflection::Namespace::IdVectorType& krvecNamespaceIds
            );
            
            const References* FindReferences(const Word::IdType knInterfaceWordId) const;
        private:
            References::MapType m_mapReferences;
        };

        class InterfaceInvokers
        {
        public:
            InterfaceInvokers()
                : m_kpcReferences(NULL)
                , m_kpcStorage(NULL)
            { }
            InterfaceInvokers(const InterfaceInvokers& rhs)
                : m_kpcReferences(rhs.m_kpcReferences)
                , m_kpcStorage(rhs.m_kpcStorage)
            { }

            inline size_t GetCount() const {
                return m_kpcReferences->GetSize();
            }

            inline void Reset() {
                m_kpcReferences = NULL;
                m_kpcStorage = NULL;
            }

            inline Error Bind(const Data::Storage* kpcStorage, const References* kpcReferences) {
                if (kpcStorage == NULL)
                    return Error(1, "absent storage");
                if (kpcReferences == NULL)
                    return Error(2, "absent references");

                m_kpcStorage = kpcStorage;
                m_kpcReferences = kpcReferences;

                return Error();
            }

            inline bool IsValid() const {
                return m_kpcReferences
                    && m_kpcStorage
                    && m_kpcReferences->GetSize() > 0
                    && m_kpcStorage->GetSize() > 0;
            }

            template<typename S>
            class Base;
        protected:
            const References* m_kpcReferences;
            const Data::Storage* m_kpcStorage;
        };


        template<typename S>
        class InterfaceInvokers::Base : public InterfaceInvokers
        {
        public:
            typedef S SignatureType;
            typedef Func<SignatureType> FuncType;

            inline Error Bind(Data::Repository& rcRepository, const Interfaces& krcInterfaces, const Word::IdType knNameId) {
                const References* kpcReferences = krcInterfaces.FindReferences(knNameId);

                if (kpcReferences == NULL)
                    return Error("absent ref");

                m_kpcStorage = rcRepository.GetStorage<FuncType>();

                if (m_kpcStorage == NULL)
                    return Error("absent storage");

                m_kpcReferences = kpcReferences;

                return Error();
            }

            inline const FuncType* GetInvoker(size_t i) const {
                return m_kpcStorage->GetPointer<FuncType>(m_kpcReferences->GetAction(i));
            }
        };

        template<typename SignatureType>
        class InterfaceInvokersFor;

        template<>
        class InterfaceInvokersFor<void(*)()> : public InterfaceInvokers::Base<void(*)()>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke();

                return true;
            }
            inline void Invoke() const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i);
            }
        };


        template<typename T1>
        class InterfaceInvokersFor<void(*)(T1)> : public InterfaceInvokers::Base<void(*)(T1)>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i, T1 a1) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke(a1);

                return true;
            }
            inline void Invoke(T1 a1) const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i, a1);
            }
        };
        template<typename T1, typename T2>
        class InterfaceInvokersFor<void(*)(T1, T2)> : public InterfaceInvokers::Base<void(*)(T1, T2)>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i, T1 a1, T2 a2) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke(a1, a2);

                return true;
            }
            inline void Invoke(T1 a1, T2 a2) const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i, a1, a2);
            }
        };
        template<typename T1, typename T2, typename T3>
        class InterfaceInvokersFor<void(*)(T1, T2, T3)> : public InterfaceInvokers::Base<void(*)(T1, T2, T3)>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i, T1 a1, T2 a2, T3 a3) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke(a1, a2, a3);

                return true;
            }
            inline void Invoke(T1 a1, T2 a2, T3 a3) const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i, a1, a2, a3);
            }
        };
        template<typename T1, typename T2, typename T3, typename T4>
        class InterfaceInvokersFor<void(*)(T1, T2, T3, T4)> : public InterfaceInvokers::Base<void(*)(T1, T2, T3, T4)>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i, T1 a1, T2 a2, T3 a3, T4 a4) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke(a1, a2, a3, a4);

                return true;
            }
            inline void Invoke(T1 a1, T2 a2, T3 a3, T4 a4) const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i, a1, a2, a3, a4);
            }
        };
        template<typename T1, typename T2, typename T3, typename T4, typename T5>
        class InterfaceInvokersFor<void(*)(T1,T2,T3,T4,T5)> : public InterfaceInvokers::Base<void(*)(T1, T2, T3, T4, T5)>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke(a1, a2, a3, a4, a5);

                return true;
            }
            inline void Invoke(T1 a1, T2 a2, T3 a3, T4 a4, T5 a5) const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i, a1, a2, a3, a4, a5);
            }
        };
        template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
        class InterfaceInvokersFor<void(*)(T1, T2, T3, T4, T5, T6)> : public InterfaceInvokers::Base<void(*)(T1, T2, T3, T4, T5, T6)>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke(a1, a2, a3, a4, a5, a6);

                return true;
            }
            inline void Invoke(T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6) const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i, a1, a2, a3, a4, a5, a6);
            }
        };
        template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
        class InterfaceInvokersFor<void(*)(T1, T2, T3, T4, T5, T6, T7)> : public InterfaceInvokers::Base<void(*)(T1, T2, T3, T4, T5, T6, T7)>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke(a1, a2, a3, a4, a5, a6, a7);

                return true;
            }
            inline void Invoke(T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7) const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i, a1, a2, a3, a4, a5, a6, a7);
            }
        };
        template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
        class InterfaceInvokersFor<void(*)(T1, T2, T3, T4, T5, T6, T7, T8)> : public InterfaceInvokers::Base<void(*)(T1, T2, T3, T4, T5, T6, T7, T8)>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke(a1, a2, a3, a4, a5, a6, a7, a8);

                return true;
            }
            inline void Invoke(T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8) const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i, a1, a2, a3, a4, a5, a6, a7, a8);
            }
        };
        template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
        class InterfaceInvokersFor<void(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9)> : public InterfaceInvokers::Base<void(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9)>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke(a1, a2, a3, a4, a5, a6, a7, a8, a9);

                return true;
            }
            inline void Invoke(T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9) const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i, a1, a2, a3, a4, a5, a6, a7, a8, a9);
            }
        };
        template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
        class InterfaceInvokersFor<void(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> : public InterfaceInvokers::Base<void(*)(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)>
        {
        public:
            inline bool TryInvokeAtIndex(size_t i, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9, T10 a10) const {
                const FuncType* kpcFunc = GetInvoker(i);

                if (kpcFunc == NULL)
                    return false;

                kpcFunc->Invoke(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10);

                return true;
            }
            inline void Invoke(T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9, T10 a10) const {
                for (size_t i = 0; i < GetCount(); ++i)
                    TryInvokeAtIndex(i, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10);
            }
        };
    }
}