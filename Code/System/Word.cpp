#include "Word.h"

namespace System
{
    namespace Word
    {

        Table& Table::Get() {
            static Table s_cTable;

            return s_cTable;
        }

        IdType Table::GetOrCreateId(const StringType& krString) {
            StringMapType::iterator it = m_mapLookup.find(krString);

            if (it != m_mapLookup.end())
                return it->second;

            it = m_mapLookup.insert(std::make_pair(krString, m_vecLoopup.size())).first;

            m_vecLoopup.push_back(krString);

            return it->second;
        }

        bool Table::HasWithId(const IdType knWordId) const {
            return knWordId < m_vecLoopup.size();
        }

        const StringType& Table::GetById(const IdType knWordId) const {
            return m_vecLoopup[knWordId];
        }


        Handle::Handle() : m_nWordId(~0) { }
        Handle::Handle(const Handle& rhs) : m_nWordId(rhs.m_nWordId) { }
        Handle::Handle(const IdType knWordId) : m_nWordId(knWordId) { }
        Handle::Handle(const StringType& krstrWord) : m_nWordId(Table::Get().GetOrCreateId(krstrWord)) { }
        Handle::Handle(PointerType kpstrWord) : m_nWordId(~0) {
            if (kpstrWord && kpstrWord[0])
                m_nWordId = Table::Get().GetOrCreateId(kpstrWord);
        }


        void Handle::SetId(const IdType knWordId) {
            m_nWordId = knWordId;
        }

        void Handle::SetString(const StringType& krstrWord) {
            m_nWordId = Table::Get().GetOrCreateId(krstrWord);
        }

        Handle::operator bool() const {
            return m_nWordId < ~0;
        }
        bool Handle::operator == (const Handle& rhs) const {
            return m_nWordId == rhs.m_nWordId;
        }
        bool Handle::operator != (const Handle& rhs) const {
            return m_nWordId != rhs.m_nWordId;
        }
        bool Handle::operator >= (const Handle& rhs) const {
            return m_nWordId >= rhs.m_nWordId;
        }
        bool Handle::operator <= (const Handle& rhs) const {
            return m_nWordId <= rhs.m_nWordId;
        }
        bool Handle::operator > (const Handle& rhs) const {
            return m_nWordId > rhs.m_nWordId;
        }
        bool Handle::operator < (const Handle& rhs) const {
            return m_nWordId < rhs.m_nWordId;
        }

        const IdType& Handle::GetId() const {
            return m_nWordId;
        }

        const StringType& Handle::GetString() const {
            return Table::Get().GetById(m_nWordId);
        }

        PointerType Handle::c_str() const {
            return m_nWordId < ~0 ? GetString().c_str() : NULL;
        }
    }
}