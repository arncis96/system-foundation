#pragma once

namespace System
{
    namespace Operator
    {
        enum EType
        {
            LeftShift,
            RightShift,
            
            //Assignment operators
            SimpleAssignment,
            AdditionAssignment,
            SubtractionAssignment,
            MultiplicationAssignment,
            DivisionAssignment,
            RemainderAssignment,
            BitwiseAndAssignment,
            BitwiseOrAssignment,
            BitwiseXorAssignment,
            BitwiseLeftShiftAssignment,
            BitwiseRightShiftAssignment,

            //Increment/decrement operators
            PreIncrement,
            PreDecrement,
            PostIncrement,
            PostDecrement,

            //Arithmetic operators
            UnaryPlus,
            UnaryMinus,
            Addition,
            Subtraction,
            Multiplication,
            Division,
            Remainder,
            BitwiseNot,
            BitwiseAnd,
            BitwiseOr,
            BitwiseXor,
            BitwiseLeftShift,
            BitwiseRightShift,

            //Logical operators
            Negation,
            And,
            InclusiveOr,

            //Comparison operators
            EqualTo,
            NotEqualTo,
            LessThan,
            GreaterThan,
            LessThanOrEqualTo,
            GreaterThanOrEqualTo,

            Unknown
        };

    }

	namespace SFINE
	{
        struct Yes { char _; };
        struct No { int _; };
		
		template<bool B, class T = void> 
		struct EnableIf {}; 

		template<class T> 
		struct EnableIf<true, T> { typedef T type; };

    	template <typename T, T t> struct ReallyHas;
        template <typename T, T t, bool b> struct ReallyHas2;
		template <typename T, T t> struct ReallyGet {
			static T value;
		};

		template <typename T, T t> T ReallyGet<T, t>::value = t;

		template <typename T, T t, typename D> struct ReallyGet2 {
			static T value;
		};

		template <typename T, T t, typename D> T ReallyGet2<T, t, D>::value = t;


        template <bool val>
        struct Bool{ static const bool value = val; };

        struct True: Bool<true> {};
        struct False: Bool<false> {};

        template<size_t>
        struct Choice {};

        struct Fallback {
            template<typename T>
            Fallback(const T&);
        };


        template<typename T>
        No& Test(...);

        template<typename L, typename R>
        No& Test(...);

        template<typename T>
        Yes& Test(Choice<0>, T&);

        template<typename T>
        Yes& Test(Choice<1>, T);

        //No& Test(Choice<0>, No);


        template<typename LHS, typename RHS, Operator::EType op = Operator::Unknown>
        struct ExternalOperator {
            static LHS& lhs;
            static RHS& rhs;
        };
        template<typename LHS, typename RHS, Operator::EType op = Operator::Unknown>
        struct InternalOperator {
            static const bool value = false;
        };

        template <template<typename> class op, bool selector = true> struct UnaryHelper {
            template<typename LHS> struct type : False {};
        };
        template <template<typename> class op> struct UnaryHelper<op, false> {
            template<typename LHS> struct type : Bool<op<LHS>::value> {};
        };

        template <template<typename, typename> class op, bool selector = true> struct BinaryHelper {
            template<typename LHS, typename RHS> struct type : False {};
        };
        template <template<typename, typename> class op> struct BinaryHelper<op, false> {
            template<typename LHS, typename RHS> struct type : Bool<op<LHS, RHS>::value> {};
        };

        template <bool selector = true> struct InternalOperatorHelper {
            template<typename LHS, typename RHS, Operator::EType> struct type : False {};
        };
        template <> struct InternalOperatorHelper<false> {
            template<typename LHS, typename RHS, Operator::EType op> struct type : Bool<InternalOperator<LHS, RHS, op>::value> {};
        };

        template <bool selector = true> struct ExternalOperatorHelper {
            template<typename LHS, typename RHS, Operator::EType> struct type : False {};
        };
        template <> struct ExternalOperatorHelper<false> {
            template<typename LHS, typename RHS, Operator::EType op> struct type : Bool<ExternalOperator<LHS, RHS, op>::value> {};
        };
	}
}


#define $has(_name, _signature, _value) static System::SFINE::Yes& _name(System::SFINE::ReallyHas< _signature , _value >* /*unused*/) { }  
#define $yes(_name) sizeof(_name(0)) == sizeof(System::SFINE::Yes)
#define $no(_name) static System::SFINE::No& _name(...) { }

#define $enable_if typename System::SFINE::EnableIf
#define $enable_ift(_value, _resultType) typename System::SFINE::EnableIf< _value , _resultType >::type
#define $enable_ifv(_type, _resultType) typename System::SFINE::EnableIf< _type##::value , _resultType >::type
#define $use_template(_resultType, _T1, _name) enum { __template_##_name = 1}; template<typename _T1> _resultType _name
#define $has_template(_name, _class, _value) static System::SFINE::Yes& _name(System::SFINE::ReallyHas< int , _class::__template_##_value >* /*unused*/) { }

#define $get_member_value(_eval, _signature, _value) static _signature _eval(System::SFINE::ReallyGet< _signature , _value > * self) { return self->value; }  
#define $get_member_null(_eval, _signature) static _signature _eval(...) { return NULL; }
#define $get_member(_name, _eval, _signature) static _signature _name() { return _eval(0); }
#define $get_member_pointer(_sig, _name, _type, _expr_type, _expr_value)\
template<typename _expr_type>\
$get_member_value(__##_name, _sig, &_expr_type::_expr_value )\
template<typename>\
$get_member_null(__##_name, _sig)\
$get_member(_name, __##_name< _type >, _sig)

#define $has_member(_sig, _name, _type, _expr_type, _expr_value)\
template<typename _expr_type>\
$has(__##_name, _sig, &_expr_type::_expr_value) \
template<typename>\
$no(__##_name)\
static const bool _name = $yes(__##_name< _type >);
