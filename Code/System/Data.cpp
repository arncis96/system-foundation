#include "Data.h"
#include "Runner.h"

namespace System
{
	namespace Data
	{

        Link::Link()
            : m_nTargetIndex(~0)
            , m_nNodeId(~0)
        { }

        Link::Link(const Link& rhs)
            : m_nTargetIndex(rhs.m_nTargetIndex)
            , m_nNodeId(rhs.m_nNodeId)
        { }

        Link::Link(const IndexType knTargetIndex,
            const Node::IdType knNodeId)
            : m_nTargetIndex(knTargetIndex)
            , m_nNodeId(knNodeId)
        { }

        Link::operator bool() const {
            return m_nTargetIndex < ~0
                && m_nNodeId < ~0;
        }
        bool Link::operator == (const Link& rhs) const {
            return m_nTargetIndex == rhs.m_nTargetIndex
                && m_nNodeId == rhs.m_nNodeId;
        }
        bool Link::operator != (const Link& rhs) const {
            return m_nTargetIndex != rhs.m_nTargetIndex
                || m_nNodeId != rhs.m_nNodeId;
        }


        Linker::Linker()
            : m_mapSymbolLinks()
        { }

        Linker::Linker(const Linker& rhs)
            : m_mapSymbolLinks(rhs.m_mapSymbolLinks)
        { }


        void Linker::Clear() {
            m_mapSymbolLinks.clear();
        }


        const Link* Linker::FindLink(const Word::IdType knWordId, const Word::IdType knSymbolWordId) const {
            for (MapType::const_iterator links = FindLinks(knSymbolWordId); links != m_mapSymbolLinks.end(); ++links) {
                Link::MapType::const_iterator link = links->second.find(knWordId);

                if (link != links->second.end())
                    return &link->second;
                else if (knSymbolWordId < ~0)
                    return NULL;
            }

            return NULL;
        }
    
        Linker::MapType::iterator Linker::FindLinks(const Word::IdType knSymbolWordId) {
            if (knSymbolWordId < ~0)
                return m_mapSymbolLinks.find(knSymbolWordId);
            else
                return m_mapSymbolLinks.begin();
        }
        Linker::MapType::const_iterator Linker::FindLinks(const Word::IdType knSymbolWordId) const {
            if (knSymbolWordId < ~0)
                return m_mapSymbolLinks.find(knSymbolWordId);
            else
                return m_mapSymbolLinks.begin();
        }

        Symbol::Symbol()
            : m_mapDefinitions()
            , m_mapUnresolvedDeclarations()
            , m_mapInternalDeclarations()
            , m_mapExternalDeclarations()
            , m_nWordId(~0)
            , m_bIsShared(false)
        { }
        Symbol::Symbol(const Symbol& rhs)
            : m_mapDefinitions(rhs.m_mapDefinitions)
            , m_mapUnresolvedDeclarations(rhs.m_mapUnresolvedDeclarations)
            , m_mapInternalDeclarations(rhs.m_mapInternalDeclarations)
            , m_mapExternalDeclarations(rhs.m_mapExternalDeclarations)
            , m_nWordId(rhs.m_nWordId)
            , m_bIsShared(rhs.m_bIsShared)
        { }

        Symbol& Symbol::operator = (const Symbol& rhs) {
            m_mapDefinitions = rhs.m_mapDefinitions;
            m_mapUnresolvedDeclarations = rhs.m_mapUnresolvedDeclarations;
            m_mapInternalDeclarations = rhs.m_mapInternalDeclarations;
            m_mapExternalDeclarations = rhs.m_mapExternalDeclarations;
            m_nWordId = rhs.m_nWordId;
            m_bIsShared = rhs.m_bIsShared;

            return *this;
        }

        void Symbol::Clear() {
            m_mapDefinitions.clear();
            m_mapUnresolvedDeclarations.clear();
            m_mapExternalDeclarations.clear();
            m_mapInternalDeclarations.clear();
            m_nWordId = ~0;
            m_bIsShared = false;
        }

        const Word::IdType& Symbol::GetWordId() const {
            return m_nWordId;
        }

        bool Symbol::IsShared() const {
            return m_bIsShared;
        }


        Node::IdMapType& Symbol::GetDefinitions() {
            return m_mapDefinitions;
        }

        Node::IdMapType& Symbol::GetInternalDeclarations() {
            return m_mapInternalDeclarations;
        }

        Node::IdMapType& Symbol::GetExternalDeclarations() {
            return m_mapExternalDeclarations;
        }

        const Node::IdMapType& Symbol::GetDefinitions() const {
            return m_mapDefinitions;
        }

        const Node::IdMapType& Symbol::GetInternalDeclarations() const {
            return m_mapInternalDeclarations;
        }

        const Node::IdMapType& Symbol::GetExternalDeclarations() const {
            return m_mapExternalDeclarations;
        }

        void Symbol::SetShared(const bool kbShared) {
            m_bIsShared = kbShared;
        }

        void Symbol::SetWordId(const Word::IdType knWordId) {
            m_nWordId = knWordId;
        }

        void Symbol::SetDefinition(
            const Node::IdType knNodeId,
            const Word::IdType knWordId
        ) {
            m_mapDefinitions[knWordId] = knNodeId;
        }

        void Symbol::SetDeclaration(
            const Node::IdType knNodeId,
            const Word::IdType knWordId
        ) {
            m_mapUnresolvedDeclarations[knWordId] = knNodeId;
        }

        bool Symbol::HasUnresolvedDeclarations() const {
            return m_mapUnresolvedDeclarations.size() > 0;
        }

        bool Symbol::ResolveInternalDeclarations(Node::VectorType& rvecNodes) {
            if (m_mapUnresolvedDeclarations.empty())
                return false;

            m_mapInternalDeclarations.clear();
            m_mapExternalDeclarations.clear();

            for (Node::IdMapType::iterator unresolved = m_mapUnresolvedDeclarations.begin(); unresolved != m_mapUnresolvedDeclarations.end(); ++unresolved) {
                const Word::IdType knWordId = unresolved->first;
                const Node::IdType knDeclarationNodeId = unresolved->second;
                Node& rcDeclarationNode = rvecNodes[knDeclarationNodeId];

                const Node* kpcInternalRoot = FindDefinitionNode(rvecNodes, knDeclarationNodeId);

                if (kpcInternalRoot) {
                    m_mapInternalDeclarations.insert(std::make_pair(knWordId, knDeclarationNodeId));

                    rcDeclarationNode.SetInternalTarget(m_nWordId, knWordId, kpcInternalRoot->GetNodeId());
                }
                else {
                    m_mapExternalDeclarations.insert(std::make_pair(knWordId, knDeclarationNodeId));

                    rcDeclarationNode.SetExternalMark(m_nWordId, knWordId);
                }
            }

            m_mapUnresolvedDeclarations.clear();

            return true;
        }


        Node* Symbol::FindDefinitionNode(Node::VectorType& krvecNodes, const Word::IdType knWordId) {
            return FindNode(krvecNodes, m_mapDefinitions, knWordId);
        }
        Node* Symbol::FindInternalDeclarationNode(Node::VectorType& krvecNodes, const Word::IdType knWordId) {
            return FindNode(krvecNodes, m_mapInternalDeclarations, knWordId);
        }
        Node* Symbol::FindExternalDeclarationNode(Node::VectorType& krvecNodes, const Word::IdType knWordId) {
            return FindNode(krvecNodes, m_mapExternalDeclarations, knWordId);
        }

        const Node* Symbol::FindDefinitionNode(const Node::VectorType& krvecNodes, const Word::IdType knWordId) const {
            return FindNode(krvecNodes, m_mapDefinitions, knWordId);
        }
        const Node* Symbol::FindInternalDeclarationNode(const Node::VectorType& krvecNodes, const Word::IdType knWordId) const {
            return FindNode(krvecNodes, m_mapInternalDeclarations, knWordId);
        }
        const Node* Symbol::FindExternalDeclarationNode(const Node::VectorType& krvecNodes, const Word::IdType knWordId) const {
            return FindNode(krvecNodes, m_mapExternalDeclarations, knWordId);
        }


        Node* Symbol::FindNode(
            Node::VectorType& krvecNodes,
            const Node::IdMapType& krmap,
            const Word::IdType knWordId
        ) {
            Node::IdMapType::const_iterator it = krmap.find(knWordId);

            if (it == krmap.end())
                return NULL;

            if (it->second >= krvecNodes.size())
                return NULL;

            return &krvecNodes[it->second];
        }

        const Node* Symbol::FindNode(
            const Node::VectorType& krvecNodes,
            const Node::IdMapType& krmap,
            const Word::IdType knWordId
        ) const {
            Node::IdMapType::const_iterator it = krmap.find(knWordId);

            if (it == krmap.end())
                return NULL;

            if (it->second >= krvecNodes.size())
                return NULL;

            return &krvecNodes[it->second];
        }


        Tree::Tree()
            : m_mapSymbols()
            , m_vecNodes()
        { }

        Tree::Tree(const Tree& rhs)
            : m_mapSymbols(rhs.m_mapSymbols)
            , m_vecNodes(rhs.m_vecNodes)
        { }

        Tree& Tree::operator = (const Tree& rhs) {
            m_mapSymbols = rhs.m_mapSymbols;
            m_vecNodes = rhs.m_vecNodes;

            return *this;
        }

        void Tree::Clear() {
            m_mapSymbols.clear();
            m_vecNodes.clear();
        }

        Node::IdType Tree::AddNode() {
            Node::IdType nId = m_vecNodes.size();

            m_vecNodes.push_back(Node(nId));

            return nId;
        }


        bool Tree::HasNode(const Node::IdType knNodeId) const {
            return knNodeId < m_vecNodes.size();
        }

        Symbol::MapType& Tree::GetSymbols() {
            return m_mapSymbols;
        }

        Symbol& Tree::GetSymbol(const Word::IdType knSymbolWordId) {
            return m_mapSymbols[knSymbolWordId];
        }

        Node::VectorType& Tree::GetNodes() {
            return m_vecNodes;
        }

        Node& Tree::GetNode(const Node::IdType knNodeId) {
            return m_vecNodes[knNodeId];
        }


        Symbol* Tree::FindSymbol(const Word::IdType knSymbolWordId) {
            Symbol::MapType::iterator it = m_mapSymbols.find(knSymbolWordId);

            if (it == m_mapSymbols.end())
                return NULL;

            return &it->second;
        }

        Node* Tree::FindDefinitionNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId) {
            
            for (Symbol::MapType::iterator it = FirstSymbol(knSymbolWordId); it != m_mapSymbols.end(); ++it) {
                Node* pcNode = it->second.FindDefinitionNode(m_vecNodes, knWordId);

                if (pcNode)
                    return pcNode;
                else if (knSymbolWordId < ~0)
                    return NULL;
            }

            return NULL;
        }

        Node* Tree::FindInternalDeclarationNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId) {
            Node* pNode = NULL;

            for (Symbol::MapType::iterator it = FirstSymbol(knSymbolWordId); pNode == NULL && it != m_mapSymbols.end(); ++it) {
                Node* pcNode = it->second.FindInternalDeclarationNode(m_vecNodes, knWordId);

                if (pcNode)
                    return pcNode;
                else if (knSymbolWordId < ~0)
                    return NULL;
            }

            return pNode;
        }

        Node* Tree::FindExternalDeclarationNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId) {
            
            for (Symbol::MapType::iterator it = FirstSymbol(knSymbolWordId); it != m_mapSymbols.end(); ++it) {
                Node* pcNode = it->second.FindExternalDeclarationNode(m_vecNodes, knWordId);

                if (pcNode)
                    return pcNode;
                else if (knSymbolWordId < ~0)
                    return NULL;
            }

            return NULL;
        }

        const Symbol::MapType& Tree::GetSymbols() const {
            return m_mapSymbols;
        }

        const Node::VectorType& Tree::GetNodes() const {
            return m_vecNodes;
        }

        const Node& Tree::GetNode(const Node::IdType knNodeId) const {
            return m_vecNodes[knNodeId];
        }


        const Symbol* Tree::FindSymbol(const Word::IdType knTypeWordId) const {
            Symbol::MapType::const_iterator it = m_mapSymbols.find(knTypeWordId);

            if (it == m_mapSymbols.end())
                return NULL;

            return &it->second;
        }

        const Node* Tree::FindDefinitionNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId) const {
            
            for (Symbol::MapType::const_iterator it = FirstSymbol(knSymbolWordId); it != m_mapSymbols.end(); ++it) {
                const Node* kpcNode = it->second.FindDefinitionNode(m_vecNodes, knWordId);

                if (kpcNode)
                    return kpcNode;
                else if (knSymbolWordId < ~0)
                    return NULL;
            }

            return NULL;
        }

        const Node* Tree::FindInternalDeclarationNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId) const {
            
            for (Symbol::MapType::const_iterator it = FirstSymbol(knSymbolWordId); it != m_mapSymbols.end(); ++it) {
                const Node* kpcNode = it->second.FindInternalDeclarationNode(m_vecNodes, knWordId);

                if (kpcNode)
                    return kpcNode;
                else if (knSymbolWordId < ~0)
                    return NULL;
            }

            return NULL;
        }

        const Node* Tree::FindExternalDeclarationNode(const Word::IdType knWordId, const Word::IdType knSymbolWordId) const {
            
            for (Symbol::MapType::const_iterator it = FirstSymbol(knSymbolWordId); it != m_mapSymbols.end(); ++it) {
                const Node* kpcNode = it->second.FindExternalDeclarationNode(m_vecNodes, knWordId);

                if (kpcNode)
                    return kpcNode;
                else if (knSymbolWordId < ~0)
                    return NULL;
            }

            return NULL;
        }


        bool Tree::HasUnresolvedDeclarations() const {
            for (Symbol::MapType::const_iterator it = m_mapSymbols.begin(); it != m_mapSymbols.end(); ++it) {
                if (it->second.HasUnresolvedDeclarations())
                    return true;
            }

            return false;
        }

        bool Tree::HasExternalDeclarations() const {
            for (Symbol::MapType::const_iterator it = m_mapSymbols.begin(); it != m_mapSymbols.end(); ++it) {
                if (it->second.GetExternalDeclarations().size() > 0)
                    return true;
            }

            return false;
        }

        bool Tree::ResolveInternalDeclarations() {
            if (!HasUnresolvedDeclarations())
                return false;

            for (Symbol::MapType::iterator it = m_mapSymbols.begin(); it != m_mapSymbols.end(); ++it) {
                if (!it->second.ResolveInternalDeclarations(m_vecNodes))
                    return false;
            }

            return true;
        }

        Symbol::MapType::iterator Tree::FirstSymbol(const Word::IdType knSymbolWordId) {
            if (knSymbolWordId < ~0)
                return m_mapSymbols.find(knSymbolWordId);
            else
                return m_mapSymbols.begin();
        }
        Symbol::MapType::const_iterator Tree::FirstSymbol(const Word::IdType knSymbolWordId) const {
            if (knSymbolWordId < ~0)
                return m_mapSymbols.find(knSymbolWordId);
            else
                return m_mapSymbols.begin();
        }


        Builder::Builder()
            : m_vecNamespaceIds()
        {
            Reflection::Namespace::IdType id;

            if (Reflection::Namespace::TryParse($type(Builder), &id))
                m_vecNamespaceIds.push_back(id);
        }

        Builder::Builder(const Builder& rhs)
            : m_vecCreatedObjectRefs(rhs.m_vecCreatedObjectRefs)
            , m_vecNamespaceIds(rhs.m_vecNamespaceIds)
        { }


        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Root).Params(0).Flag(Serialization::Keyword::AllowSelfAsKey)][$name("build")], bool, Builder, Build)() {
            Runner::Processor& rcProcessor = Runner::Processor::Get();
            Runner::Program& rcProgram = rcProcessor.GetProgram();

            for (size_t i = 0, c = rcProcessor.GetNode().GetChildrenCount(); i < c; ++i)
                if (rcProcessor.InvokeChild(i) != Runner::Succeeded)
                    return false;

            m_cError = rcProgram.GetBindingError();

            if (!m_cError)
                m_cError = rcProcessor.BindInterfaces(this);

            if (m_cError)
                return false;

            return Dispatch(rcProcessor, rcProcessor.GetNodeName(), Serialization::Descriptor());
        }


        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural).Childs(0)][$name("using")], bool, Builder, Using)(const Word::Handle::VectorType& krvecNamespaceWords) {

            const Reflection::Namespace::IdType knNamespaceId = Reflection::Namespace::FindId(krvecNamespaceWords);

            if (knNamespaceId < ~0) {
                m_vecNamespaceIds.push_back(knNamespaceId);

                return true;
            }

            return false;
        }

        $method(
            [Runner::Task()]
            [$name("new")]
            [
                Serialization::Keyword()
                .Type(Serialization::Keyword::Structural)
                .MinParams(1)
                .Flag(Serialization::Keyword::AllowWordAsParameter)
            ], bool, Builder, New)(
                const Word::Handle& krcTypeWord, 
                const Serialization::Descriptor& krcDescriptor
         ) {
            for (Reflection::Namespace::IdVectorType::const_iterator it = m_vecNamespaceIds.begin(); it != m_vecNamespaceIds.end(); ++it) {
                const Reflection::Namespace& krcNamespace = Reflection::Namespace::Get(*it);
                const Factory* kpcFactory = krcNamespace.FindAttribute<Factory>(krcTypeWord.GetId());

                if (kpcFactory) {
                    Runner::Processor& rcProcessor = Runner::Processor::Get();

                    return CreateObject(rcProcessor, kpcFactory, rcProcessor.GetNodeName(), krcDescriptor);
                }
            }

            return false;
        }

        $method([Runner::Task()][$name("var")][Serialization::Keyword().MinParams(3).Flag(Serialization::Keyword::AllowWordAsParameter)], bool, Builder, Var)(const Reflection::Type& krcType, const Word::Handle& krcName, const Visitor::Slice& krsValue)
        {
            Runner::Processor& rcProcessor = Runner::Processor::Get();
            Runner::Program& rcProgram = rcProcessor.GetProgram();
            Runner::Script* pcScript = rcProgram.GetScript();
            Repository& rcRepository = *rcProcessor.GetRepository();

            Storage* pcStorage = rcRepository.FindStorage(krcType.GetId());
            
            if (pcStorage == NULL)
                return false;

            pcScript->Bind(rcRepository, this);
            
            const Reference* kpcReference = rcRepository.FindVariableReference(krcName.GetId());

            if (kpcReference) 
                return kpcReference->MatchType(krcType.GetId()) 
                    && pcScript->ParseOverride(pcStorage, kpcReference->GetIndex(), krsValue);

            IndexType nIndex(~0);

            if (!pcScript->ParseNew(pcStorage, nIndex, krsValue))
                return false;

            rcRepository.SetVariableReference(krcName.GetId(), Reference(krcType, nIndex));

            return true;
        }

        $method([Runner::Task()][$name("set")][Serialization::Keyword().MinParams(2).Flag(Serialization::Keyword::AllowWordAsParameter)], bool, Builder, Set)(const Word::Handle& krcName, const Visitor::Slice& krsValue)
        {
            Runner::Processor& rcProcessor = Runner::Processor::Get();
            Runner::Program& rcProgram = rcProcessor.GetProgram();
            Runner::Script* pcScript = rcProgram.GetScript();
            Repository& rcRepository = *rcProcessor.GetRepository();
            
            const Reference* kpcReference = rcRepository.FindVariableReference(krcName.GetId());

            if (kpcReference == NULL)
                return false;

            Storage* pcStorage = rcRepository.FindStorage(kpcReference->GetType().GetId());

            if (pcStorage == NULL)
                return false;

            pcScript->Bind(rcRepository, this);

            return pcScript->ParseOverride(pcStorage, kpcReference->GetIndex(), krsValue);
        }

        bool Builder::CreateObject(
            Runner::Processor& rcProcessor, 
            const Factory* kpcFactory,
            const Word::Handle& krcMethodName,
            const Serialization::Descriptor& krcDescriptor
        ) {
            const Reflection::Class* kpcClass = kpcFactory->GetClass();
            Runner::Program& rcProgram = rcProcessor.GetProgram();
            Runner::Script* pcScript = rcProgram.GetScript();
            Repository& rcRepository = *rcProcessor.GetRepository();

            pcScript->Bind(rcRepository, this);

            m_vecCreatedObjectRefs.push_back(kpcFactory->CreateObject(rcRepository));

            const Data::IndexType knObjectIndex = m_vecCreatedObjectRefs.back().GetIndex();

            rcProgram.RebindScript(rcRepository, this);

            pcScript->Bind_Run_Delete(kpcClass, krcMethodName, krcDescriptor, knObjectIndex);

            for (size_t i = 0, c = rcProcessor.GetNode().GetChildrenCount(); i < c; ++i)
                if (rcProcessor.InvokeChild(i) != Runner::Succeeded)
                    return false;

            return true;
        }

        bool Builder::Dispatch(
            Runner::Processor& rcProcessor, 
            const Word::Handle& krcMethodName, 
            const Serialization::Descriptor& krcDescriptor
        ) {
            Runner::Program& rcProgram = rcProcessor.GetProgram();
            Data::Repository* pcRepository = rcProcessor.GetRepository();
            Runner::Script* pcScript = rcProgram.GetScript();

            pcScript->Bind(pcRepository, this);

            for (Data::Reference::VectorType::iterator it = m_vecCreatedObjectRefs.begin(); it != m_vecCreatedObjectRefs.end(); ++it) {
                pcScript->Bind_Run_Delete(*it, krcMethodName, krcDescriptor);
            }

            return true;
        }
	}
}