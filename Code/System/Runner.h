#pragma once

#include "Serialization.h"
#include "Binding.h"

#include <sstream>

namespace System
{
    namespace Runner
    {
        enum EStatus
        {
            Ready,
            Running,
            Succeeded,
            Failed,
            Absent
        };

        typedef std::vector<EStatus> StatusVectorType;
        typedef Action<EStatus> StatusActionType;
        typedef Action<bool> BinaryActionType;
        typedef Action<void> DefaultActionType;
        typedef Action<Error> ErrorActionType;

        typedef Binding::Factory Task;

        typedef Binding::Interface Interface;


        class Program;
        class Script
        {
            friend class Program;
        public:
            typedef size_t IdType; 
            typedef std::vector<IdType> IdVectorType;
            typedef std::vector<Script*> PointerVectorType;

            Script()
            : m_Stream(std::ios::in | std::ios::out | std::ios::binary)
            , m_cBindingParser(m_Stream)
            { }

            inline void Write(const char* data, size_t length) {
                m_Stream.write(data, length);
            }
            
            template<typename T>
            friend inline Script& operator >> (Script& lhs, T& rhs) {
                lhs.m_Stream >> rhs;

                return lhs;
            }

            template<typename T>
            friend inline Script& operator << (Script& lhs, T& rhs) {
                lhs.m_Stream << rhs;

                return lhs;
            }

            inline Error Parse() {
                Error cErr;
                
                Serialization::Parser cParser;

                m_Stream.flush();
                m_Stream.clear();
                m_Stream.seekg(0);
                m_Stream.seekp(0);

                m_Stream >> m_cTokenizer;
                
                return cParser.TryParse(m_cTokenizer, m_cTree, m_cDescriptors, m_Stream);
            }

            inline Error BindTargets(
                Binding::Targets& rcTargets,
                Data::IndexVectorType& rvecAbsentIds
            ) {
                rcTargets.Clear();
                rcTargets.Bind(m_cBindingParser, m_cDescriptors, m_cTokenizer.GetTokens(), rvecAbsentIds);

                return Error();
            }
            inline bool RebindTargets(
                Binding::Targets& rcRuntime,
                Data::IndexVectorType& rvecAbsentIds,
                Data::IndexVectorType& rvecReadyIds
            ) {
                return rcRuntime.Rebind(m_cBindingParser, m_cDescriptors, m_cTokenizer.GetTokens(), rvecAbsentIds, rvecReadyIds);
            }

            inline bool TryGetSlices(const Serialization::Descriptor& krcDescriptor, Visitor::Slices& rsSlices) {
                return krcDescriptor.TryPushSlices(rsSlices, m_cTokenizer.GetTokens());
            }

            inline bool ParseNew(Data::Storage* pcStorage, Data::IndexType& rnIndex, const Visitor::Slice& sSlice) {
                return pcStorage->ParseNew(m_Stream, rnIndex, sSlice);
            }

            inline bool ParseOverride(Data::Storage* pcStorage, const Data::IndexType& krnIndex, const Visitor::Slice& sSlice) {
                return pcStorage->ParseOverride(m_Stream, krnIndex, sSlice);
            }

            inline const Data::Tree& GetTree() const {
                return m_cTree;
            }
            
            static inline const Data::Node* FindFirstDefinitionNode(const PointerVectorType& krvecScripts, const Word::IdType knWordId, IdType& rnOutScriptId) {
                for (IdType i = 0; i < krvecScripts.size(); ++i) {
                    const Data::Node* knNode = krvecScripts[i]->GetTree().FindDefinitionNode(knWordId);

                    if (knNode) {
                        rnOutScriptId = i;

                        return knNode;
                    }
                }

                return NULL;
            }

            template<typename KeyType>
            static std::map<KeyType, Script*>& GetMap() {
                static std::map<KeyType, Script*> s_map;

                return s_map;
            }

            template<typename KeyType>
            static Script* GetOrCreate(const KeyType& krKey, bool* pbWasCreated = NULL) {
                typedef std::map<KeyType, Script*> MapType;
                
                MapType& rmap = GetMap<KeyType>();
                MapType::iterator it = rmap.find(krKey);
                const bool kbNeedsCreation = it == rmap.end();

                if (pbWasCreated)
                    *pbWasCreated = kbNeedsCreation;

                if (kbNeedsCreation)
                    it = rmap.insert(std::make_pair(krKey, new Script())).first;

                return it->second;
            }

            template<typename KeyType>
            static bool Remove(const KeyType& krKey, Script** pcOutRemoved = NULL) {
                typedef std::map<KeyType, Script*> MapType;

                MapType& rmap = GetMap<KeyType>();
                MapType::iterator it = rmap.find(krKey);
                const bool kbNeedsRemoving = it != rmap.end();

                if (*pcOutRemoved)
                    if (kbNeedsRemoving)
                        *pcOutRemoved = it->second;
                    else
                        *pcOutRemoved = NULL;
                else
                    delete it->second;

                rmap.erase(it);

                return kbNeedsRemoving;
            }

            inline void Clear() {
                m_cDescriptors.Clear();
                m_cTokenizer.Clear();
                m_cTree.Clear();

                m_Stream.clear();
                m_Stream.str("");
            }

            inline const Serialization::Token& GetNodeToken(const Data::Node::IdType knNodeId) {
                const Serialization::Descriptor& krcDescriptor = m_cDescriptors.GetDescriptor(knNodeId);
                
                return krcDescriptor.GetToken(m_cTokenizer.GetTokens());
            }

            inline void Bind(Data::Repository* pcRepository, const Data::Builder* kpcBuilder = NULL) {
                if (kpcBuilder == NULL)
                    m_cBindingParser.m_kpcBuilder = pcRepository->GetFirstObject<Data::Builder>();
                else
                    m_cBindingParser.m_kpcBuilder = kpcBuilder;

                m_cBindingParser.m_pcRepository = pcRepository;
            }

            inline void Bind(Data::Repository& rcRepository, const Data::Builder* kpcBuilder = NULL) {
                return Bind(&rcRepository, kpcBuilder);
            }

            inline bool Bind(
                Data::Reference& rcTargetRef,
                Data::Reference& rcActionRef,
                Reflection::Method::IdType& rnMethodId,
                const Reflection::Class* kpcClass,
                const Word::Handle& krcMethodName,
                const Serialization::Descriptor& krcDescriptor,
                const Data::IndexType knObjectInstanceIndex = 0
            ) {
                return Binding::Targets::Bind(
                    m_cBindingParser,
                    rcTargetRef,
                    rcActionRef,
                    rnMethodId,
                    kpcClass,
                    krcMethodName,
                    krcDescriptor,
                    m_cTokenizer.GetTokens(),
                    knObjectInstanceIndex
                );
            }

            inline bool Bind_Run_Delete(
                const Reflection::Class* kpcClass,
                const Word::Handle& krcMethodName, 
                const Serialization::Descriptor& krcDescriptor,
                const Data::IndexType knInstanceIndex
            ) {
                Data::Reference cTargetRef;
                Data::Reference cActionRef;
                Reflection::Method::IdType nMethodId;

                if (!Bind(
                    cTargetRef, 
                    cActionRef, 
                    nMethodId, 
                    kpcClass, 
                    krcMethodName, 
                    krcDescriptor, 
                    knInstanceIndex
                ))
                    return false;

                Data::Storage* pcTargetStorage = m_cBindingParser.FindStorage(cTargetRef);
                Data::Storage* pcActionStorage = m_cBindingParser.FindStorage(cActionRef);

                pcActionStorage->GetPointer<Action<void> >(cActionRef)->Invoke();
                pcActionStorage->Delete(cActionRef);
                pcTargetStorage->Delete(cTargetRef);

                return true;
            }

            inline bool Bind_Run_Delete(
                const Data::Reference& krcObjectRef,
                const Word::Handle& krcMethodName,
                const Serialization::Descriptor& krcDescriptor
            ) {
                return krcObjectRef && krcMethodName && Bind_Run_Delete(
                    Reflection::Class::GetClass(krcObjectRef.GetType()),
                    krcMethodName,
                    krcDescriptor,
                    krcObjectRef.GetIndex()
                );
            }

        private:
            Serialization::Descriptors m_cDescriptors;
            Serialization::Tokenizer m_cTokenizer;
            Binding::Parser m_cBindingParser;
            Data::Tree m_cTree;
            
            std::stringstream m_Stream;
        };

        class EntryPoint
        {
        public:
            typedef std::vector<EntryPoint> VectorType;
            typedef std::map<Word::IdType, EntryPoint> MapType;
            typedef std::stack<EntryPoint> StackType;
            typedef std::deque<EntryPoint> DequeType;
            typedef std::list<EntryPoint> ListType;

            EntryPoint()
                : m_nTargetId(~0)
                , m_nWordId(~0)
                , m_nNodeId(~0) { }

            EntryPoint(const EntryPoint& rhs)
                : m_nTargetId(rhs.m_nTargetId)
                , m_nWordId(rhs.m_nWordId)
                , m_nNodeId(rhs.m_nNodeId) { }

            inline void Reset() {
                m_nTargetId = ~0;
                m_nWordId = ~0;
                m_nNodeId = ~0;
            }

            inline bool IsValid() const {
                return m_nTargetId < ~0
                    && m_nWordId < ~0
                    && m_nNodeId < ~0;
            }

            inline Error Bind(const EntryPoint& krcRootEntry, const Data::Node& krcNode) {
                m_nWordId = ~0;
                m_nTargetId = ~0;
                m_nNodeId = ~0;

                if (krcNode.HasExternalTarget()) {
                    m_nWordId = krcNode.GetWordId();
                    m_nTargetId = krcNode.GetExternalTargetId();
                    m_nNodeId = krcNode.GetTargetNodeId();
                }
                else if (krcNode.HasInternalTarget()) {
                    m_nWordId = krcNode.GetWordId();
                    m_nTargetId = krcRootEntry.GetTargetId();
                    m_nNodeId = krcNode.GetTargetNodeId();
                }

                return Error();
            }

            inline Error Bind(
                const Word::IdType knWordId,
                const Data::IndexType knTargetId,
                const Data::Node::IdType knNodeId
            ) {
                m_nWordId = knWordId;
                m_nTargetId = knTargetId;
                m_nNodeId = knNodeId;

                return Error();
            }


            inline const Word::IdType GetWordId() const {
                return m_nWordId;
            }

            inline const Data::IndexType GetTargetId() const {
                return m_nTargetId;
            }

            inline const Data::Node::IdType GetNodeId() const {
                return m_nNodeId;
            }

            inline operator bool() const {
                return IsValid();
            }

            inline bool operator == (const EntryPoint& rhs) const {
                return m_nTargetId == rhs.m_nTargetId
                    && m_nWordId == rhs.m_nWordId
                    && m_nNodeId == rhs.m_nNodeId;
            }

            inline bool operator != (const EntryPoint& rhs) const {
                return m_nTargetId != rhs.m_nTargetId
                    || m_nWordId != rhs.m_nWordId
                    || m_nNodeId != rhs.m_nNodeId;
            }
        private:
            Data::IndexType m_nTargetId;
            Word::IdType m_nWordId;
            Data::Node::IdType m_nNodeId;
        };

        class Program
        {
            friend class Script;

        public:
            typedef size_t IdType; 
            typedef std::vector<Program> VectorType;

            Program()
            : m_cTree()
            , m_cTargets()
            , m_vecData()
            , m_vecStatus()
            , m_vecAbsentIds()
            , m_pcScript(NULL)
            , m_kpcErrorActionStorage(NULL)
            , m_kpcDefaultActionStorage(NULL)
            , m_kpcBinaryActionStorage(NULL)
            , m_kpcStatusActionStorage(NULL)
            { }

            Program(const Program& rhs)
            : m_cTree(rhs.m_cTree)
            , m_cTargets(rhs.m_cTargets)
            , m_vecData(rhs.m_vecData)
            , m_vecStatus(rhs.m_vecStatus)
            , m_vecAbsentIds(rhs.m_vecAbsentIds)
            , m_pcScript(rhs.m_pcScript)
            , m_kpcErrorActionStorage(rhs.m_kpcErrorActionStorage)
            , m_kpcDefaultActionStorage(rhs.m_kpcDefaultActionStorage)
            , m_kpcBinaryActionStorage(rhs.m_kpcBinaryActionStorage)
            , m_kpcStatusActionStorage(rhs.m_kpcStatusActionStorage)
            { }

            inline void ForceReset() {
                m_pcScript = NULL;
                Clear();
            }

            inline Error BindActionStorages(Data::Repository& rcRepository) {
                m_kpcErrorActionStorage = rcRepository.GetStorage<ErrorActionType>();
                m_kpcDefaultActionStorage = rcRepository.GetStorage<DefaultActionType>();
                m_kpcBinaryActionStorage = rcRepository.GetStorage<BinaryActionType>();
                m_kpcStatusActionStorage = rcRepository.GetStorage<StatusActionType>();
                
                return Error();
            }

            inline Error BindScript(Data::Repository& rcRepository, Script* pcScript) {
                if (m_pcScript == pcScript)
                    return Error();

                Clear();

                m_pcScript = pcScript;

                if (m_pcScript == NULL)
                    return Error();

                m_pcScript->Bind(&rcRepository);

                $return_on_error(m_pcScript->BindTargets(m_cTargets, m_vecAbsentIds));

                m_cTree = m_pcScript->GetTree();

                const size_t knNodes = m_cTree.GetNodes().size();

                m_vecStatus.resize(knNodes, Runner::Ready);

                for (Data::IndexVectorType::const_iterator it = m_vecAbsentIds.begin(); it != m_vecAbsentIds.end(); ++it)
                    m_vecStatus[*it] = Runner::Absent;

                m_vecData.resize(knNodes);

                return BindActionStorages(rcRepository);
            }

            inline bool RebindScript(Data::Repository& rcRepository, const Data::Builder* kpcBuilder = NULL) {
                if (m_vecAbsentIds.empty())
                    return false;
                
                Data::IndexVectorType vecReadyIds;

                m_pcScript->Bind(&rcRepository, kpcBuilder);
                m_pcScript->RebindTargets(m_cTargets, m_vecAbsentIds, vecReadyIds);
                
                for (Data::IndexVectorType::const_iterator it = vecReadyIds.begin(); it != vecReadyIds.end(); ++it)
                    m_vecStatus[*it] = Runner::Ready;

                for (Data::IndexVectorType::const_iterator it = m_vecAbsentIds.begin(); it != m_vecAbsentIds.end(); ++it)
                    m_vecStatus[*it] = Runner::Absent;

                BindActionStorages(rcRepository);

                return m_vecAbsentIds.empty();
            }

            inline bool NeedsScriptRebind() const {
                return m_vecAbsentIds.size() > 0;
            }

            inline Error GetBindingError() const {
                if (m_vecAbsentIds.empty())
                    return Error();
                
                Error cError(m_vecAbsentIds.size(), "Absent Bindings");

                for (Data::IndexVectorType::const_iterator it = m_vecAbsentIds.begin(); it != m_vecAbsentIds.end(); ++it) {
                    const Data::Node::IdType knNodeId = *it;
                    const Serialization::Token& krcNodeToken = m_pcScript->GetNodeToken(knNodeId);
                
                    cError.AddMessage(krcNodeToken.GetWordPointer());
                }

                return cError;
            }

            inline void Clear() {
                m_cTree.Clear();
                m_cTargets.Clear();
                m_vecData.clear();
                m_vecStatus.clear();
                m_vecAbsentIds.clear();
                m_kpcErrorActionStorage = NULL;
                m_kpcDefaultActionStorage = NULL;
                m_kpcBinaryActionStorage = NULL;
                m_kpcStatusActionStorage = NULL;
            }

            inline void SoftReset(
                const EntryPoint& krcRootEntryPoint, 
                const Data::Node& krcRootNode, 
                EntryPoint::DequeType& rdeqTargetEntryPoints
            ) {
                Data::Node::IdDequeType deqNodeIds(1, krcRootNode.GetNodeId());

                while (deqNodeIds.size() > 0)
                {
                    const Data::Node::IdType knNodeId = deqNodeIds.front();
                    const Data::Node& krcNode = m_cTree.GetNode(knNodeId);

                    deqNodeIds.pop_front();

                    m_vecStatus[knNodeId] = Ready;

                    if (krcNode.HasTargetMark()) {
                        EntryPoint cTargetEntryPoint;
                        Error cTargetError = cTargetEntryPoint.Bind(krcRootEntryPoint, krcNode);

                        if (cTargetError)
                            continue;

                        if (cTargetEntryPoint)
                            rdeqTargetEntryPoints.push_back(cTargetEntryPoint);
                    } else {
                        const Data::Node::IdVectorType& krvecChildren = krcNode.GetChildren();

                        for (Data::Node::IdVectorType::const_iterator it = krvecChildren.begin(); it != krvecChildren.end(); ++it) {
                            const Data::Node::IdType knChildNodeId = *it;

                            deqNodeIds.push_back(knChildNodeId);
                        }
                    }
                }
            }

            inline void SoftReset() {
                for (size_t i = 0; i < m_cTree.GetNodes().size(); ++i) {
                    m_vecStatus[i] = Ready;
                }
            }

            inline const Data::Node& GetEntryPointNode(const EntryPoint& krcEntryPoint) const {
                return GetNode(krcEntryPoint.GetNodeId());
            }

            inline EStatus GetNodeStatus(const Data::Node& krcNode) const {
                return m_vecStatus[krcNode.GetNodeId()];
            }

            inline void SetNodeStatus(const Data::Node& krcNode, const EStatus keStatus) {
                m_vecStatus[krcNode.GetNodeId()] = keStatus;
            }

            inline Script* GetScript() const {
                return m_pcScript;
            }

            inline Data::Tree& GetTree() {
                return m_cTree;
            }
            inline const Data::Tree& GetTree() const {
                return m_cTree;
            }

            inline const Data::Node& GetNode(const Data::Node::IdType knNodeId) const {
                return m_cTree.GetNode(knNodeId);
            }
            inline const Data::Node& GetNode(const Data::Node& krcNode, const size_t knIndex) const {
                return krcNode.GetChild(m_cTree.GetNodes(), knIndex);
            }

            inline const Reflection::Method& GetNodeMethod(const Data::Node& krcNode) const {
                return m_cTargets.GetMethod(krcNode.GetNodeId());
            }

            inline const Data::Reference& GetNodeData(const Data::Node& krcNode) {
                return m_vecData[krcNode.GetNodeId()];
            }

            inline void SetNodeData(const Data::Node& krcNode, const Data::Reference& krcData) {
                m_vecData[krcNode.GetNodeId()] = krcData;
            }

            template<typename T>
            T& GetOrCreateNodeData(Data::Repository& rcRepository, const Data::Node& krcNode) {
                return rcRepository.GetOrCreateObject<T>(m_vecData[krcNode.GetNodeId()]);
            }
            
            inline EStatus Invoke(const Data::Node& krcNode) {
                EStatus eStatus = Ready;

                const Data::Node::IdType knNodeId(krcNode.GetNodeId());
                const Data::Reference& krcActionReference = m_cTargets.GetAction(knNodeId);
                
                if (!krcActionReference)
                    return eStatus;

                if (TryInvoke<StatusActionType>(m_kpcStatusActionStorage, krcActionReference, knNodeId, eStatus))
                    return eStatus;
                
                if (TryInvoke<BinaryActionType>(m_kpcBinaryActionStorage, krcActionReference, knNodeId, eStatus))
                    return eStatus;

                if (TryInvoke<DefaultActionType>(m_kpcDefaultActionStorage, krcActionReference, knNodeId, eStatus))
                    return eStatus;

                if (TryInvoke<ErrorActionType>(m_kpcErrorActionStorage, krcActionReference, knNodeId, eStatus))
                    return eStatus;

                return eStatus;
            }
        private:
            template<typename ActionType>
            inline bool TryInvoke(
                const Data::Storage* kpcActionStorage,
                const Data::Reference& krcActionReference,
                const Data::Node::IdType knNodeId,
                EStatus& rStatus
            ) {
                return kpcActionStorage && TryInvoke(kpcActionStorage->GetPointer<ActionType>(krcActionReference), knNodeId, rStatus);
            }

            inline bool TryInvoke(const ErrorActionType* kpcAction, const Data::Node::IdType knNodeId, EStatus& rStatus) {
                if (kpcAction) {
                    EStatus& rNodeStatus = m_vecStatus[knNodeId];
                    const Error krcError = kpcAction->Invoke();

                    if (krcError)
                        rNodeStatus = Runner::Failed;
                    else if (rNodeStatus != Runner::Running)
                        rNodeStatus = Runner::Succeeded;

                    rStatus = rNodeStatus;

                    return true;
                }

                return false;
            }

            inline bool TryInvoke(const DefaultActionType* kpcAction, const Data::Node::IdType knNodeId, EStatus& rStatus) {
                if (kpcAction) {
                    kpcAction->Invoke();

                    rStatus = m_vecStatus[knNodeId];

                    return true;
                }

                return false;
            }

            inline bool TryInvoke(const BinaryActionType* kpcAction, const Data::Node::IdType knNodeId, EStatus& rStatus) {
                if (kpcAction) {
                    if (kpcAction->Invoke())
                        rStatus = Succeeded;
                    else
                        rStatus = Failed;

                    m_vecStatus[knNodeId] = rStatus;

                    return true;
                }

                return false;
            }
            
            inline bool TryInvoke(const StatusActionType* kpcAction, const Data::Node::IdType knNodeId,  EStatus& rStatus) {
                if (kpcAction) {
                    rStatus = kpcAction->Invoke();

                    m_vecStatus[knNodeId] = rStatus;

                    return true;
                }

                return false;
            }
        private:
            Script* m_pcScript;
            Data::Tree m_cTree;
            Binding::Targets m_cTargets;
            Data::Reference::VectorType m_vecData;
            StatusVectorType m_vecStatus;
            Data::IndexVectorType m_vecAbsentIds;
            const Data::Storage* m_kpcErrorActionStorage;
            const Data::Storage* m_kpcDefaultActionStorage;
            const Data::Storage* m_kpcBinaryActionStorage;
            const Data::Storage* m_kpcStatusActionStorage;
        };


        class Programs
        {
            struct ProgramTreeProvider
            {
                inline Data::Tree& operator[] (const Data::IndexType knTargetIndex) {
                    return m_pcPrograms->m_vecPrograms[knTargetIndex].GetTree();
                }

                inline const Data::Tree& operator[] (const Data::IndexType knTargetIndex) const {
                    return m_pcPrograms->m_vecPrograms[knTargetIndex].GetTree();
                }

                inline Data::IndexType size() const {
                    return m_pcPrograms->m_vecPrograms.size();
                }

                Programs* m_pcPrograms;

                ProgramTreeProvider(Programs* pcPrograms) : m_pcPrograms(pcPrograms) { }
            };

            struct ScriptTreeProvider
            {
                inline const Data::Tree& operator[] (const Data::IndexType knTargetIndex) const {
                    return m_pcPrograms->m_vecScripts[knTargetIndex]->GetTree();
                }

                inline Data::IndexType size() const {
                    return m_pcPrograms->m_vecScripts.size();
                }

                Programs* m_pcPrograms;

                ScriptTreeProvider(Programs* pcPrograms) : m_pcPrograms(pcPrograms) { }
            };
        public:
            Programs()
            : m_cInterfaces()
            , m_cLinker()
            , m_vecScripts()
            , m_vecPrograms()
            , m_bDirtyScripts(false)
            { }

            Programs(const Programs& rhs)
            : m_cInterfaces(rhs.m_cInterfaces)
            , m_cLinker(rhs.m_cLinker)
            , m_vecScripts(rhs.m_vecScripts)
            , m_vecPrograms(rhs.m_vecPrograms)
            , m_bDirtyScripts(rhs.m_bDirtyScripts)
            { }

            void Reset() {
                m_cInterfaces.Clear();
                m_cLinker.Clear();
                m_vecScripts.clear();
                m_vecPrograms.clear();
                m_bDirtyScripts = false;
            }

            inline void SoftReset() {
                for (Program::VectorType::iterator it = m_vecPrograms.begin(); it != m_vecPrograms.end(); ++it) {
                    it->SoftReset();
                }
            }

            inline void SoftReset(const EntryPoint& krcRootEntryPoint) {
                EntryPoint::DequeType deqEntryPoints(1, krcRootEntryPoint);

                while (deqEntryPoints.size() > 0) {
                    const EntryPoint kcEntry = deqEntryPoints.front();
                    
                    deqEntryPoints.pop_front();

                    if (!kcEntry)
                        continue;

                    Program& rcProgram = GetEntryPointProgram(kcEntry);
                    const Data::Node& krcRootNode = rcProgram.GetEntryPointNode(kcEntry);
                    
                    rcProgram.SoftReset(kcEntry, krcRootNode, deqEntryPoints);
                }
            }

            inline Error BindEntryPoint(
                EntryPoint& rcEntryPoint, 
                const Word::Handle& krcWordHandle, 
                const Word::Handle kcSymbolWordHandle = "",
                const bool kbRequired = true
            ) {
                const Data::Link* kpcEntryLink = m_cLinker.FindLink(krcWordHandle.GetId(), kcSymbolWordHandle.GetId());

                if (kpcEntryLink == NULL)
                    return Error(kbRequired, "link not found",krcWordHandle.c_str(), kcSymbolWordHandle.c_str(), NULL);

                return rcEntryPoint.Bind(krcWordHandle.GetId(), kpcEntryLink->m_nTargetIndex, kpcEntryLink->m_nNodeId);
            }


            inline Error AddScript(Script* pcScript) {
                if (pcScript == NULL)
                    return Error(1, "invalid parameter");

                for (Script::PointerVectorType::iterator script = m_vecScripts.begin(); script != m_vecScripts.end(); ++script) {
                    if (*script == pcScript) {
                        return Error(2, "already added");
                    } else if (*script == NULL) {
                        *script = pcScript;
                        m_bDirtyScripts = true;
                        return Error();
                    }
                }

                m_vecScripts.push_back(pcScript);

                m_bDirtyScripts = true;

                return Error();
            }

            inline Error RemoveScript(Script* pcScript) {
                if (pcScript == NULL)
                    return Error(1, "invalid parameter");

                if (m_vecScripts.empty())
                    return Error(2, "no scripts");

                for (Script::PointerVectorType::iterator script = m_vecScripts.begin(); script != m_vecScripts.end(); ++script) {
                    if (*script == pcScript) {
                        *script = NULL;
                        m_bDirtyScripts = true;
                        return Error();
                    }
                }

                return Error(3, "script not found");
            }

            inline Error ResolveInterfaces(Data::Repository* pcRepository, Data::Builder* pcBuilder = NULL) {
                
                if (pcRepository == NULL)
                    return Error(2, "absent repository");

                if (pcBuilder == NULL)
                    pcBuilder = pcRepository->GetFirstObject<Data::Builder>();

                if (pcBuilder == NULL)
                    return Error(1, "absent builder");

                m_cInterfaces.Bind(pcRepository, pcBuilder->GetNamespaceIds());

                return Error();
            }

            inline Error ResolvePrograms(Data::Repository& rcRepository) {
                if (!m_bDirtyScripts)
                    return Error();

                Data::Linker cNewLinker;
                const ScriptTreeProvider kcScriptTreeProvider(this);

                $return_on_error(cNewLinker.SetDefinitions(kcScriptTreeProvider));
                $return_on_error(cNewLinker.CheckExternalDeclarations(kcScriptTreeProvider));
                $return_on_error(cNewLinker.CheckCircularity(kcScriptTreeProvider));

                m_cLinker = cNewLinker;
                
                m_vecPrograms.resize(m_vecScripts.size());

                for (Script::IdType id = 0; id < m_vecScripts.size();) {
                    if (m_vecScripts[id] == NULL) {
                        m_vecScripts.erase(m_vecScripts.begin() + id);
                        m_vecPrograms.erase(m_vecPrograms.begin() + id);
                    } else {
                        ++id;
                    }
                }

                for (Script::IdType id = 0; id < m_vecScripts.size(); id++) {
                    if (m_vecPrograms[id].GetScript() != m_vecScripts[id]) {
                        $return_on_error(m_vecPrograms[id].BindScript(rcRepository, m_vecScripts[id]));
                    }
                }

                ProgramTreeProvider cProgramTreeProvider(this);

                $return_on_error(m_cLinker.LinkExternalDeclarations(cProgramTreeProvider));
                
                m_bDirtyScripts = false;

                return Error();
            }


            inline const Data::Node& GetEntryPointNode(const EntryPoint& krcEntryPoint) const {
                return m_vecPrograms[krcEntryPoint.GetTargetId()].GetNode(krcEntryPoint.GetNodeId());
            }
            inline const Program& GetEntryProgram(const EntryPoint& krcEntryPoint) const {
                return m_vecPrograms[krcEntryPoint.GetTargetId()];
            }
            inline Program& GetEntryPointProgram(const EntryPoint& krcEntryPoint) {
                return m_vecPrograms[krcEntryPoint.GetTargetId()];
            }

            inline const Binding::Interfaces& GetInterfaces() const {
                return m_cInterfaces;
            }

            inline Program& GetProgram(const Program::IdType knProgramId) {
                return m_vecPrograms[knProgramId];
            }

            inline const Data::Link* FindLink(const Word::IdType knRootWordId, const Word::IdType knSymbolWordId = ~0) const {
                return m_cLinker.FindLink(knRootWordId, knSymbolWordId);
            }

            inline const Data::Node* FindNode(const Data::Link* kpcLink) const {
                if (kpcLink )
                    return &m_vecPrograms[kpcLink->m_nTargetIndex].GetNode(kpcLink->m_nNodeId);

                return NULL;
            }
        private:
            Binding::Interfaces m_cInterfaces;
            Data::Linker m_cLinker;
            Script::PointerVectorType m_vecScripts;
            Program::VectorType m_vecPrograms;
            bool m_bDirtyScripts;
        };

        class Processor
        {
        public:
            typedef std::stack<size_t> IdStackType;
            typedef std::stack<Processor*> PointerStackType;

            Processor()
            : m_pcRepository(NULL)
            , m_pcPrograms(NULL)
            { }

            Processor(const Processor& lhs)
            : m_pcRepository(lhs.m_pcRepository)
            , m_pcPrograms(lhs.m_pcPrograms)
            { }

            inline Error Bind(Data::Repository* pcRepository, Programs* pcPrograms) {
                if (pcRepository == NULL)
                    return Error(1, "invalid parameter");
                if (pcPrograms == NULL)
                    return Error(2, "invalid parameter");

                m_pcRepository = pcRepository;
                m_pcPrograms = pcPrograms;

                return Error();
            }

            inline Error BindInterfaces(Data::Builder* pcBuilder = NULL) {
                return m_pcPrograms->ResolveInterfaces(m_pcRepository, pcBuilder);
            }

            template<typename SignatureType>
            inline Error BindInterfaceInvokers(Binding::InterfaceInvokersFor<SignatureType>& rcInterfaceInvokers, const Word::Handle& krcNameWordHandle) const {
                return rcInterfaceInvokers.Bind(*m_pcRepository, m_pcPrograms->GetInterfaces(), krcNameWordHandle.GetId());
            }
            
            inline const IdStackType& GetCurrentNodeIdsStack() const {
                return m_stackCurrentNodeIds;
            }
            
            inline const IdStackType& GetCurrentProgramIdsStack() const {
                return m_stackCurrentProgramIds;
            }
            
            static PointerStackType& GetCurrentProcessorsStack() {
                static PointerStackType s_stackCurrentProcessors;

                return s_stackCurrentProcessors;
            }

            static Processor& Get() {
                return *GetCurrentProcessorsStack().top();
            }

            inline Data::Repository* GetRepository() const {
                return m_pcRepository;
            }

            inline Program& GetProgram() const {
                return m_pcPrograms->GetProgram(m_stackCurrentProgramIds.top());
            }

            inline const Data::Node& GetNode() const {
                return GetProgram().GetNode(m_stackCurrentNodeIds.top());
            }

            inline const Reflection::Method& GetNodeMethod() const {
                const Program& krcProgram = GetProgram();
                const Data::Node::IdType knNodeId = m_stackCurrentNodeIds.top();
                const Data::Node& krcNode = krcProgram.GetNode(knNodeId);

                return krcProgram.GetNodeMethod(krcNode);
            }

            inline const Word::Handle& GetNodeName() const {
                return GetNodeMethod().GetName().GetWordHandle();
            }

            inline const Data::Node& GetChild(size_t nIndex) const {
                return GetProgram().GetNode(GetNode(), nIndex);
            }
            inline const Data::Node& GetChild(const Data::Node& krcNode, size_t nIndex) const {
                return GetProgram().GetNode(krcNode, nIndex);
            }

            inline const Data::Node* GetOptionalChild(size_t nIndex) const {
                return GetNode().GetOptionalChild(GetProgram().GetTree().GetNodes(), nIndex);
            }
            inline const Data::Node* GetOptionalChild(const Data::Node& krcNode, size_t nIndex) const {
                return krcNode.GetOptionalChild(GetProgram().GetTree().GetNodes(), nIndex);
            }

            inline const Data::Node* FindRoot(const Word::IdType knRootWordId, const Word::IdType knSymbolWordId = ~0) const {
                return m_pcPrograms->FindNode(m_pcPrograms->FindLink(knRootWordId, knSymbolWordId));
            }

            inline EStatus Invoke(const Data::Node& krcNode) {
                EStatus eStatus = GetStatus(krcNode);

                if (eStatus == Runner::Ready || eStatus == Runner::Running) {
                    if (krcNode.HasInternalTarget())
                        return InvokeInternalProxy(krcNode);
                    if (krcNode.HasExternalTarget())
                        return InvokeExternalProxy(krcNode);

                    m_stackCurrentNodeIds.push(krcNode.GetNodeId());

                    eStatus = GetProgram().Invoke(krcNode);

                    if (eStatus == Runner::Ready)
                        SetStatus(krcNode, eStatus = Runner::Running);

                    m_stackCurrentNodeIds.pop();
                }
                
                return eStatus;
            }

            inline EStatus InvokeInternalProxy(const Data::Node& krcProxyNode) {
                const Data::Node::IdType knNodeId = krcProxyNode.GetTargetNodeId();

                return Invoke(GetProgram().GetNode(knNodeId));
            }

            inline EStatus InvokeExternalProxy(const Data::Node& krcProxyNode) {
                const Data::Node::IdType knNodeId = krcProxyNode.GetTargetNodeId();

                m_stackCurrentProgramIds.push(krcProxyNode.GetExternalTargetId());

                const EStatus eStatus = Invoke(GetProgram().GetNode(knNodeId));

                m_stackCurrentProgramIds.pop();

                return eStatus;
            }

            inline EStatus InvokeChild(const Data::Node& krcNode, size_t nIndex) {
                return Invoke(GetProgram().GetNode(krcNode, nIndex));
            }

            inline EStatus InvokeChild(size_t nIndex) {
                return Invoke(GetProgram().GetNode(GetNode(), nIndex));
            }

            inline EStatus InvokeEntry(const EntryPoint& krcEntry, const EStatus keDefaultStatus) {
                EStatus eStatus = keDefaultStatus;

                if (krcEntry && this->operator bool()) {
                    const Data::Node& krcRootNode = m_pcPrograms->GetEntryPointNode(krcEntry);
                    PointerStackType& rstackCurrentProcessors = GetCurrentProcessorsStack();
                    m_stkEntryPoints.push(krcEntry);
                    m_stackCurrentProgramIds.push(krcEntry.GetTargetId());
                    rstackCurrentProcessors.push(this);

                    eStatus = Invoke(krcRootNode);

                    rstackCurrentProcessors.pop();
                    m_stackCurrentProgramIds.pop();
                    m_stkEntryPoints.pop();
                }

                return eStatus;
            }

            inline EStatus GetStatus(const Data::Node& krcNode) const {
                return GetProgram().GetNodeStatus(krcNode); 
            }

            inline void SetStatus(const Data::Node& krcNode, const EStatus keStatus) const {
                GetProgram().SetNodeStatus(krcNode, keStatus);
            }
            
            template<typename T>
            inline T& GetOrCreateData(const Data::Node& krcNode) {
                return GetProgram().GetOrCreateNodeData<T>(*m_pcRepository, krcNode);
            }

            template<typename T>
            inline T& GetOrCreateVariable(const Data::Node& krcNode) {
                return GetProgram().GetOrCreateNodeData<Data::Variable<T> >(*m_pcRepository, krcNode).GetReference();
            }

            template<typename T>
            inline T& GetOrCreateEntryPointData(const EntryPoint& krcEntryPoint) {
                Program& rcProgram = m_pcPrograms->GetEntryPointProgram(krcEntryPoint);
                const Data::Node& krcNode = rcProgram.GetEntryPointNode(krcEntryPoint);

                return rcProgram.GetOrCreateNodeData<T>(*m_pcRepository, krcNode);
            }

            template<typename T>
            inline T* GetEntryPointDataPointer() const {
                if (m_pcRepository == NULL)
                    return NULL;
                if (m_pcPrograms == NULL)
                    return NULL;

                if (m_stkEntryPoints.empty())
                    return NULL;

                const EntryPoint& krcEntryPoint = m_stkEntryPoints.top();

                if (!krcEntryPoint)
                    return NULL;

                const Program& rcProgram = m_pcPrograms->GetEntryPointProgram(krcEntryPoint);
                const Data::Node& krcNode = rcProgram.GetEntryPointNode(krcEntryPoint);
                const Data::Reference& krcReference = rcProgram.GetNodeData(krcNode);

                if (!krcReference)
                    return NULL;

                return m_pcRepository->GetObjectByReference<T>(krcReference);
            }

            inline bool IsBusy() const {
                return m_stackCurrentNodeIds.size() > 0;
            }

            inline operator bool() const {
                return m_pcPrograms && m_pcRepository;
            }
            inline EStatus operator() (const EntryPoint& krcEntry, const EStatus keDefaultStatus = Runner::Ready) {
                return InvokeEntry(krcEntry, keDefaultStatus);
            }
            inline void Ready() {
                GetProgram().SetNodeStatus(GetNode(), Runner::Ready);
            }
            inline void Running() {
                GetProgram().SetNodeStatus(GetNode(), Runner::Running);
            }
            inline void Succeeded() {
                GetProgram().SetNodeStatus(GetNode(), Runner::Succeeded);
            }
            inline void Failed() {
                GetProgram().SetNodeStatus(GetNode(), Runner::Failed);
            }
        private:
            Data::Repository* m_pcRepository;
            Programs* m_pcPrograms;

            EntryPoint::StackType m_stkEntryPoints;

            IdStackType m_stackCurrentNodeIds;
            IdStackType m_stackCurrentProgramIds;
        };
    }
}