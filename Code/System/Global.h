#pragma once

#include <stack>


namespace System
{
    namespace Global
    {
        class Stack
        {
        public:
            template<typename T>
            static std::stack<T>& Get() {
                static std::stack<T> s_stk;

                return s_stk;
            }

            template<typename T>
            static T* Top() {
                std::stack<T>& rstk = Get<T>();

                if (rstk.empty())
                    return NULL;

                return &rstk.top();
            }


            template<typename T>
            static void Push(const T& val) {
                std::stack<T>& rstk = Get<T>();

                rstk.push(val);
            }

            template<typename T>
            static bool Pop() {
                std::stack<T>& rstk = Get<T>();

                if (rstk.empty())
                    return false;

                rstk.pop();

                return true;
            }
        };

        class Singleton
        {
        public:
            template<typename T>
            static T& Get() {
                static T s_instance;

                return s_instance;
            }
        };

        class Service
        {
            template<typename T>
            static bool& Allocated() {
                static bool s_allocated = false;

                return s_allocated;
            }

            template<typename T>
            static T*& Pointer() {
                static T* s_ptr = NULL;

                return s_ptr;
            }
        public:

            template<typename I, typename T>
            static void Use(T* ptr) {
                if (Allocated())
                    Destroy();

                Pointer() = static_cast<I*>(ptr);
                Allocated() = false;
            }

            template<typename T>
            static void Set(T* ptr) {
                if (Allocated())
                    Destroy();

                Pointer() = ptr;
                Allocated() = false;
            }

            template<typename T>
            static T* Get() {
                return Pointer();
            }

            template<typename T>
            static T* GetOrCreate() {
                T*& ptr = Pointer();

                if (ptr == NULL) {
                    ptr = new T();

                    Allocated() = true;
                }
                
                return ptr;
            }

            template<typename T>
            static bool Destroy() {
                if (!Allocated())
                    return false;

                T*& ptr = Pointer();

                if (ptr == NULL)
                    return false;

                delete ptr;

                ptr = NULL;

                Allocated() = false;

                return true;
            }
        };
    }
}