#pragma once

#include "SFINE.h"

namespace System
{
    namespace SFINE
    {
        template <typename E> struct IsIntConvertable
        {
            static No eval(...);
            static Yes eval(int z);

            static const bool value = sizeof(Yes) == sizeof(eval(*(E*)0));
        };




        template<typename T>
        No operator < (T const&, Fallback const&);
        template<typename T>
        No operator > (T const&, Fallback const&);
        template<typename T>
        No operator <= (T const&, Fallback const&);
        template<typename T>
        No operator >= (T const&, Fallback const&);
        template<typename T>
        No operator == (T const&, Fallback const&);
        template<typename T>
        No operator != (T const&, Fallback const&);

        No operator ++ (Fallback const&);
        No operator -- (Fallback const&);

        No operator ++ (Fallback const&, int);
        No operator -- (Fallback const&, int);
        
        No operator ! (Fallback const&);
        template<typename T>
        No operator && (T const&, Fallback const&);
        template<typename T>
        No operator || (T const&, Fallback const&);


        No operator + (Fallback const&);
        No operator - (Fallback const&);
        No operator ~ (Fallback const&);

        template<typename T>
        No operator + (T const&, Fallback const&);
        template<typename T>
        No operator - (T const&, Fallback const&);
        template<typename T>
        No operator * (T const&, Fallback const&);
        template<typename T>
        No operator / (T const&, Fallback const&);
        template<typename T>
        No operator % (T const&, Fallback const&);
        template<typename T>
        No operator & (T const&, Fallback const&);
        template<typename T>
        No operator | (T const&, Fallback const&);
        template<typename T>
        No operator ^ (T const&, Fallback const&);
        template<typename T>
        No operator << (T const&, Fallback const&);
        template<typename T>
        No operator >> (T const&, Fallback const&);

        template<typename T>
        No operator += (T const&, Fallback const&);
        template<typename T>
        No operator -= (T const&, Fallback const&);
        template<typename T>
        No operator *= (T const&, Fallback const&);
        template<typename T>
        No operator /= (T const&, Fallback const&);
        template<typename T>
        No operator %= (T const&, Fallback const&);
        template<typename T>
        No operator &= (T const&, Fallback const&);
        template<typename T>
        No operator |= (T const&, Fallback const&);
        template<typename T>
        No operator ^= (T const&, Fallback const&);
        template<typename T>
        No operator <<= (T const&, Fallback const&);
        template<typename T>
        No operator >>= (T const&, Fallback const&);

        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::LeftShift> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs << rhs)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::RightShift> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs >> rhs)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::GreaterThan> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<bool>(Choice<1>(), lhs > rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::LessThan> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<bool>(Choice<1>(), lhs < rhs)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::GreaterThanOrEqualTo> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<bool>(Choice<1>(), lhs >= rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::LessThanOrEqualTo> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<bool>(Choice<1>(), lhs <= rhs)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::EqualTo> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<bool>(Choice<1>(), lhs == rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::NotEqualTo> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<bool>(Choice<1>(), lhs != rhs)) == sizeof(Yes);
        };


        template<typename LHS>
        struct ExternalOperator<LHS, LHS, Operator::PreIncrement> : ExternalOperator<LHS, LHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), ++lhs)) == sizeof(Yes);
        };
        template<typename LHS>
        struct ExternalOperator<LHS, LHS, Operator::PreDecrement> : ExternalOperator<LHS, LHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), --lhs)) == sizeof(Yes);
        };

        template<typename LHS>
        struct ExternalOperator<LHS, LHS, Operator::PostIncrement> : ExternalOperator<LHS, LHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs++)) == sizeof(Yes);
        };
        template<typename LHS>
        struct ExternalOperator<LHS, LHS, Operator::PostDecrement> : ExternalOperator<LHS, LHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs--)) == sizeof(Yes);
        };

        template<typename LHS>
        struct ExternalOperator<LHS, LHS, Operator::Negation> : ExternalOperator<LHS, LHS> {
            static bool const value = sizeof(Test<bool>(Choice<1>(), !lhs)) == sizeof(Yes);
        };
        template<typename LHS>
        struct ExternalOperator<LHS, LHS, Operator::And> : ExternalOperator<LHS, LHS> {
            static bool const value = sizeof(Test<bool>(Choice<1>(), lhs && rhs)) == sizeof(Yes);
        };
        template<typename LHS>
        struct ExternalOperator<LHS, LHS, Operator::InclusiveOr> : ExternalOperator<LHS, LHS> {
            static bool const value = sizeof(Test<bool>(Choice<1>(), lhs || rhs)) == sizeof(Yes);
        };


        template<typename LHS>
        struct ExternalOperator<LHS, LHS, Operator::UnaryPlus> : ExternalOperator<LHS, LHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), +lhs)) == sizeof(Yes);
        };
        template<typename LHS>
        struct ExternalOperator<LHS, LHS, Operator::UnaryMinus> : ExternalOperator<LHS, LHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), -lhs)) == sizeof(Yes);
        };
        template<typename LHS>
        struct ExternalOperator<LHS, LHS, Operator::BitwiseNot> : ExternalOperator<LHS, LHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), ~lhs)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::Addition> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs + rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::Subtraction> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs - rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::Multiplication> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs * rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::Division> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs / rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::Remainder> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs % rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::BitwiseAnd> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs & rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::BitwiseOr> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs | rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::BitwiseXor> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs ^ rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::BitwiseLeftShift> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs << rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::BitwiseRightShift> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<1>(), lhs >> rhs)) == sizeof(Yes);
        };


        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::AdditionAssignment> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs += rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::SubtractionAssignment> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs -= rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::MultiplicationAssignment> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs *= rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::DivisionAssignment> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs /= rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::RemainderAssignment> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs %= rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::BitwiseAndAssignment> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs &= rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::BitwiseOrAssignment> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs |= rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::BitwiseXorAssignment> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs ^= rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::BitwiseLeftShiftAssignment> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs <<= rhs)) == sizeof(Yes);
        };
        template<typename LHS, typename RHS>
        struct ExternalOperator<LHS, RHS, Operator::BitwiseRightShiftAssignment> : ExternalOperator<LHS, RHS> {
            static bool const value = sizeof(Test<LHS>(Choice<0>(), lhs >>= rhs)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::SimpleAssignment> {
            template<typename L, typename R> 
            static Yes Test(ReallyHas< L&(L::*)(const R&), &L::operator= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::AdditionAssignment> {
            
            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(const R&), &L::operator+= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::SubtractionAssignment> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(const R&), &L::operator-= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::MultiplicationAssignment> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(const R&), &L::operator*= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::DivisionAssignment> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(const R&), &L::operator/= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::RemainderAssignment> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(const R&), &L::operator%= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::BitwiseAndAssignment> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(const R&), &L::operator&= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::BitwiseOrAssignment> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(const R&), &L::operator|= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::BitwiseXorAssignment> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(const R&), &L::operator^= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::BitwiseLeftShiftAssignment> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(const R&), &L::operator<<= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::BitwiseRightShiftAssignment> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(const R&), &L::operator>>= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS>
        struct InternalOperator<LHS, LHS, Operator::PreIncrement> {

            template<typename L>
            static Yes Test(ReallyHas< L& (L::*)(), &L::operator++ >*);

            static const bool value = sizeof(Test<LHS>(0)) == sizeof(Yes);
        };

        template<typename LHS>
        struct InternalOperator<LHS, LHS, Operator::PreDecrement> {

            template<typename L>
            static Yes Test(ReallyHas< L& (L::*)(), &L::operator-- >*);

            static const bool value = sizeof(Test<LHS>(0)) == sizeof(Yes);
        };

        template<typename LHS>
        struct InternalOperator<LHS, LHS, Operator::PostIncrement> {

            template<typename L>
            static Yes Test(ReallyHas< L (L::*)(int), &L::operator++ >*);

            static const bool value = sizeof(Test<LHS>(0)) == sizeof(Yes);
        };

        template<typename LHS>
        struct InternalOperator<LHS, LHS, Operator::PostDecrement> {

            template<typename L>
            static Yes Test(ReallyHas< L(L::*)(int), &L::operator-- >*);

            static const bool value = sizeof(Test<LHS>(0)) == sizeof(Yes);
        };

        template<typename LHS>
        struct InternalOperator<LHS, LHS, Operator::UnaryPlus> {

            template<typename L>
            static Yes Test(ReallyHas< L(L::*)() const, &L::operator+ >*);

            static const bool value = sizeof(Test<LHS>(0)) == sizeof(Yes);
        };

        template<typename LHS>
        struct InternalOperator<LHS, LHS, Operator::UnaryMinus> {

            template<typename L>
            static Yes Test(ReallyHas< L(L::*)() const, &L::operator- >*);

            static const bool value = sizeof(Test<LHS>(0)) == sizeof(Yes);
        };


        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::Addition> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L(L::*)(const R&) const, &L::operator+ >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::Subtraction> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L(L::*)(const R&) const, &L::operator- >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::Multiplication> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L(L::*)(const R&) const, &L::operator* >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::Division> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L(L::*)(const R&) const, &L::operator/ >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::Remainder> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L(L::*)(const R&) const, &L::operator% >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS>
        struct InternalOperator<LHS, LHS, Operator::BitwiseNot> {

            template<typename L>
            static Yes Test(ReallyHas< L(L::*)() const, &L::operator~ >*);

            static const bool value = sizeof(Test<LHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::BitwiseAnd> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L(L::*)(const R&) const, &L::operator& >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::BitwiseOr> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L(L::*)(const R&) const, &L::operator| >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::BitwiseXor> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L(L::*)(const R&) const, &L::operator^ >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::BitwiseLeftShift> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L(L::*)(const R&) const, &L::operator<< >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::BitwiseRightShift> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L(L::*)(const R&) const, &L::operator>> >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS>
        struct InternalOperator<LHS, LHS, Operator::Negation> {

            template<typename L>
            static Yes Test(ReallyHas< bool(L::*)() const, &L::operator! > *);

            static const bool value = sizeof(Test<LHS>(0)) == sizeof(Yes);
        };

        template<typename LHS>
        struct InternalOperator<LHS, LHS, Operator::And> {

            template<typename L>
            static Yes Test(ReallyHas< bool(L::*)() const, &L::operator&& >*);

            static const bool value = sizeof(Test<LHS>(0)) == sizeof(Yes);
        };

        template<typename LHS>
        struct InternalOperator<LHS, LHS, Operator::InclusiveOr> {

            template<typename L>
            static Yes Test(ReallyHas< bool(L::*)() const, &L::operator|| >*);

            static const bool value = sizeof(Test<LHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::EqualTo> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< bool(L::*)(const R&) const, &L::operator== > *);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::NotEqualTo> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< bool(L::*)(const R&) const, &L::operator!= > *);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::LessThan> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< bool(L::*)(const R&) const, &L::operator< >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        /*
        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::GreaterThan> {

            template<typename L, typename R>
            static Yes Test(ReallyHas2< bool(L::*)(const R&) const, &L::operator>, false >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };
        */

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::LessThanOrEqualTo> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< bool(L::*)(const R&) const, &L::operator<= > *);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::GreaterThanOrEqualTo> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< bool(L::*)(const R&) const, &L::operator>= >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::LeftShift> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L&(L::*)(R&), &L::operator<< >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };

        template<typename LHS, typename RHS>
        struct InternalOperator<LHS, RHS, Operator::RightShift> {

            template<typename L, typename R>
            static Yes Test(ReallyHas< L& (L::*)(R&), &L::operator>> >*);

            static const bool value = sizeof(Test<LHS, RHS>(0)) == sizeof(Yes);
        };
    }


    template <typename T> struct IsArray : SFINE::False {};
    template <class T, size_t N> struct IsArray<T[N]> : SFINE::True {};
    template <class T, size_t N> struct IsArray<T const [N]> : SFINE::True {};
    template <class T, size_t N> struct IsArray<T volatile [N]> : SFINE::True {};
    template <class T, size_t N> struct IsArray<T const volatile [N]> : SFINE::True {};


    template <class T> struct IsReference : SFINE::False {};
    template <class T> struct IsReference<T&> : SFINE::True {};
    template <class T> struct IsReference<const T&> : SFINE::True {};
    template <class T> struct IsReference<volatile T&> : SFINE::True {};
    template <class T> struct IsReference<const volatile T&> : SFINE::True {};


    template <class T> struct IsFloat : SFINE::False {};
    template <class T> struct IsFloat<const T> : IsFloat<T> {};
    template <class T> struct IsFloat<volatile const T> : IsFloat<T> {};
    template <class T> struct IsFloat<volatile T> : IsFloat<T> {};
    template<> struct IsFloat<float> : SFINE::True {};
    template<> struct IsFloat<double> : SFINE::True {};
    template<> struct IsFloat<long double> : SFINE::True {};


    template <class T> struct IsIntegral : SFINE::False {};
    template <class T> struct IsIntegral<const T> : IsIntegral<T> {};
    template <class T> struct IsIntegral<volatile const T> : IsIntegral<T> {};
    template <class T> struct IsIntegral<volatile T> : IsIntegral<T> {};

    template<> struct IsIntegral<unsigned char> : SFINE::True {};
    template<> struct IsIntegral<unsigned short> : SFINE::True {};
    template<> struct IsIntegral<unsigned int> : SFINE::True {};
    template<> struct IsIntegral<unsigned long> : SFINE::True {};

    template<> struct IsIntegral<signed char> : SFINE::True {};
    template<> struct IsIntegral<short> : SFINE::True {};
    template<> struct IsIntegral<int> : SFINE::True {};
    template<> struct IsIntegral<long> : SFINE::True {};

    template<> struct IsIntegral<char> : SFINE::True {};
    template<> struct IsIntegral<bool> : SFINE::True {};

    template <typename E> struct IsClass
    {
        template <class U> static SFINE::Yes eval(void(U::*)(void));
        template <class U> static SFINE::No eval(...);

        static const bool value = sizeof(SFINE::Yes) == sizeof(eval<E>(0));
    };

    template<typename T> struct IsMember : SFINE::False {};
    template<typename C, typename T> struct IsMember<T C::*> : SFINE::True {};

    template<typename T> struct IsMember<T const> : IsMember<T> {};
    template<typename T> struct IsMember<T const volatile> : IsMember<T> {};
    template<typename T> struct IsMember<T volatile> : IsMember<T> {};

    template<typename T> 
    struct IsMethod : SFINE::False {};
    template<typename C, typename R>
    struct IsMethod<R(C::*)()> : SFINE::True {};
    template<typename C, typename R, typename T1>
    struct IsMethod<R(C::*)(T1)> : SFINE::True {};
    template<typename C, typename R, typename T1, typename T2>
    struct IsMethod<R(C::*)(T1, T2)> : SFINE::True {};
    template<typename C, typename R, typename T1, typename T2, typename T3>
    struct IsMethod<R(C::*)(T1, T2, T3)> : SFINE::True {};
    template<typename C, typename R, typename T1, typename T2, typename T3, typename T4>
    struct IsMethod<R(C::*)(T1, T2, T3, T4)> : SFINE::True {};
    template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
    struct IsMethod<R(C::*)(T1, T2, T3, T4, T5)> : SFINE::True {};
    template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
    struct IsMethod<R(C::*)(T1, T2, T3, T4, T5, T6)> : SFINE::True {};
    template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
    struct IsMethod<R(C::*)(T1, T2, T3, T4, T5, T6, T7)> : SFINE::True {};
    template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
    struct IsMethod<R(C::*)(T1, T2, T3, T4, T5, T6, T7, T8)> : SFINE::True {};
    template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
    struct IsMethod<R(C::*)(T1, T2, T3, T4, T5, T6, T7, T8, T9)> : SFINE::True {};
    template<typename C, typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
    struct IsMethod<R(C::*)(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> : SFINE::True {};

    template<typename T>
    struct IsField : SFINE::Bool<
        IsMember<T>::value && !IsMethod<T>::value
    > {};

    template<typename E> struct IsEnum : SFINE::UnaryHelper<
        SFINE::IsIntConvertable,
        IsClass<E>::value ||
        IsArray<E>::value ||
        IsReference<E>::value ||
        IsFloat<E>::value ||
        IsIntegral<E>::value
    >::type<E> {};


    template<typename T> struct MemberClass {
        typedef void type;
    };

    template<typename T, typename C> struct MemberClass<T C::*> {
        typedef C type;
    };


    template<typename LHS, typename RHS, Operator::EType Type>
    struct HasInternal : SFINE::InternalOperatorHelper<
        IsEnum<LHS>::value || IsEnum<RHS>::value
    >::type<LHS, RHS, Type> {};

    template<typename LHS, typename RHS, Operator::EType Type>
    struct HasExternal : SFINE::ExternalOperatorHelper<
        IsEnum<LHS>::value || IsEnum<RHS>::value
    >::type<LHS, RHS, Type> {};
}

