#include "Serialization.h"

namespace System
{
	namespace Serialization
	{


        Keyword::Keyword()
            : m_eType(Unknown)
            , m_nFlags(0)
            , m_nMinParams(~0)
            , m_nMaxParams(~0)
            , m_nMinChilds(~0)
            , m_nMaxChilds(~0)
            , m_nKeyParam(~0)
        { }

        Keyword::Keyword(const Keyword& rhs)
            : m_eType(rhs.m_eType)
            , m_nFlags(rhs.m_nFlags)
            , m_nMinParams(rhs.m_nMinParams)
            , m_nMaxParams(rhs.m_nMaxParams)
            , m_nMinChilds(rhs.m_nMinChilds)
            , m_nMaxChilds(rhs.m_nMaxChilds)
            , m_nKeyParam(rhs.m_nKeyParam)
        { }


        Keyword& Keyword::Key(const size_t knParamIndex) {
            m_nKeyParam = knParamIndex;
            return *this;
        }

        Keyword& Keyword::Type(const EType keValue) {
            m_eType = keValue;
            return *this;
        }

        Keyword& Keyword::Flag(const EFlags keValue) {
            m_nFlags |= keValue;
            return *this;
        }

        Keyword& Keyword::Params(const size_t knValue) {
            m_nMinParams = knValue;
            m_nMaxParams = knValue;
            return *this;
        }
        Keyword& Keyword::Childs(const size_t knValue) {
            m_nMinChilds = knValue;
            m_nMaxChilds = knValue;
            return *this;
        }

        Keyword& Keyword::MinParams(const size_t knValue) {
            m_nMinParams = knValue;
            return *this;
        }
        Keyword& Keyword::MaxParams(const size_t knValue) {
            m_nMaxParams = knValue;
            return *this;
        }

        Keyword& Keyword::MinChilds(const size_t knValue) {
            m_nMinChilds = knValue;
            return *this;
        }
        Keyword& Keyword::MaxChilds(const size_t knValue) {
            m_nMaxChilds = knValue;
            return *this;
        }

        bool Keyword::TryGetMinParams(size_t& reValue) const {
            reValue = m_nMinParams;
            return m_nMinParams != ~0;
        }
        bool Keyword::TryGetMaxParams(size_t& reValue) const {
            reValue = m_nMaxParams;
            return m_nMaxParams != ~0;
        }

        bool Keyword::TryGetMinChilds(size_t& reValue) const {
            reValue = m_nMinChilds;
            return m_nMinChilds != ~0;
        }
        bool Keyword::TryGetMaxChilds(size_t& reValue) const {
            reValue = m_nMaxChilds;
            return m_nMaxChilds != ~0;
        }

        bool Keyword::TryGetKeyParam(size_t& rnValue) const {
            if (m_nKeyParam == ~0)
                return false;
            rnValue = m_nKeyParam;
            return true;
        }

        bool Keyword::HasFlag(const EFlags keFlag) const {
            return (m_nFlags & keFlag) == keFlag;
        }

        const Keyword::EType Keyword::GetType() const {
            return m_eType;
        }

		const Keyword* Keyword::Get(const Word::IdType knWordId) {
			return Keywords::GetInstance().GetByWordId(knWordId);
		}


        Keywords& Keywords::GetInstance() {
            static Keywords s_cInstance;

            if (s_cInstance.m_mapKeywords.empty())
                s_cInstance.BindClasses();

            return s_cInstance;
        }

        const Keyword* Keywords::GetByWordId(const Word::IdType knWordId) {
            MapType::const_iterator it = m_mapKeywords.find(knWordId);

            if (it != m_mapKeywords.end())
                return &Reflection::Attribute::Get<Keyword>(it->second);

            return NULL;
        }

        Error Keywords::BindClasses() {
            const Reflection::Class::MapType& krmapClasses = Reflection::Class::GetClasses();

            for (Reflection::Class::MapType::const_iterator it = krmapClasses.begin(); it != krmapClasses.end(); ++it)
                $return_on_error(BindKeywords(it->second));

            return Error();
        }

        Error Keywords::BindKeywords(const Reflection::Class& krcClass) {
            const Reflection::Attribute::Link::VectorType* kpvecAttributes = krcClass.GetAttributedMethods($type(Keyword));

            if (kpvecAttributes == NULL)
                return Error();

            const char* szClass = krcClass.GetType().GetName();

            for (Reflection::Attribute::Link::VectorType::const_iterator link = kpvecAttributes->begin(); link != kpvecAttributes->end(); ++link) {
                const Reflection::Method& krcMethod = link->GetOwner<Reflection::Method>();
                const Word::IdType knWordId = krcMethod.GetName().GetWordId();
                const Word::PointerType szWord = krcMethod.GetName().GetWordString().c_str();

                if (!m_mapKeywords.insert(std::make_pair(knWordId, link->GetId())).second)
                    return Error(1, "duplicate keyword detected", szWord, szClass, NULL);
            }

            return Error();
        }


        Descriptor::Descriptor()
            : m_nTokenId(~0)
            , m_vecParameters()
        { }

        Descriptor::Descriptor(const Descriptor& rhs)
            : m_nTokenId(rhs.m_nTokenId)
            , m_vecParameters(rhs.m_vecParameters)
        { }

        Descriptor::Descriptor(const Token::IdType knTokenId)
            : m_nTokenId(knTokenId)
            , m_vecParameters()
        { }

        Descriptor::operator bool() const {
            return m_nTokenId < ~0;
        }

        bool Descriptor::operator == (const Descriptor& rhs) const {
            return m_nTokenId == rhs.m_nTokenId;
        }

        bool Descriptor::operator != (const Descriptor& rhs) const {
            return m_nTokenId != rhs.m_nTokenId;
        }

        void Descriptor::SetToken(const Token::IdType knTokenId) {
            m_nTokenId = knTokenId;
        }

        bool Descriptor::HasParameter(const Token::IdType knTokenId) const {
            for (Token::IdVectorType::const_iterator it = m_vecParameters.begin(); it != m_vecParameters.end(); ++it)
                if (*it == knTokenId)
                    return true;

            return false;
        }

        void Descriptor::AddParameter(const Token::IdType knTokenId) {
            m_vecParameters.push_back(knTokenId);
        }


        bool Descriptor::TryAddParameter(const Token::IdType knTokenId) {
            if (HasParameter(knTokenId))
                return false;

            m_vecParameters.push_back(knTokenId);

            return true;
        }

        const Token::IdVectorType& Descriptor::GetParameters() const {
            return m_vecParameters;
        }

        size_t Descriptor::GetParametersCount() const {
            return m_vecParameters.size();
        }

        Token::IdType Descriptor::GetTokenId() const {
            return m_nTokenId;
        }

        const Token& Descriptor::GetToken(const Token::VectorType& krvecTokens) const {
            return krvecTokens[m_nTokenId];
        }

        const Token& Descriptor::GetParameter(const Token::VectorType& krvecTokens, const size_t knIndex) const {
            return krvecTokens[m_vecParameters[knIndex]];
        }

        bool Descriptor::TryPushSlices(Visitor::Slices& rcSlices, const Serialization::Token::VectorType& krvecTokens) const {
            if (!GetToken(krvecTokens).TryPushSlice(rcSlices, m_nTokenId))
                return false;

            for (size_t i = 0, c = GetParametersCount(); i < c; ++i) {
                const size_t knParameterTokenId = m_vecParameters[i];
                
                if (!krvecTokens[knParameterTokenId].TryPushSlice(rcSlices, knParameterTokenId))
                    return false;
            }

            return true;
        }

        bool Descriptor::TryGetKeyWordId(Word::IdType& rnKeyWordId, const Token::VectorType& krvecTokens, const Keyword* kpcKeyword, const Keyword::EType keAllowedType) const {
            rnKeyWordId = ~0;

            if (kpcKeyword == NULL)
                return false;

            if (kpcKeyword->GetType() != keAllowedType)
                return false;

            size_t nParamIndex = ~0;

            if (kpcKeyword->TryGetKeyParam(nParamIndex))
                rnKeyWordId = GetParameter(krvecTokens, nParamIndex).GetWordId();
            else if (kpcKeyword->HasFlag(Keyword::AllowSelfAsKey))
                rnKeyWordId = GetToken(krvecTokens).GetWordId();

            return rnKeyWordId < ~0;
        }


        Descriptor::IdType Descriptors::AddDescriptor(const Token::IdType knTokenId) {
            Descriptor::IdType nId = m_vecDescriptors.size();

            m_vecDescriptors.push_back(Descriptor(knTokenId));

            return nId;
        }

        Descriptor& Descriptors::GetDescriptor(const Descriptor::IdType knId) {
            return m_vecDescriptors[knId];
        }

        const Descriptor& Descriptors::GetDescriptor(const Descriptor::IdType knId) const {
            return m_vecDescriptors[knId];
        }

        size_t Descriptors::GetDescriptorCount() const {
            return m_vecDescriptors.size();
        }

        void Descriptors::Clear() {
            m_vecDescriptors.clear();
        }
	}
}