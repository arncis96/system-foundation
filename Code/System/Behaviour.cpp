#include "Behaviour.h"


namespace System
{
    namespace Behaviour
    {

        Counter::Counter()
            : m_nCurrent(0)
        { }

        Counter::Counter(const Counter& rhs)
            : m_nCurrent(rhs.m_nCurrent)
        { }


        size_t Counter::Get() const {
            return m_nCurrent;
        }

        void Counter::Next() {
            ++m_nCurrent;
        }

        void Counter::Reset() {
            m_nCurrent = 0;
        }

        bool Counter::IsValid(const Data::Node& krcNode) const {
            return m_nCurrent < krcNode.GetChildrenCount();
        }

        bool Counter::IsValid(const size_t knLimit) const {
            return m_nCurrent < knLimit;
        }


        Randomizer::Randomizer()
            : m_nCurrent(0)
            , m_bAreValidWeights(false)
        { }

        Randomizer::Randomizer(const Randomizer& rhs)
            : m_nCurrent(rhs.m_nCurrent)
            , m_bAreValidWeights(rhs.m_bAreValidWeights)
        { }

        size_t Randomizer::Get() const {
            return m_nCurrent;
        }

        void Randomizer::Reset(const Data::Node& krcNode) {

        }

        bool Randomizer::IsValid(const Data::Node& krcNode) const {
            return m_bAreValidWeights && m_nCurrent < krcNode.GetChildrenCount();
        }


        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Root).Flag(Serialization::Keyword::AllowProxy).Params(1).Childs(1).Key(0)][$name("run")], Runner::EStatus, Core, Run) (const Word::Handle& krcNameWord)
        {
            return Runner::Processor::Get().InvokeChild(0);
        }

        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural)][$name("fallback")], Runner::EStatus, Core, Fallback)()
        {
            Runner::Processor& rcProcessor = Runner::Processor::Get();
            const Data::Node& krcNode = rcProcessor.GetNode();
            const Runner::EStatus keCurrentState = rcProcessor.GetStatus(krcNode);

            if (keCurrentState == Runner::Failed)
                return Runner::Failed;

            Counter& rcCounter = rcProcessor.GetOrCreateData<Counter>(krcNode);

            if (keCurrentState == Runner::Ready)
                rcCounter.Reset();

            Runner::EStatus eStatus = Runner::Failed;

            do {
                eStatus = rcProcessor.InvokeChild(krcNode, rcCounter.Get());

                if (eStatus == Runner::Failed)
                    rcCounter.Next();

            } while (eStatus == Runner::Failed && rcCounter.IsValid(krcNode));

            return eStatus;
        }
        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural)][$name("not")], Runner::EStatus, Core, Not)()
        {
            Runner::EStatus eStatus = Runner::Processor::Get().InvokeChild(0);

            if (eStatus == Runner::Failed)
                eStatus = Runner::Succeeded;
            else if (eStatus == Runner::Succeeded)
                eStatus = Runner::Failed;

            return eStatus;
        }
        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural)][$name("parallel")], Runner::EStatus, Core, Parallel)()
        {
            Runner::Processor& rcProcessor = Runner::Processor::Get();
            const Data::Node& krcNode = rcProcessor.GetNode();

            Runner::EStatus eStatus = Runner::Failed;
            bool bAllSucceeded = true;

            for (size_t i = 0; i < krcNode.GetChildrenCount(); ++i) {
                Runner::EStatus eChildStatus = rcProcessor.InvokeChild(krcNode, i);

                if (eChildStatus == Runner::Failed) {
                    eStatus = Runner::Failed;
                    bAllSucceeded = false;

                    break;
                }
                else if (eChildStatus == Runner::Ready || eChildStatus == Runner::Running) {
                    eStatus = Runner::Running;
                    bAllSucceeded = false;
                }
            }

            if (bAllSucceeded)
                eStatus = Runner::Succeeded;

            return eStatus;
        }
        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural)][$name("repeat")], Runner::EStatus, Core, Repeat)(const size_t knLimit)
        {
            Runner::Processor& rcProcessor = Runner::Processor::Get();

            const Data::Node& krcNode = rcProcessor.GetNode();
            const Data::Node& krcAction = rcProcessor.GetChild(0);
            const Runner::EStatus keCurrentStatus = rcProcessor.GetStatus(krcNode);
            const Runner::EStatus keCurrentActionStatus = rcProcessor.GetStatus(krcAction);

            Counter& rcCounter = rcProcessor.GetOrCreateData<Counter>(krcNode);

            if (keCurrentStatus == Runner::Ready)
                rcCounter.Reset();
            else if (keCurrentStatus == Runner::Running && keCurrentActionStatus == Runner::Succeeded && (!knLimit || rcCounter.IsValid(knLimit)))
                rcCounter.Reset();

            Runner::EStatus eStatus = rcProcessor.Invoke(krcAction);

            if (eStatus == Runner::Succeeded) {
                rcCounter.Next();

                if (!knLimit || rcCounter.IsValid(knLimit))
                    eStatus = Runner::Running;
            }

            return eStatus;
        }
        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural)][$name("race")], Runner::EStatus, Core, Race)()
        {
            Runner::Processor& rcProcessor = Runner::Processor::Get();
            const Data::Node& krcNode = rcProcessor.GetNode();

            Runner::EStatus eStatus = Runner::Failed;
            bool bAllFailed = true;


            for (size_t i = 0; i < krcNode.GetChildrenCount(); ++i) {
                Runner::EStatus eChildStatus = rcProcessor.InvokeChild(krcNode, i);

                if (eChildStatus == Runner::Succeeded) {
                    eStatus = Runner::Succeeded;
                    bAllFailed = false;

                    break;
                }
                else if (eChildStatus == Runner::Running) {
                    eStatus = Runner::Running;
                    bAllFailed = false;
                }
            }

            if (bAllFailed)
                eStatus = Runner::Failed;

            return eStatus;
        }
        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural)][$name("random")], Runner::EStatus, Core, Random)()
        {
            Runner::Processor& rcProcessor = Runner::Processor::Get();
            const Data::Node& krcNode = rcProcessor.GetNode();

            Randomizer& rcRandomizer = rcProcessor.GetOrCreateData<Randomizer>(krcNode);

            Runner::EStatus eStatus = Runner::Failed;

            if (rcProcessor.GetStatus(krcNode) == Runner::Ready)
                rcRandomizer.Reset(krcNode);

            if (rcRandomizer.IsValid(krcNode))
                eStatus = rcProcessor.InvokeChild(krcNode, rcRandomizer.Get());

            return eStatus;
        }
        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural)][$name("sequence")], Runner::EStatus, Core, Sequence)()
        {
            Runner::Processor& rcProcessor = Runner::Processor::Get();
            const Data::Node& krcNode = rcProcessor.GetNode();

            Counter& rcCounter = rcProcessor.GetOrCreateData<Counter>(krcNode);

            if (rcProcessor.GetStatus(krcNode) == Runner::Ready)
                rcCounter.Reset();

            Runner::EStatus eStatus = Runner::Failed;

            do {
                const Data::Node& krcChild = rcProcessor.GetChild(krcNode, rcCounter.Get());

                eStatus = rcProcessor.Invoke(krcChild);

                if (eStatus == Runner::Succeeded)
                    rcCounter.Next();

            } while (eStatus == Runner::Succeeded && rcCounter.IsValid(krcNode));

            if (eStatus == Runner::Succeeded && rcCounter.IsValid(krcNode))
                eStatus = Runner::Running;

            return eStatus;
        }
        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural)][$name("while")], Runner::EStatus, Core, While)()
        {
            Runner::Processor& rcProcessor = Runner::Processor::Get();
            const Data::Node& krcNode = rcProcessor.GetNode();
            const Data::Node& krcCondition = rcProcessor.GetChild(0);
            const Data::Node& krcAction = rcProcessor.GetChild(1);

            bool& rbConditionHasSucceeded = rcProcessor.GetOrCreateVariable<bool>(krcNode);

            if (rcProcessor.GetStatus(krcNode) == Runner::Ready) {
                rcProcessor.SetStatus(krcCondition, Runner::Ready);
                rcProcessor.SetStatus(krcAction, Runner::Ready);

                rbConditionHasSucceeded = false;
            }

            if (rcProcessor.GetStatus(krcCondition) == Runner::Succeeded)
                rcProcessor.SetStatus(krcCondition, Runner::Ready);

            Runner::EStatus eStatus = rcProcessor.Invoke(krcCondition);

            if (eStatus != Runner::Running)
                rbConditionHasSucceeded = eStatus == Runner::Succeeded;

            if (rbConditionHasSucceeded)
                eStatus = rcProcessor.Invoke(krcAction);

            return eStatus;
        }
        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural)][$name("if")], Runner::EStatus, Core, If)()
        {
            Runner::Processor& rcProcessor = Runner::Processor::Get();
            const Data::Node& krcNode = rcProcessor.GetNode();
            const Data::Node& krcCondition = rcProcessor.GetChild(0);
            const Data::Node& krcThen = rcProcessor.GetChild(1);
            const Data::Node* kpcElse = rcProcessor.GetOptionalChild(2);

            if (rcProcessor.GetStatus(krcNode) == Runner::Ready) {
                rcProcessor.SetStatus(krcCondition, Runner::Ready);
                rcProcessor.SetStatus(krcThen, Runner::Ready);
                
                if (kpcElse)
                    rcProcessor.SetStatus(*kpcElse, Runner::Ready);
            }

            if (rcProcessor.GetStatus(krcCondition) != Runner::Running)
                rcProcessor.SetStatus(krcCondition, Runner::Ready);

            Runner::EStatus eStatus = rcProcessor.Invoke(krcCondition);

            switch (eStatus)
            {
                case Runner::Succeeded:
                    eStatus = rcProcessor.Invoke(krcThen);
                    break;
                case Runner::Failed:
                    if (kpcElse)
                        eStatus = rcProcessor.Invoke(*kpcElse);
                    break;

                case Runner::Running:
                case Runner::Ready:
                case Runner::Absent:
                default:
                    break;
            }

            return eStatus;
        }

        $method([Runner::Task()][Serialization::Keyword().Type(Serialization::Keyword::Structural)][$name("mute")], Runner::EStatus, Core, Mute)()
        {
            Runner::EStatus eStatus = Runner::Processor::Get().InvokeChild(0);

            if (eStatus == Runner::Failed)
                eStatus = Runner::Succeeded;

            return eStatus;
        }
        $method([Runner::Task()], Runner::EStatus, Core, Fail)() {
            return Runner::Failed;
        }
        $method([Runner::Task()], Runner::EStatus, Core, Success)() {
            return Runner::Succeeded;
        }
    }
}