#include "Test.h"
#include "Runner.h"
#include "ISystem.h"
#include "ILog.h"
#include "ICryPak.h"
#include "IRenderer.h"
#include "Stream.h"

struct EntityCloneState;

namespace System
{
    namespace Test
    {
        const float eps = 0.0001;

        $class(, Example)
        {
            CFColor m_color;
            float m_timer;
        public:
            typedef void(*WriteSignatureType)(CStream&, EntityCloneState*);
            typedef void(*ReadSignatureType)(CStream&);

            Example();
            Example(const Example& rhs);

            bool operator == (const Example& rhs) const;
            bool operator != (const Example& rhs) const;

            Runner::EStatus SetColor(float r, float g, float b);
            Runner::EStatus Wait(float duration);

            void Write(CStream& stream, EntityCloneState* ecs);
            void Read(CStream & stream);
        };

        Example::Example()
            : m_color(0.f)
            , m_timer(0.f)
        { }

        Example::Example(const Example& rhs)
            : m_color(rhs.m_color)
            , m_timer(rhs.m_timer)
        { }


        bool Example::operator == (const Example& rhs) const {
            return fabsf(m_color.r - rhs.m_color.r) <= eps
                && fabsf(m_color.g - rhs.m_color.g) <= eps
                && fabsf(m_color.b - rhs.m_color.b) <= eps
                && fabsf(m_color.a - rhs.m_color.a) <= eps
                && fabsf(m_timer - rhs.m_timer) <= eps;
        }
        bool Example::operator != (const Example& rhs) const {
            return fabsf(m_color.r - rhs.m_color.r) > eps
                || fabsf(m_color.g - rhs.m_color.g) > eps
                || fabsf(m_color.b - rhs.m_color.b) > eps
                || fabsf(m_color.a - rhs.m_color.a) > eps
                || fabsf(m_timer - rhs.m_timer) > eps;
        }

        $method([Runner::Task()], Runner::EStatus, Example, SetColor)(float r, float g, float b) {
            GetISystem()->GetILog()->Log("%s::SetColor(%f, %f, %f)", $typeof(this).GetName(), r, g, b);

            m_color.Set(r, g, b);

            return Runner::Succeeded;
        }
        $method([Runner::Task()], Runner::EStatus, Example, Wait)(float duration) {
            
            GetISystem()->GetILog()->Log("%s::Wait(%f)", $typeof(this).GetName(), duration);

            m_timer = duration;

            return Runner::Succeeded;
        }

        $method([Runner::Interface()], void, Example, Write)(CStream& stream, EntityCloneState* ecs) {
            stream.Write(m_color.r);
            stream.Write(m_color.g);
            stream.Write(m_color.b);
            stream.Write(m_color.a);
            stream.Write(m_timer);
        }

        $method([Runner::Interface()], void, Example, Read)(CStream& stream) {
            stream.Read(m_color.r);
            stream.Read(m_color.g);
            stream.Read(m_color.b);
            stream.Read(m_color.a);
            stream.Read(m_timer);
        }

        Error Read(Runner::Script& script, const char* path)
        {
            ICryPak* pak = GetISystem()->GetIPak();

            if (pak == NULL)
                return Error(1, "vfs not found");

            FILE* file = pak->FOpen(path, "rb");

            if (file == NULL)
                return Error(2, "file not found", path, NULL);

            while (!pak->FEof(file))
            {
                char buff[512] = { 0 };

                if (pak->FRead(buff, 1, 512, file))
                    script << buff;
            }

            pak->FClose(file);

            return script.Parse();
        }

        Error RunnerCase()
        {
            Data::Repository repository;
            Runner::Script script;
            Runner::Programs programs;
            Runner::Processor procesor;
            Runner::EntryPoint build;
            Runner::EntryPoint root;


            repository.CreateObject<Data::Builder>();


            $return_on_error(Read(script, "ColorTest.txt"));

            $return_on_error(programs.AddScript(&script));

            $return_on_error(programs.ResolvePrograms(repository));

            $return_on_error(procesor.Bind(&repository, &programs));

            $return_on_error(programs.BindEntryPoint(build, "build"));
            $return_on_error(programs.BindEntryPoint(root, "Root"));

            if (procesor(build) != Runner::Succeeded)
                return Error(1, "failed to build");

            Binding::InterfaceInvokersFor<Example::WriteSignatureType> cWriteInterface;

            $return_on_error(procesor.BindInterfaceInvokers(cWriteInterface, "Write"));

            while (procesor(root) == Runner::Running);

            CStream stream;

            cWriteInterface.Invoke(stream, NULL);

            const Example* kpcExample = repository.GetFirstObject<Example>();

            Example example;

            example.Read(stream);

            if (example != *kpcExample)
                return Error(1, "failed example data serialization");

            return Error();
        }

        Error Main()
        {
            const Word::Table& kcWords = Word::Table::Get();

            $return_on_error(RunnerCase());

            return Error();
        }
    }
}