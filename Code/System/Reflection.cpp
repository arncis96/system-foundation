#include "Reflection.h"
#include "Serialization.h"

#include <sstream>

namespace System
{
	namespace Reflection
	{
        inline bool Remove(size_t& len, char* buffer, const char* input) {
            size_t input_len = strlen(input);

            if ((len > input_len) && !strncmp(buffer, input, input_len)) {
                memmove(buffer, buffer + input_len, len - input_len);
                buffer[len - input_len] = '\0';
                len -= input_len;

                return true;
            }

            return false;
        }

        inline bool RemoveEnding(size_t& len, char* buffer, const char* input) {
            size_t input_len = strlen(input);

            if (len > input_len) {
                if (!strncmp(&buffer[len - input_len], input, input_len)) {
                    buffer[len - input_len] = '\0';
                    len -= input_len;

                    return true;
                }
            }

            return false;
        }


        Name::Name()
            : m_cWordHandle()
        { }

        Name::Name(const Name& rhs)
            : m_cWordHandle(rhs.m_cWordHandle)
        { }

        Name::Name(const char* szName)
            : m_cWordHandle(szName)
        { }

        const Word::Handle& Name::GetWordHandle() const {
            return m_cWordHandle;
        }

        const Word::StringType& Name::GetWordString() const {
            return m_cWordHandle.GetString();
        }

        const Word::IdType& Name::GetWordId() const {
            return m_cWordHandle.GetId();
        }

        void Name::Trim(char* name)
        {
            size_t len = strlen(name);

            Remove(len, name, "const ");
            Remove(len, name, "struct ");
            Remove(len, name, "class ");
            Remove(len, name, "enum ");

            while (name[len - 1] == ' ' ||
                name[len - 1] == '&' ||
                name[len - 1] == '*')
            {
                len--;
                name[len] = '\0';
            }

            RemoveEnding(len, name, " const");
        }


        SourceLocation::SourceLocation()
            : m_szFileName(NULL)
            , m_knLineNumber(~0)
        { }

        SourceLocation::SourceLocation(const SourceLocation& rhs)
            : m_szFileName(rhs.m_szFileName)
            , m_knLineNumber(rhs.m_knLineNumber)
        { }


        SourceLocation::SourceLocation(const char* szFileName, const size_t knLineNunmber)
            : m_szFileName(szFileName)
            , m_knLineNumber(knLineNunmber)
        { }

        SourceLocation::operator bool() const {
            return m_szFileName && m_knLineNumber != ~0;
        }

        const char* SourceLocation::GetFileName() const {
            return m_szFileName;
        }

        const size_t SourceLocation::GetLineNumber() const {
            return m_knLineNumber;
        }


        MembersInfo::Node::Node()
            : m_mapByName()
            , m_mapByAttributes()
        { }

        MembersInfo::Node::Node(const Node& rhs)
            : m_mapByName(rhs.m_mapByName)
            , m_mapByAttributes(rhs.m_mapByAttributes)
        { }

        void MembersInfo::Node::Add(const Attribute::VectorType& krvecAttribues, const Word::Handle& krcName, IdType nOwnerId)
        {
            const size_t knAttributes = krvecAttribues.size();

            m_mapByName.insert(std::make_pair(krcName, nOwnerId));

            for (Attribute::VectorType::const_iterator it = krvecAttribues.begin(); it != krvecAttribues.end(); ++it)
            {
                const Type& krcType = it->GetType();
                const IdType knAttributeId = it->GetId();
                const Attribute::Link knAttributeLink(knAttributeId, nOwnerId);

                m_mapByAttributes[krcType.GetId()].push_back(knAttributeLink);
            }
        }

        const Type::IdType* MembersInfo::Node::GetById(Word::IdType nNameId) const {

            MapType::const_iterator it = m_mapByName.find(nNameId);

            if (it == m_mapByName.end())
                return NULL;

            return &it->second;
        }

        const Attribute::Link::VectorType* MembersInfo::Node::GetByAttribute(const Type& krcAttributeType) const {
            Attribute::Link::MapType::const_iterator it = m_mapByAttributes.find(krcAttributeType.GetId());

            if (it == m_mapByAttributes.end())
                return NULL;

            return &it->second;
        }

        MembersInfo::MembersInfo()
            : m_cFlatNode()
            , m_mapByOwnerType()
        { }

        MembersInfo::MembersInfo(const MembersInfo& rhs)
            : m_cFlatNode(rhs.m_cFlatNode)
            , m_mapByOwnerType(rhs.m_mapByOwnerType)
        { }

        void MembersInfo::Add(
            const Attribute::VectorType& krvecAttribues, 
            const Word::Handle& krcName, 
            const Type& krcOwnerType, 
            IdType nOwnerId
        ) {
            m_cFlatNode.Add(krvecAttribues, krcName, nOwnerId);
            m_mapByOwnerType[krcOwnerType.GetId()].Add(krvecAttribues, krcName, nOwnerId);
        }

        const MembersInfo::MapType& MembersInfo::GetWithName() const {
            return m_cFlatNode.m_mapByName;
        }

        const MembersInfo::MapType* MembersInfo::GetWithName(const Type& krcOwnerType) const {
            const Node* ptrsNode = GetByType(krcOwnerType);

            if (ptrsNode == NULL)
                return NULL;

            return &ptrsNode->m_mapByName;
        }

        const MembersInfo::IdType* MembersInfo::GetById(const Type& krcOwnerType, Word::IdType nNameId) const {
            const Node* ptrsNode = GetByType(krcOwnerType);

            if (ptrsNode == NULL)
                return NULL;

            return ptrsNode->GetById(nNameId);
        }

        const Attribute::Link::VectorType* MembersInfo::GetByAttribute(const Type& krcOwnerType, const Type& krcAttributeType) const {
            const Node* ptrsNode = GetByType(krcOwnerType);

            if (ptrsNode == NULL)
                return NULL;

            return ptrsNode->GetByAttribute(krcAttributeType);
        }

        const MembersInfo::IdType* MembersInfo::GetById(Word::IdType nNameId) const {
            return m_cFlatNode.GetById(nNameId);
        }

        const Attribute::Link::VectorType* MembersInfo::GetByAttribute(const Type& krcAttributeType) const {
            return m_cFlatNode.GetByAttribute(krcAttributeType);
        }

        const MembersInfo::Node* MembersInfo::GetByType(const Type& krcOwnerType) const {
            std::map<Type::IdType, Node>::const_iterator it = m_mapByOwnerType.find(krcOwnerType.GetId());

            if (it == m_mapByOwnerType.end())
                return NULL;

            return &it->second;
        }



        Enum& Enum::Create(Type& rcEnumType)
        {
            m_nTypeId = rcEnumType.GetDecayId();
            Namespace::TryParse(rcEnumType);

            return *this;
        }

        Class& Class::Create(Type& krcClassType)
        {
            m_nTypeId = krcClassType.GetDecayId();
            Namespace::TryParse(krcClassType);

            return *this;
        }

        Namespace::Namespace()
            : m_cWord()
            , m_nId(~0)
            , m_nParentId(~0)
            , m_mapChildIds()
            , m_mapTypeIds()
        { }
        Namespace::Namespace(const Namespace& rhs)
            : m_cWord(rhs.m_cWord)
            , m_nId(rhs.m_nId)
            , m_nParentId(rhs.m_nParentId)
            , m_mapChildIds(rhs.m_mapChildIds)
            , m_mapTypeIds(rhs.m_mapTypeIds)
        { }

        Namespace::IdType Namespace::FindId(const Word::Handle::VectorType& krvecWordIds) {
            IdType id = 0;

            for (Word::Handle::VectorType::const_iterator it = krvecWordIds.begin(); id < ~0 && it != krvecWordIds.end(); ++it) {
                const Word::PointerType kszWord = it->GetString().c_str();
                
                id = Get(id).GetChildId(it->GetId());
            }
            
            return id;
        }

        Namespace& Namespace::Get(const IdType knId) {
            return GetNamespaces()[knId];
        }

        const Namespace::IdType Namespace::GetId() const {
            return m_nId;
        }

        const Namespace::IdType Namespace::GetParentId() const {
            return m_nParentId;
        }

        const Word::Handle& Namespace::GetWord() const {
            return m_cWord;
        }

        const Type::IdType Namespace::GetTypeId(const Word::Handle& krcWord) const {
            MapType::const_iterator it = m_mapTypeIds.find(krcWord);

            if (it != m_mapTypeIds.end())
                return it->second;

            return ~0;
        }

        const Type::IdType Namespace::GetChildId(const Word::Handle& krcWord) const {
            MapType::const_iterator it = m_mapChildIds.find(krcWord);

            if (it != m_mapChildIds.end())
                return it->second;

            return ~0;
        }

        Namespace& Namespace::Set(const Word::Handle& krcWord, const IdType knId, const IdType knParentId) {
            m_cWord = krcWord;
            m_nId = knId;
            m_nParentId = knParentId;

            return *this;
        }

        Namespace& Namespace::SetType(const Word::Handle& krcWord, Type& rcType) {
            rcType.Set(krcWord.GetId(), m_nId);

            m_mapTypeIds[krcWord] = rcType.GetId();

            return *this;
        }

        const Namespace::IdType Namespace::GetOrCreateChildId(const Word::Handle& krcWord) {
            VectorType& rvecNamespaces = GetNamespaces();
            MapType::const_iterator it = m_mapChildIds.find(krcWord);

            if (it == m_mapChildIds.end()) {
                const IdType knId = rvecNamespaces.size();
                const IdType knParenId = m_nId;

                it = m_mapChildIds.insert(std::make_pair(krcWord, knId)).first;

                rvecNamespaces.push_back(Namespace().Set(krcWord, knId, knParenId));
            }

            return it->second;
        }

        Namespace::VectorType& Namespace::GetNamespaces() {
            static VectorType s_vecNamespaces;

            if (s_vecNamespaces.empty())
                s_vecNamespaces.push_back(Namespace().Set(~0, 0, ~0));

            return s_vecNamespaces;
        }



        bool Namespace::TryParse(Type& rcType, IdType* pnOutId) {
            std::stringstream stream;
            stream << rcType.GetName();
            Serialization::Token cToken;
            bool eof;
            IdType id = 0;
            VectorType& rvecNamespaces = GetNamespaces();


            while (Serialization::TryParse(stream, cToken, &eof)) {
                char cTokenString[512] = { 0 };

                if (!cToken.TryGetString(stream, cTokenString))
                    cTokenString[0] = 0;

                switch (cToken.GetType()) {
                case Serialization::Token::Word:
                case Serialization::Token::Namespace: {
                    const Word::Handle kcWord(cToken.GetWordId());
                    Namespace& rcNamespace = rvecNamespaces[id];

                    if (eof || cToken.GetType() != Serialization::Token::Namespace) {
                        rcNamespace.SetType(kcWord, rcType);

                        if (pnOutId)
                            *pnOutId = id;

                        return true;
                    }
                    else {
                        id = rcNamespace.GetOrCreateChildId(kcWord);
                    }
                } break;

                default:
                    break;
                }
            }

            return false;
        }

        bool Namespace::TryParse(Visitor::Slices& rcSlices, Type::IdType* pnOutTypeId, IdType* pnOutNamespaceId) {
            
            Visitor::Slice sSlice;

            IdType id = 0;
            const VectorType& krvecNamespaces = GetNamespaces();

            while (rcSlices.TryPopSlice(sSlice)) {
                switch (sSlice.m_nType) {
                case Serialization::Token::Word:
                case Serialization::Token::Namespace: {
                    const Word::Handle kcWordHandle(sSlice.m_nWordId);
                    const Namespace& krcNamespace = krvecNamespaces[id];
                    const IdType knChildId = krcNamespace.GetChildId(kcWordHandle);

                    if (pnOutNamespaceId)
                        *pnOutNamespaceId = id;

                    if (knChildId < ~0) {
                        id = knChildId;
                    }
                    else {
                        const Type::IdType knTypeId = krcNamespace.GetTypeId(kcWordHandle);

                        if (knTypeId < ~0) {
                            if (pnOutTypeId)
                                *pnOutTypeId = knTypeId;

                            return true;
                        }
                        else
                            return false;
                    }

                } break;

                default:
                    break;
                }
            }

            return false;
        }
	}
}